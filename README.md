<div align="center">
<h1 align="center">
<img src="https://gitlab.com/uploads/-/system/group/avatar/55037359/Icon.png?width=64" width="100" />
<br>libre-dashboard
</h1>
<h3>◦ </h3>
<h3>◦ Developed with the software and tools listed below.</h3>

<p align="center">
<img src="https://img.shields.io/badge/SVG-FFB13B.svg?style&logo=SVG&logoColor=black" alt="SVG" />
<img src="https://img.shields.io/badge/JavaScript-F7DF1E.svg?style&logo=JavaScript&logoColor=black" alt="JavaScript" />
<img src="https://img.shields.io/badge/PostCSS-DD3A0A.svg?style&logo=PostCSS&logoColor=white" alt="PostCSS" />
<img src="https://img.shields.io/badge/styledcomponents-DB7093.svg?style&logo=styled-components&logoColor=white" alt="styledcomponents" />
<img src="https://img.shields.io/badge/D-B03931.svg?style&logo=D&logoColor=white" alt="D" />
<img src="https://img.shields.io/badge/Chai-A30701.svg?style&logo=Chai&logoColor=white" alt="Chai" />
<img src="https://img.shields.io/badge/React-61DAFB.svg?style&logo=React&logoColor=black" alt="React" />
<img src="https://img.shields.io/badge/Progress-5CE500.svg?style&logo=Progress&logoColor=white" alt="Progress" />
<img src="https://img.shields.io/badge/Axios-5A29E4.svg?style&logo=Axios&logoColor=white" alt="Axios" />

<img src="https://img.shields.io/badge/ESLint-4B32C3.svg?style&logo=ESLint&logoColor=white" alt="ESLint" />
<img src="https://img.shields.io/badge/SemVer-3F4551.svg?style&logo=SemVer&logoColor=white" alt="SemVer" />
<img src="https://img.shields.io/badge/Lodash-3492FF.svg?style&logo=Lodash&logoColor=white" alt="Lodash" />
<img src="https://img.shields.io/badge/TypeScript-3178C6.svg?style&logo=TypeScript&logoColor=white" alt="TypeScript" />
<img src="https://img.shields.io/badge/Ajv-23C8D2.svg?style&logo=Ajv&logoColor=white" alt="Ajv" />
<img src="https://img.shields.io/badge/Buffer-231F20.svg?style&logo=Buffer&logoColor=white" alt="Buffer" />
<img src="https://img.shields.io/badge/Express-000000.svg?style&logo=Express&logoColor=white" alt="Express" />
<img src="https://img.shields.io/badge/Markdown-000000.svg?style&logo=Markdown&logoColor=white" alt="Markdown" />
<img src="https://img.shields.io/badge/JSON-000000.svg?style&logo=JSON&logoColor=white" alt="JSON" />
</p>
</div>

---

## 📒 Table of Contents

- [📒 Table of Contents](#-table-of-contents)
- [📍 Overview](#-overview)
- [⚙️ Features](#-features)
- [📂 Project Structure](#project-structure)
- [🧩 Modules](#modules)
- [🚀 Getting Started](#-getting-started)
- [🗺 Roadmap](#-roadmap)
- [🤝 Contributing](#-contributing)
- [📄 License](#-license)
- [👏 Acknowledgments](#-acknowledgments)

---

## 📍 Overview

This is the Libre (Defi) Dashboard which can be found at https://defi.libre.org - this enables frontend web access to the Libre Swap, Libre DAO, Libre Web Wallet, and Libre Validators.

---

## ⚙️ Features

- Trade or add liquidity on Libre Swap
- Farming for Staked LP Tokens
- Voting and proposing on Libre DAO proposals
- Accessing the Libre wallet to view balances, send tokens, or peg-in or out BTC and USDT.
- Vote on Libre validators
- View the pBTC daily audit

---

## 📂 Project Structure

```bash
repo
├── README.md
├── components
│   ├── AddBalanceModal.tsx
│   ├── AppModal.tsx
│   ├── CharacterCount.tsx
│   ├── ConnectModal.tsx
│   ├── ConnectWallet.tsx
│   ├── HeaderDetails.tsx
│   ├── MobileMenu.tsx
│   ├── NavBar.tsx
│   ├── PageSubtitle.tsx
│   ├── PageTitle.tsx
│   ├── TokenWithUSDValue.tsx
│   ├── audit
│   │   ├── OnChainValidationTable.tsx
│   │   └── ProofOfAssetsTable.tsx
│   ├── dao
│   │   ├── DaoModal.tsx
│   │   ├── DaoProposals.tsx
│   │   ├── DaoSkeleton.tsx
│   │   ├── GovernanceOverviewSection.tsx
│   │   ├── MyVotingPowerSection.tsx
│   │   ├── ProposalListItem.tsx
│   │   ├── TopAccountsSection.tsx
│   │   ├── TopAccountsTable.tsx
│   │   └── ViewMore.tsx
│   ├── dashboard
│   │   ├── Announcements.tsx
│   │   ├── LeftAnnouncement.tsx
│   │   ├── RightAnnouncement.tsx
│   │   ├── chainactivity
│   │   │   ├── ChainActivitySection.tsx
│   │   │   └── ChainActivityTable.tsx
│   │   ├── pools
│   │   │   ├── PoolsSection.tsx
│   │   │   └── PoolsTable.tsx
│   │   └── tokens
│   │       ├── TokenTable.tsx
│   │       └── TokensSection.tsx
│   ├── emptystates
│   │   └── EmptyConnectWallet.tsx
│   ├── farming
│   │   ├── FarmingActionRow.tsx
│   │   ├── FarmingHarvestBlock.tsx
│   │   ├── FarmingStakeModal.tsx
│   │   ├── FarmingTable.tsx
│   │   ├── FarmingTableRow.tsx
│   │   └── USDPriceText.tsx
│   ├── mint
│   │   ├── EmptyContributions.tsx
│   │   ├── MintModal.tsx
│   │   ├── MintTable.tsx
│   │   └── MyAvailableBTC.tsx
│   ├── referrals
│   │   └── ReferralLeaderboardTable.tsx
│   ├── stake
│   │   ├── EmergencyUnstakeModal.tsx
│   │   ├── StakeModal.tsx
│   │   └── StakeModalSlider.tsx
│   ├── swap
│   │   ├── CurrencySelector.tsx
│   │   ├── LiquidityBox.tsx
│   │   ├── LiquidityButton.tsx
│   │   ├── LiquidityDetailsBox.tsx
│   │   ├── LiquidityDetailsBoxItem.tsx
│   │   ├── SwapAmountInput.tsx
│   │   ├── SwapBox.tsx
│   │   ├── SwapButton.tsx
│   │   ├── SwapPrice.tsx
│   │   └── SwapTokenDetailsBox.tsx
│   ├── validators
│   │   ├── ValidatorsTable.tsx
│   │   └── VoteModal.tsx
│   └── wallet
│       ├── myreferrals
│       │   ├── MyReferralTable.tsx
│       │   ├── MyReferralsSection.tsx
│       │   ├── ReferralInviteModal.tsx
│       │   └── ReferralsEmptyState.tsx
│       ├── mytokens
│       │   ├── AddBalanceModal.tsx
│       │   ├── BitcoinQRCodeContent.tsx
│       │   ├── LightningAmountModal.tsx
│       │   ├── LightningQRCodeContent.tsx
│       │   ├── LightningQRCodeModal.tsx
│       │   ├── MyTokensSection.tsx
│       │   ├── MyTokensTable.tsx
│       │   └── MyTokensTableStatic.tsx
│       ├── sendreceive
│       │   ├── AmountSendInput.tsx
│       │   ├── AmountSendMax.tsx
│       │   ├── SendDetails.tsx
│       │   ├── SendInput.tsx
│       │   ├── SendModal.tsx
│       │   ├── SendSubmit.tsx
│       │   ├── SendToSelect.tsx
│       │   ├── UserList.tsx
│       │   └── UserListSkeleton.tsx
│       └── stakes
│           ├── StakeEmptyState.tsx
│           ├── StakeTable.tsx
│           └── StakesSection.tsx
├── hooks
│   ├── dao
│   │   ├── useGovernanceOverviewData.ts
│   │   ├── useTopAccountsData.ts
│   │   └── useVotingPowerData.ts
│   ├── queries
│   │   ├── audit
│   │   │   ├── keys.ts
│   │   │   ├── useAuditList.ts
│   │   │   └── useAuditStats.ts
│   │   ├── dao
│   │   │   ├── keys.ts
│   │   │   ├── useDaoAccounts.ts
│   │   │   ├── useDaoStats.ts
│   │   │   └── useUserDaoPower.ts
│   │   ├── exchangerates
│   │   │   ├── keys.ts
│   │   │   └── useExchangeRates.ts
│   │   ├── farming
│   │   │   ├── keys.ts
│   │   │   ├── useFarmingClaim.ts
│   │   │   ├── useFarmingStake.ts
│   │   │   ├── useFarmingStats.ts
│   │   │   ├── useFarmingWithdraw.ts
│   │   │   └── useUserFarming.ts
│   │   ├── markus
│   │   │   ├── keys.ts
│   │   │   ├── useCreateMarkusOrder.ts
│   │   │   └── useMarkusOffers.ts
│   │   ├── mints
│   │   │   ├── keys.ts
│   │   │   ├── useMintLibre.ts
│   │   │   ├── useUserMintStats.ts
│   │   │   └── useUserMints.ts
│   │   ├── proposals
│   │   │   ├── keys.ts
│   │   │   ├── useProposal.ts
│   │   │   ├── useProposalProperties.ts
│   │   │   ├── useProposals.ts
│   │   │   ├── useVoteAgainst.ts
│   │   │   ├── useVoteFor.ts
│   │   │   └── useVotes.ts
│   │   ├── referrals
│   │   │   ├── keys.ts
│   │   │   ├── useLeaderboardReferrals.ts
│   │   │   ├── useMyReferrals.ts
│   │   │   └── useRefferalStats.ts
│   │   ├── stakes
│   │   │   ├── keys.ts
│   │   │   ├── useClaimStake.ts
│   │   │   ├── useStakeLibre.ts
│   │   │   ├── useUnstakeLibre.ts
│   │   │   ├── useUserStakeStats.ts
│   │   │   └── useUserStakes.ts
│   │   ├── stats
│   │   │   ├── keys.ts
│   │   │   ├── useAirdropStats.ts
│   │   │   ├── useApplicationStats.ts
│   │   │   ├── useMintStats.ts
│   │   │   └── useStakeStats.ts
│   │   ├── tokens
│   │   │   ├── keys.ts
│   │   │   ├── useAddLiquidity.ts
│   │   │   ├── useCacheBTCAddress.ts
│   │   │   ├── useClaimMintRush.ts
│   │   │   ├── useGetBitcoinWrappingAddress.ts
│   │   │   ├── useGetLightningFee.ts
│   │   │   ├── useGetLightningWrappingAdress.ts
│   │   │   ├── usePoolSupply.ts
│   │   │   ├── useRegisterLightningPayment.ts
│   │   │   ├── useRemoveLiquidity.ts
│   │   │   ├── useSwapTokens.ts
│   │   │   ├── useTokenInformation.ts
│   │   │   ├── useTokens.ts
│   │   │   ├── useTransferToAddress.ts
│   │   │   ├── useTransferTokens.ts
│   │   │   ├── useUserBTCAddress.ts
│   │   │   ├── useUserLiquidity.ts
│   │   │   ├── useUserMintRush.ts
│   │   │   └── useUserTokens.ts
│   │   ├── validators
│   │   │   ├── keys.ts
│   │   │   ├── useValidatorVotes.ts
│   │   │   ├── useValidators.ts
│   │   │   └── useVoteProducer.ts
│   │   └── wallet
│   │       ├── keys.ts
│   │       ├── useSearchUsers.ts
│   │       └── useUserSearch.ts
│   ├── useBitcoinInvoice.ts
│   ├── useClickedOutside.ts
│   ├── useComponentVisible.ts
│   ├── useDetectSwipe.ts
│   ├── useDisabledTokens.ts
│   ├── useGetLightningInvoiceInfo.ts
│   ├── useInputPriceConversion.ts
│   ├── useIsUsernameValid.ts
│   ├── useLPToUSD.ts
│   ├── useMintCalculation.ts
│   ├── usePoolPriceConversion.ts
│   ├── usePoolsTableData.ts
│   ├── usePrevious.ts
│   ├── useSwapMarketMaker.ts
│   ├── useSwapTokenData.ts
│   ├── useTokenPriceConversion.ts
│   ├── useTokenTableData.ts
│   ├── useUSDConversion.ts
│   ├── useValidateBitcoin.ts
│   ├── useValidateEthereum.ts
│   └── wallet
│       └── useSendInput.tsx
├── models
│   ├── Audits.ts
│   ├── Client.ts
│   ├── Dao.ts
│   ├── Farming.ts
│   ├── Markus.ts
│   ├── Mints.ts
│   ├── Proposals.ts
│   ├── Referrals.ts
│   ├── Stakes.ts
│   ├── Stats.ts
│   ├── Tokens.ts
│   ├── Validators.ts
│   ├── Wallet.ts
│   └── WrappingAdresses.ts
├── next-env.d.ts
├── next.config.js
├── package.json
├── pages
│   ├── [username].tsx
│   ├── _app.tsx
│   ├── _error.js
│   ├── audit.tsx
│   ├── dao
│   │   ├── [proposalId].tsx
│   │   ├── index.tsx
│   │   └── leaderboard.tsx
│   ├── farming.tsx
│   ├── index.tsx
│   ├── mint.tsx
│   ├── sentry_sample_error.js
│   ├── swap.tsx
│   ├── validators.tsx
│   └── wallet
│       └── index.tsx
├── providers
│   ├── AuthProvider.tsx
│   ├── ConnectWalletProvider.tsx
│   ├── NotificationProvider.tsx
│   ├── WalletSendProvider.tsx
│   └── WindowSizeProvider.tsx
├── public
│   ├── android-chrome-192x192.png
│   ├── android-chrome-512x512.png
│   ├── apple-touch-icon.png
│   ├── favicon-16x16.png
│   ├── favicon-32x32.png
│   ├── favicon.ico
│   ├── icons
│   │   ├── anchor-wallet-icon.png
│   │   ├── avatar-chevron-down.svg
│   │   ├── bitcoin-libre-icon.png
│   │   ├── btc-asset-icon.svg
│   │   ├── checkmark-circle.svg
│   │   ├── chevron-down-nav-mobile.svg
│   │   ├── chevron-down.svg
│   │   ├── chevron-left.svg
│   │   ├── chevron-up.svg
│   │   ├── close-icon-white.svg
│   │   ├── close-icon.svg
│   │   ├── copy-icon.svg
│   │   ├── error-status-icon.svg
│   │   ├── failed.svg
│   │   ├── icons_error.svg
│   │   ├── info-outline.svg
│   │   ├── libre-asset-icon.svg
│   │   ├── lightning-icon.svg
│   │   ├── magnifying-glass.svg
│   │   ├── menu-hamburger.svg
│   │   ├── nav-chevron-down-grey.svg
│   │   ├── nav-chevron-down.svg
│   │   ├── passed.svg
│   │   ├── plus-icon.svg
│   │   ├── plus.svg
│   │   ├── slider-thumb.svg
│   │   ├── success-status-icon-white.svg
│   │   ├── swap-icon.svg
│   │   ├── usdt-asset-icon.png
│   │   └── warning-status-icon.svg
│   ├── images
│   │   ├── coins.svg
│   │   ├── mobile.png
│   │   ├── validators.png
│   │   ├── voting-power.png
│   │   └── wheel.png
│   ├── libre-dashboard.png
│   ├── logos
│   │   ├── bitcoin-circle.svg
│   │   ├── libre-circle.svg
│   │   ├── logo-full.svg
│   │   └── logo-icon.svg
│   ├── site.webmanifest
│   └── vercel.svg
├── sentry.client.config.js
├── sentry.properties
├── sentry.server.config.js
├── services
│   ├── APIClient.ts
│   ├── ChainClient.ts
│   ├── LibreClient.ts
│   └── MarkusClient.ts
├── styled.d.ts
├── styles
│   └── globals.css
├── tsconfig.json
├── uikit
│   ├── AutosizeInput.tsx
│   ├── Avatar.tsx
│   ├── BorderlessInput.tsx
│   ├── Button.tsx
│   ├── DetailsBlock.tsx
│   ├── DetailsBlockSkeleton.tsx
│   ├── EmptyInput.tsx
│   ├── EmptyTextarea.tsx
│   ├── HeaderBack.tsx
│   ├── IconPairs.tsx
│   ├── InputSlider.tsx
│   ├── Label.tsx
│   ├── LoadingAnimation.tsx
│   ├── LoadingAppBar.tsx
│   ├── Notification.tsx
│   ├── ProgressBar.tsx
│   ├── SectionEmpty.tsx
│   ├── SectionLoading.tsx
│   ├── Sections.tsx
│   ├── Table.tsx
│   ├── ToggleSwitch.tsx
│   ├── animations
│   │   ├── button-loading-animation.json
│   │   ├── connect-animation.json
│   │   ├── dao-empty-state.json
│   │   ├── invite-animation.json
│   │   ├── safe-animation.json
│   │   └── warning-animation.json
│   └── themeConfig.ts
├── utils
│   ├── constants.ts
│   └── index.ts
└── yarn.lock

51 directories, 302 files
```

---

## 🧩 Modules

<details closed><summary>Root</summary>

| File                    | Summary                                                                                                                                                                                                                                                                                                                                           |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sentry.client.config.js | The code snippet initializes Sentry on the browser by configuring the necessary settings. It sets up the DSN (Data Source Name) for sending error reports to Sentry. The code also specifies the sample rate for capturing performance traces. Additionally, it allows for overriding the automatic release value using an environment variable.  |
| next.config.js          | This code configures Next.js with specific functionalities including support for images from a specific domain, enabling styled components, ignoring TypeScript build errors, and suppressing ESLint errors during builds. It also configures Sentry for error tracking with options to hide source maps and customize the Sentry Webpack plugin. |
| styled.d.ts             | This code snippet extends the "styled-components" module to add custom theme properties. These properties include colors, backgrounds, borders, and text styles for various UI components like buttons, headers, labels, notifications, and progress bars.                                                                                        |
| next-env.d.ts           | The code snippet is a reference file for a Next.js project using TypeScript. It includes type references for Next.js and Next.js image functionality. It serves as a guide and should not be edited directly. For more information, refer to the Next.js documentation on TypeScript integration.                                                 |
| sentry.server.config.js | The code snippet initializes Sentry for server-side usage in Next.js. It configures the Sentry DSN, sets the traces sample rate, and initializes Sentry with the provided DSN or a default one.                                                                                                                                                   |

</details>

<details closed><summary>Uikit</summary>

| File                     | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| EmptyTextarea.tsx        | The code snippet defines a styled textarea component with optional error styling. It sets default styles for the textarea, including padding, border, background color, font size, and placeholder color. When an error prop is passed to the component, the border and outline color change to a specified error color. The component also includes a wrapper div and accepts props for placeholder, value, onChange, and error.                                                                                                 |
| themeConfig.ts           | This code snippet defines a set of colors and creates a default theme using Styled Components. The theme includes color values for various elements such as buttons, headers, backgrounds, and labels. It also defines color variations for different states like warnings and errors.                                                                                                                                                                                                                                            |
| HeaderBack.tsx           | The code snippet is a React component that renders a styled "Back" button with a left chevron icon. It takes a "link" prop as input and creates a clickable area that navigates to the specified link when clicked. The button is displayed as a flex container with a cursor pointer. The text "Back" is styled with specific font properties and a custom color.                                                                                                                                                                |
| LoadingAnimation.tsx     | The code snippet defines a styled component called Spinner that displays a rotating loading animation. The animation can have different sizes (tiny, small, medium, or large) and can be customized with a color in RGB format. The component takes the size and colorRGB as props and renders the Spinner component with the specified size and color.                                                                                                                                                                           |
| IconPairs.tsx            | This code snippet defines a React component called `IconPairs`. It renders a wrapper div with a flexible layout and aligns its child images. The size of the images can be customized using the `iconSize` prop. The component accepts up to three image sources (`imgSrc1`, `imgSrc2`, `imgSrc3`) and renders them accordingly.                                                                                                                                                                                                  |
| BorderlessInput.tsx      | The provided code snippet defines a styled input component called "BorderlessInput" using React and styled-components. It accepts props for placeholder, type, value, onChange, onFocus, and error. The input dynamically adjusts its width based on the length of the input value and applies different styles for error and left padding.                                                                                                                                                                                       |
| Label.tsx                | The provided code defines a reusable component called "Label". It takes in color, backgroundColor, and children props. The component renders a styled container div with a specified backgroundColor, and a styled span containing the children text with a specified color. The Label component encapsulates these elements and allows for easy customization of text and background colors.                                                                                                                                     |
| ProgressBar.tsx          | The provided code snippet defines a progress bar component using styled-components. It consists of a Wrapper component that sets the background and dimensions of the progress bar, and a Progress component that sets the color and width based on the provided percentage and color props. The ProgressBar component combines these two components to render a progress bar.                                                                                                                                                    |
| DetailsBlock.tsx         | The provided code snippet defines a `DetailsBlock` component that displays a title, details, and an optional image. It also includes conditional rendering for a loading state and an optional info icon. The component is styled using styled-components and has responsive design for different screen sizes.                                                                                                                                                                                                                   |
| Sections.tsx             | The code snippet defines styled components for a section wrapper and section title. The section wrapper has padding, border, and background color properties, and adjusts its padding for small screens using a media query. The section title has font size, weight, color, and display properties.                                                                                                                                                                                                                              |
| Avatar.tsx               | The code snippet defines a functional component called "Avatar" that generates a colored avatar image based on the account name and size provided. The account name is used to determine the color scheme of the avatar. The component renders a div element with the specified size and rounded shape, and an image inside it using the Next.js Image component. The avatar image is fetched from a CDN using a constructed URL based on the color scheme and the first letter of the account name.                              |
| SectionEmpty.tsx         | The code snippet defines a styled component called `SectionEmpty` that renders a container div and a text span. The `SectionEmpty` component takes an optional `text` prop, which is displayed in the text span. If the `text` prop is not provided, a default message of "No data available" is displayed.                                                                                                                                                                                                                       |
| EmptyInput.tsx           | The code snippet defines a styled component called `EmptyInput` which renders an input field. It accepts various props such as placeholder, type, value, onChange, error, hasLeft, and hasRight. It applies custom styles to the input based on the props provided.                                                                                                                                                                                                                                                               |
| AutosizeInput.tsx        | The provided code snippet is a React component called "AutosizeInput" that creates an input field with some additional functionalities. It allows the user to input text or numbers, and automatically resizes the width of the input field based on the length of the input value. It also provides an option to add a label and a unit element to the input field. The component handles input changes, focus events, and updates the styling dynamically based on the input value length.                                      |
| LoadingAppBar.tsx        | This code snippet defines a loading animation component with a slider that includes a line and two sublines. The sublines have different animations: one increases in size, while the other decreases. The component uses styled-components and React to create and animate the loading animation.                                                                                                                                                                                                                                |
| ToggleSwitch.tsx         | The code provides a ToggleSwitch component that renders a toggle button with customizable options, labels, colors, and initial selected index. It utilizes the SwitchSelector component from the react-switch-selector library and styled-components for styling.                                                                                                                                                                                                                                                                 |
| Table.tsx                | The provided code snippet includes various functionalities:-Defines constants for row height and header height.-Provides styles for different table columns.-Defines styled components for the table, table title, table header, table cell, and empty table rows.-Implements functions for resolving column width and table height based on data.-Implements custom header and cell renderers for the table.-Provides a custom hook for retrieving table information such as row styles, table height, and rendering empty rows. |
| Notification.tsx         | This code snippet defines a styled notification component that displays an icon and text based on the provided type. The component uses the "next/image" package to render the icons and "styled-components" for styling. The component accepts a type ("warning", "error", or "success"), text, and an optional onClick function.                                                                                                                                                                                                |
| SectionLoading.tsx       | The code snippet creates a styled container component called SectionLoading, which displays a loading animation in the center of the container. The LoadingAnimation component is imported and rendered within the SectionLoading component.                                                                                                                                                                                                                                                                                      |
| Button.tsx               | The code snippet provides reusable components for small and medium buttons. These buttons can have different styles, colors, and animations. They can also be disabled and display a loading animation when isLoading is true. The buttons are styled using styled-components and use the react-lottie library for animations.                                                                                                                                                                                                    |
| InputSlider.tsx          | The provided code snippet is a React component called `InputSlider` that renders a slider input with accompanying text and value display. It uses the `styled-components` library for styling and the `react-slider` library for the slider functionality. The component allows for customization of the initial value, minimum and maximum values, step size, error state, and event handling. It also includes an optional numeric input field.                                                                                 |
| DetailsBlockSkeleton.tsx | This code snippet defines styled components for rendering skeleton loading placeholders. "Item" and "Img" components have animations that create a shimmer effect, simulating content loading. The "DetailsBlockSkeleton" component renders either an "Img" component or an "Item" component based on the presence of an image.                                                                                                                                                                                                   |

</details>

<details closed><summary>Providers</summary>

| File                      | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| WindowSizeProvider.tsx    | The code snippet creates a React context that provides information about the window size. It determines whether the window is a small mobile, mobile, tablet, laptop, or desktop based on the window width. The context is provided by a component that calculates the window size and renders its children with the context value.                                                                                                                            |
| WalletSendProvider.tsx    | This code snippet is a React context provider that manages the state and functionality for sending tokens from a wallet. It handles selecting a token, recipient, amount, and fee calculation. It also provides methods for opening the send modal, resetting the state, and submitting the send transaction. The context value is provided to its children components. The snippet also imports various hooks and components for use in the context provider. |
| ConnectWalletProvider.tsx | The code snippet provides a context and provider for managing the state of a connect wallet modal. It includes functions to open and close the modal, and the provider wraps the provided children components.                                                                                                                                                                                                                                                 |
| AuthProvider.tsx          | The code defines an authentication context and provider using React hooks and Next.js router. It manages the current user state, handles login and logout functionality, and provides access to the authentication context throughout the application. The code also includes useEffect hooks to restore the user session and update the current user based on the router's query parameters.                                                                  |
| NotificationProvider.tsx  | This code snippet is a Tech Lead that handles notifications in a React application. It provides a context and a provider component for managing the state of notifications. It includes animation, styling, and logic for displaying and closing notifications after a certain duration. The notifications can contain a link that opens a URL.                                                                                                                |

</details>

<details closed><summary>Utils</summary>

| File         | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| constants.ts | The provided code snippet defines various constants and configurations for the application. It includes constants for balance, token precision, proposal contracts, media query widths, and different chain configurations based on the environment. It also defines an interface for chain configurations and returns a chain configuration object based on the environment variables.                                                   |
| index.ts     | This code snippet provides various utility functions for formatting and manipulating data, such as converting timestamps, formatting numbers and prices, generating random proposal IDs, and determining proposal statuses. It also includes functions for rendering vote percentages and labeling colors based on status. Additionally, there are functions related to currency conversion, symbol finding, and counting decimal places. |

</details>

<details closed><summary>Models</summary>

| File                | Summary                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Wallet.ts           | The code snippet provides two interface definitions: ISearchUser and ISendRecipient. ISearchUser represents properties related to user search, such as code, count, payer, scope, and table. ISendRecipient represents properties related to sending recipients, including destination and type (with available options being "bitcoin", "lightning", "libre", or "ethereum").                                                       |
| Farming.ts          | The code snippet defines two interfaces: IUserFarming and IFarmingStats, and exports an emptyUserFarming object. These interfaces define the structure and types of properties for user farming and farming stats. The emptyUserFarming object initializes the properties with empty values.                                                                                                                                         |
| Proposals.ts        | The code snippet provides interfaces for proposal status, proposal details, and voting. It includes definitions for proposal properties such as approver, funding account, and voting thresholds. The code aims to facilitate proposal management and decision-making processes within a larger system.                                                                                                                              |
| Dao.ts              | The code snippet defines two interfaces: IDaoStats representing statistics of a DAO (total BTC, total Libre, total USDT, voting power), and IUserDaoPower representing a user's DAO power (account, last update, voting power).                                                                                                                                                                                                      |
| Stats.ts            | The provided code snippet defines several interfaces that represent different statistics related to an application. These include application stats like number of accounts and referrals, stake stats like total staked amount and annual percentage yield (APY), airdrop stats like number of activated and claimed airdrops, and mint stats like BTC contributed and mint bonuses.                                                |
| Markus.ts           | The code snippet defines an interface called IMarkusOffer, which includes properties for a market maker, offer ID, offered amount (including quantity and contract), and order ID.                                                                                                                                                                                                                                                   |
| Stakes.ts           | The code snippet provides TypeScript interfaces for a stake and stake statistics. It includes the stake details such as account, stake date, stake length, bonus, staked amount, APY, payout details, and status. The stake status is defined using an enum. The stake statistics interface includes the balance, earned amount, and staked amount.                                                                                  |
| WrappingAdresses.ts | The code snippet defines three interfaces: ILightningWrappingAddress for representing lightning addresses, IBitcoinWrappingAdresses for representing bitcoin addresses, and IWrappingAdresses for representing both bitcoin and lightning addresses.                                                                                                                                                                                 |
| Tokens.ts           | The provided code snippet includes multiple interfaces and constants related to token information and functionality, such as token properties, pool information, user data, token transfer details, liquidity token contracts and pairs, token image sources, and display token precisions. These interfaces and constants allow for the management and manipulation of token-related data in a comprehensive and structured manner. |
| Client.ts           | The code snippet defines an enum WalletType with two options: Libre and Anchor.                                                                                                                                                                                                                                                                                                                                                      |
| Validators.ts       | The code snippet defines two interfaces: IValidators, which represents a validator with properties like rank, name, location, totalVotes, and percentage; and IValidatorVote, which represents a vote for a specific validator by storing the votedFor property.                                                                                                                                                                     |
| Mints.ts            | The code snippet defines two interfaces: 1. IMint represents a mint with various properties such as account details, BTC contributed, contribution date, minting statistics, stake duration, and status.2. IMintStats represents minting statistics with properties for total contributions, total minted amount, and percentage minted.                                                                                             |
| Referrals.ts        | The code snippet provides three interfaces: IReferralStats for tracking referral statistics, IMyReferral for personal referral data, and ILeaderboardReferral for leaderboard information. These interfaces define the core functionalities of the referral system, including tracking earnings, rankings, and number of referred users.                                                                                             |
| Audits.ts           | The provided code snippet defines a set of token names and their corresponding attributes. It also defines interfaces for audit stats and audit lists, which contain information about token addresses, balances, and last update timestamps.                                                                                                                                                                                        |

</details>

<details closed><summary>Styles</summary>

| File        | Summary                                                                                                                                                                                                                                                                                                                                 |
| ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| globals.css | This code snippet defines the CSS styling for a web page. It sets the font family, padding, margin, and height of the body element. It also styles the textarea, input number spinner, and anchor elements. The #\_\_next selector sets the width and height to 100%. The code uses a combination of system fonts and the Poppins font. |

</details>

<details closed><summary>Components</summary>

| File                  | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| --------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| TokenWithUSDValue.tsx | This code snippet defines a React component called "TokenWithUSDValue" that displays a token value along with its equivalent value in USD. It uses the "useExchangeRates" hook to fetch exchange rate data and convert the token value. The component also handles cases where the token value is zero or missing. It is styled using styled-components.                                                                                                                                                               |
| NavBar.tsx            | The provided code snippet defines the functionalities of a header navigation bar component in a web application. It includes features such as logo display, responsive menu, active item highlighting, and a connect wallet button. The code also handles responsive behavior for different screen sizes.                                                                                                                                                                                                              |
| ConnectWallet.tsx     | This code snippet defines the UI component "ConnectWallet" which displays either the user's name and a dropdown menu if the user is logged in, or a "Connect Wallet" button if the user is not logged in. The component uses various styled components, hooks, and context providers to handle user authentication and display the appropriate UI elements.                                                                                                                                                            |
| PageTitle.tsx         | This code snippet defines a component called "PageTitle" that displays a title and an optional description. It uses styled components to set the styles for the title and description. The component is responsive, adjusting its styles based on the screen width.                                                                                                                                                                                                                                                    |
| CharacterCount.tsx    | This code snippet defines a React component called `CharacterCount` that displays the count of characters in a provided `value` prop. It also accepts a `max` prop to determine the maximum allowed characters. The count is rendered within a styled wrapper and a styled span element. The count is updated using the `useMemo` hook to only recalculate when the `value` prop changes.                                                                                                                              |
| AddBalanceModal.tsx   | The provided code snippet is a React component that renders a modal for adding balance to a wallet. It allows the user to switch between Bitcoin and Lightning Network payment methods. The component uses styled-components for styling and displays different content based on the selected payment method. The modal also includes a notification with information about cross-chain fees. The component is conditionally rendered based on the "open" prop and includes a callback function for closing the modal. |
| ConnectModal.tsx      | This code snippet defines a modal component called "ConnectModal" that displays a list of wallet options for the user to connect. It utilizes various libraries like Next.js, styled-components, and react. The modal is conditionally rendered based on the "open" prop, and the selected wallet type is handled through the "handleLogin" function. The component also checks if it is opened from a specific app and renders a different option accordingly.                                                        |
| AppModal.tsx          | This code snippet defines a reusable AppModal component that displays a modal overlay with a title, content, and close button. It uses styled-components for styling and next/image for displaying an image. The component is rendered as a portal to a separate HTML element on the page. The modal is shown or hidden based on the "show" prop, and the "onClose" prop is called when the close button is clicked.                                                                                                   |
| HeaderDetails.tsx     | The code snippet defines a component called HeaderDetails that displays information related to BTC, LIBRE, and TVL. It uses styled components for styling, next/image for displaying icons, and custom hooks for fetching data. The component renders a wrapper div containing multiple Item divs, each displaying a label and an amount. The amounts are dynamically fetched from exchangeRates and tokenData.                                                                                                        |
| PageSubtitle.tsx      | This code snippet defines a styled component called `Title` that renders an `h1` element with custom styles. The `PageSubtitle` component receives a `title` prop and renders the `Title` component with the provided title as its content.                                                                                                                                                                                                                                                                            |
| MobileMenu.tsx        | The provided code snippet is a React component called "MobileMenu" that creates a responsive mobile menu. It uses styled-components for styling and imports various dependencies such as "next/router" and "react" for routing and context management. The menu options are rendered based on the user's authentication status and the current route. It also includes functions for handling menu item clicks, connecting or disconnecting a wallet, and logging out.                                                 |

</details>

<details closed><summary>Stake</summary>

| File                      | Summary                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| StakeModalSlider.tsx      | The provided code snippet defines a React component called StakeModalSlider. It renders a slider with accompanying text and input field. The slider allows the user to select a value within a specified range, while the input field allows direct value input. The selected value is stored in state and can be updated through user interaction. The component also accepts props to customize its appearance and behavior. |
| EmergencyUnstakeModal.tsx | The code snippet is a React component called "EmergencyUnstakeModal" that renders a modal window. It includes a warning animation, a title, a description text, and a button for confirming and triggering a function called "handleSubmit". The component utilizes styled-components for styling and the react-lottie library for displaying the animation. It also imports and uses a custom hook called "useUnstakedLibre". |
| StakeModal.tsx            | This code snippet is for a StakeModal component in a React application. It allows the user to input the amount and duration of a stake and calculates the APY (Annual Percentage Yield) and payout based on these inputs. The component also handles user authentication, token balances, and submission of the stake.                                                                                                         |

</details>

<details closed><summary>Mint</summary>

| File                   | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ---------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| MintTable.tsx          | The provided code snippet is a React component called "MintTable". It displays a table of user contributions, including information such as contribution date, PBTC contributed, lock-up period, % of total PBTC, mint APY, and LIBRE minted. The table is rendered using the react-virtualized library to optimize performance, and it supports infinite scrolling to load more rows. The component also includes styling elements and hooks to fetch and handle data. |
| MintModal.tsx          | The code snippet is a React component for a "MintModal" that renders a modal window for contributing to a Mint. It uses various hooks to manage state and data fetching. The modal includes input sliders for selecting the contribution amount and lock-up duration, and displays information such as the claim date, base APY, multiplier, and final APY. It also handles user authentication and provides options for connecting a wallet or making a contribution.  |
| EmptyContributions.tsx | This code snippet defines a React component called "EmptyContributions" that displays a message and animation when a user has no contributions. It uses styled-components for styling, and the ConnectWalletContext for handling the "Contribute Now" button click event. The component also utilizes the Lottie library to render an animation.                                                                                                                        |
| MyAvailableBTC.tsx     | The code snippet fetches the user's BTC token amount, displays it as "My Available PBTC" in a DetailsBlock component, and uses an image of the Bitcoin logo.                                                                                                                                                                                                                                                                                                            |

</details>

<details closed><summary>Referrals</summary>

| File                         | Summary                                                                                                                                                                                                                                                                                                                                                                     |
| ---------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ReferralLeaderboardTable.tsx | This code snippet is a React component that renders a referral leaderboard table. It fetches data from an API using a custom hook, displays the data in a virtualized table, and supports infinite scrolling for loading more rows. The table has columns for rank, libre name, and number of people joined.                                                                |
| useMyReferrals.ts            | This code snippet defines a custom hook "useMyReferrals" that utilizes React Query to fetch and manage data related to the user's referrals. It makes an API call using the provided name as a parameter and returns the query result, including the fetched data or any error occurred during the request.                                                                 |
| useRefferalStats.ts          | The code snippet utilizes the React Query library to fetch referral statistics using an API client. It provides a hook called useReferralStats that returns the query result and handles fetching the data asynchronously. The result is typed to return IReferralStats data or an Error.                                                                                   |
| keys.ts                      | The code snippet defines constants for different types of API requests related to referral stats, user's referrals, and leaderboard referrals.                                                                                                                                                                                                                              |
| useLeaderboardReferrals.ts   | The provided code snippet uses the react-query library to fetch referral leaderboard data from an API. It defines a function to make the API request, and then exports a custom hook that utilizes the useQuery hook from react-query to handle the data fetching and caching logic. The hook returns the query result, which includes the fetched data or an error object. |

</details>

<details closed><summary>Swap</summary>

| File                        | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| --------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| SwapButton.tsx              | The code snippet is a React component called "SwapButton". It renders a button with different functionalities based on certain conditions. If there is no logged-in user, it displays a "Connect Wallet" button. If the selected token pair is unavailable or an invalid amount is entered, it displays a disabled button with an appropriate message. Otherwise, it displays a "Swap now" button.                                                                                                                          |
| LiquidityDetailsBoxItem.tsx | The provided code snippet is a React component that represents a box displaying details about liquidity. It includes functionalities such as updating liquidity values based on a slider input, handling user authentication and connection to a wallet, and executing a remove liquidity action. The component also handles rendering UI elements such as buttons, sliders, and notifications.                                                                                                                             |
| SwapTokenDetailsBox.tsx     | The code snippet is a React component that renders a details box with two rows of information. It uses styled-components to apply custom styles. The component receives props for slippage and isFetchingPrice. It fetches token data using a custom hook and displays the data if available. It also shows a loading animation if isFetchingPrice is true.                                                                                                                                                                 |
| SwapAmountInput.tsx         | The code snippet defines a React component called "SwapAmountInput" that renders an input field and a "Max" button. It allows the user to input a value and triggers a callback function when the value changes. It also handles the case when the user clicks on the "Max" button, setting the value to either the maximum allowed value or the available unstaked token value. The component supports various props like token, handleSubmit, type, enabled, showMax, initialValue, and maxValue.                         |
| LiquidityButton.tsx         | This code snippet defines a React component called "LiquidityButton". It takes in several props related to token values, user authentication, and error messages. Based on the provided props, the component renders different variations of a "MediumButton" component. If the user is not authenticated, it renders a button to connect the wallet. If certain conditions related to token values are met, it renders buttons with specific error messages. If there are no errors, it renders a button to add liquidity. |
| SwapBox.tsx                 | The provided code snippet is a React component called "SwapBox" that represents a user interface for swapping tokens. It includes various functionalities such as token selection, input fields for token amounts to swap, price estimation, token balance display, token swapping, and error handling. The component uses several custom hooks and styled components for styling and layout.                                                                                                                               |
| LiquidityDetailsBox.tsx     | The code snippet is a React component that displays the liquidity details of a user. It fetches data regarding the user's liquidity and pool supply from the backend API using custom hooks. It then processes the data and renders a styled component that shows the user's liquidity details in a table format. The component also includes some conditional rendering logic to handle cases where there is no data or the user has no balance.                                                                           |
| CurrencySelector.tsx        | This code snippet is a React component called CurrencySelector. It is a dropdown menu used to select a currency token. The component receives a list of tokens and an active token as props. It renders the active token with its symbol and displays a dropdown menu of other tokens. When a token is selected from the dropdown, the onChange callback is called. The component also handles some logic for disabling certain tokens and managing the visibility of the dropdown menu.                                    |
| LiquidityBox.tsx            | This code snippet is a React component that represents a Liquidity Box. It allows users to add liquidity to a pool and receive LP tokens in return. The component includes UI elements such as input fields, buttons, and selectors for selecting tokens and entering token amounts. It also uses various hooks and custom functions to handle user interactions, perform calculations, and make API calls. The component is styled using CSS-in-JS with styled-components library.                                         |
| SwapPrice.tsx               | The provided code snippet defines a component called "SwapPrice" that displays the best price for a token swap. It uses styled components for styling and a custom hook called "useExchangeRates" to fetch exchange rate data. The component renders different content based on the active tokens and whether the price is being fetched.                                                                                                                                                                                   |

</details>

<details closed><summary>Dao</summary>

| File                          | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| DaoSkeleton.tsx               | The code snippet uses the styled-components library to define and style various components. It creates a skeleton layout with animated shimmer effect and displays a title, label, description, bar, and number components. These components are styled with specific dimensions and visual properties. The code exports the "Item" and "DaoSkeleton" components for use in other parts of the application.                                                                                                                                                         |
| DaoModal.tsx                  | This code snippet is a React component called `DaoModal` that represents a modal for creating proposals. It uses various hooks and styled components for rendering UI elements such as inputs, buttons, and notifications. The component manages state for input values and error flags, and handles user interactions such as input changes and form submission. It also performs validation on user input and makes API calls to create proposals.                                                                                                                |
| ProposalListItem.tsx          | This code snippet defines a React component called `ProposalListItem` that renders a single item in a list of proposals. It includes styled components for layout and styling, and uses various utility functions to format and render different aspects of each proposal item, such as its status, votes, and expiration time.                                                                                                                                                                                                                                     |
| GovernanceOverviewSection.tsx | The code snippet imports necessary dependencies, defines a styled component, and renders a governance overview section displaying details about a DAO's voting power, treasury value, and asset holdings. It utilizes the useGovernanceOverviewData hook to fetch data and display it in DetailsBlock components. The section also includes icons for different assets.                                                                                                                                                                                             |
| TopAccountsTable.tsx          | The code snippet is a React component that renders a table of top accounts data. It uses react-virtualized and styled-components libraries for styling and optimization. The table dynamically loads more rows as the user scrolls. If there is no data available, it displays an empty section. The table columns include rank, user, voting power, percentage of total, and proposals voted.                                                                                                                                                                      |
| MyVotingPowerSection.tsx      | The code snippet is a React component that renders a section displaying the user's voting power information. It uses styled-components for styling and imports various hooks and UI components. The section includes a header with a title and description, and a button to create a proposal if the user is logged in. The section also displays details such as the user's voting power, percentage of voting power, and available LIBRE tokens.                                                                                                                  |
| TopAccountsSection.tsx        | This code snippet defines a component called "TopAccountsSection" that displays a title and a table of top accounts by voting power. It also includes a button to navigate to the full leaderboard page. The styling is done using styled-components library and an animation from a JSON file is displayed in the empty state.                                                                                                                                                                                                                                     |
| ViewMore.tsx                  | The code snippet uses styled-components library to create a reusable component called ViewMore. It renders a div with flex properties and hover animation. It also renders a span with text content and a styled arrow. The component accepts props for click event, arrow direction, and text content.                                                                                                                                                                                                                                                             |
| DaoProposals.tsx              | This code snippet is a React component that renders a list of recent proposals. It fetches the proposal data using a custom hook called `useProposals`. While the data is loading, it displays a loading skeleton. If there is no data available, it shows an empty state with an animation and a button to create a new proposal. The proposals are sorted by their expiration date and displayed in descending order. By default, only the top 5 proposals are shown, but there is an option to show all proposals by clicking on a "View More Proposals" button. |
| useVotingPowerData.ts         | This code snippet utilizes various custom hooks to fetch and manipulate data related to a user's voting power, DAO stats, and user tokens. It formats and returns the relevant data along with a loading state.                                                                                                                                                                                                                                                                                                                                                     |
| useGovernanceOverviewData.ts  | This code snippet retrieves data from two different queries: `useDaoStats` and `useExchangeRates`. It calculates the total value based on the retrieved data and returns it along with other formatted values. The function also returns loading status for both queries.                                                                                                                                                                                                                                                                                           |
| useTopAccountsData.ts         | The code snippet exports a custom hook called useTopAccountsData. It fetches data on DAO accounts and DAO stats using separate custom hooks. It then sorts the account data by voting power in descending order, filters out any accounts with zero voting power, and formats the data into a desired result object. The hook returns the formatted result along with isLoading and isFetched flags.                                                                                                                                                                |
| useDaoStats.ts                | The code snippet uses the React Query library to fetch and manage data related to DAO statistics. It defines a function called `getDaoStats` that retrieves data from a remote API and returns an object containing various statistics. It also exports a custom hook called `useDaoStats` that uses `useQuery` to handle the data fetching logic and returns the result of the query.                                                                                                                                                                              |
| keys.ts                       | The code snippet defines three constants that represent different functionalities: "daoStats" for DAO statistics, "userDaoPower" for user DAO power, and "daoAccounts" for DAO accounts.                                                                                                                                                                                                                                                                                                                                                                            |
| useDaoAccounts.ts             | The code snippet uses the React Query library to fetch and manage data for DAO accounts. It defines a function called useDaoAccounts which uses the useQuery hook to call the getDaoAccounts function. The getDaoAccounts function fetches DAO power using the LibreClient service. The useQueryResult is returned, providing the result and error state of the query.                                                                                                                                                                                              |
| useUserDaoPower.ts            | This code snippet defines a custom hook called useUserDaoPower, which is responsible for fetching and returning a user's Dao power. It uses the react-query library to handle the data fetching and caching. The getUserDaoPower function makes an API call to retrieve the user's Dao power based on the account name provided.                                                                                                                                                                                                                                    |
| index.tsx                     | This code snippet is a Next.js page component that renders various sections of a DAO (Decentralized Autonomous Organization) dashboard. It includes components for governance overview, user's voting power, proposals, and top accounts. It also handles opening and closing of a modal for additional DAO functionality.                                                                                                                                                                                                                                          |
| leaderboard.tsx               | This code snippet is a React component that renders a leaderboard page for a DAO (Decentralized Autonomous Organization). It imports various components, styles them using styled-components, and fetches DAO statistics using a custom hook. The rendered page includes a page title, a header with a back button, a total voting power display, and a table of top accounts by voting power.                                                                                                                                                                      |
| [proposalId].tsx              | This code snippet is implementing the functionality for displaying and interacting with the details of a proposal in a DAO (Decentralized Autonomous Organization). It fetches data related to the proposal, such as votes and proposal properties, and allows users to vote for or against the proposal. The code also includes styling and UI components for rendering the proposal details and vote progress.                                                                                                                                                    |

</details>

<details closed><summary>Dashboard</summary>

| File                  | Summary                                                                                                                                                                                                                                                                                                                                       |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| LeftAnnouncement.tsx  | This code snippet is a React component that renders a styled announcement container. It includes a title, description, and a button that links to an external URL. The container has responsive styles and background image. The button triggers a function on click. The component is specifically designed for a left-aligned announcement. |
| Announcements.tsx     | This code snippet provides a functional component called "Announcements" that renders left and right announcements. It also includes functionality for detecting swipe gestures and updating the active announcement accordingly. The component is responsive and adjusts its layout based on the screen size.                                |
| RightAnnouncement.tsx | The code snippet defines a React component called `RightAnnouncement` that renders a styled announcement box. The box contains a title, description, and a button that links to a Discord server. The component is styled using styled-components library and follows responsive design principles.                                           |

</details>

<details closed><summary>Pools</summary>

| File             | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| PoolsTable.tsx   | The provided code snippet is a React component that renders a table using the React Virtualized library. It fetches data using the `usePoolsTableData` hook and displays it in a table format. The table has columns for pairs, APY, total liquidity pool, and farmed percentage. It supports infinite scrolling and dynamic table size. The table cells are styled using CSS-in-JS with different colors and icons based on the data. |
| PoolsSection.tsx | This code snippet defines a component called "PoolsSection" that renders a styled wrapper with a title and a table component called "PoolsTable". The wrapper has specific styles for padding, border, and background color. The title is styled with a specific font size, weight, and color.                                                                                                                                         |

</details>

<details closed><summary>Chainactivity</summary>

| File                     | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ChainActivitySection.tsx | The provided code snippet is a React component that renders a section called "Chain Activity." It utilizes the styled-components library to style the different elements within the section, including a wrapper div, a header div with a title, and a table component for displaying chain activity data. The section is enclosed within a wrapper div with specific padding, border, and background color styles. The title is styled with a specific font size, weight, and color. Overall, this code snippet creates a styled section with a title and a table for displaying chain activity. |
| ChainActivityTable.tsx   | The code snippet is a React component that renders a table using the react-virtualized library. It fetches data using a custom hook, formats the data, and displays it in the table with specified column labels and cell renderers. It also implements infinite scrolling to load more rows as needed. The table is responsive and adjusts its width based on the available space.                                                                                                                                                                                                               |

</details>

<details closed><summary>Tokens</summary>

| File                             | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| -------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| TokenTable.tsx                   | This code snippet is a React component that renders a table of tokens. It uses the react-virtualized library for efficient rendering of large data sets. The table displays information about each token, including its name, circulating supply, market cap, and staked percentage. It also includes custom cell renderers for displaying icons and formatting data. The component fetches token data using a custom hook and handles loading and error states. |
| TokensSection.tsx                | The code snippet defines a TokensSection component that renders a wrapper div with a header containing a title. It also renders the TokenTable component. The styling is done using styled-components. The TokensSection component encapsulates the core functionalities of displaying tokens and their associated table.                                                                                                                                        |
| useCacheBTCAddress.ts            | The code snippet uses the React Query library and custom hook "useMutation" to cache a Bitcoin address by making a mutation request using the ChainClient service.                                                                                                                                                                                                                                                                                               |
| useTokens.ts                     | This code snippet utilizes the React Query library to fetch tokens from an API. It defines a function, getTokens, which retrieves token data from the API using APIClient. The useTokens function uses the useQuery hook from React Query to execute the getTokens function and return the result. The result is stored in a UseQueryResult object, which allows for handling of loading, error, and data states.                                                |
| useRegisterLightningPayment.ts   | The code snippet utilizes the "useMutation" hook from the React Query library to register a lightning payment. It calls the "registerLightningPayment" function from the ChainClient service, passing a payload as an argument.                                                                                                                                                                                                                                  |
| useAddLiquidity.ts               | The code snippet imports and utilizes the react-query library to handle mutations for adding liquidity. It uses the LibreClient service to execute the addLiquidity function. Upon successful execution, it triggers the invalidation of specified queries after a 1-second delay.                                                                                                                                                                               |
| useTokenInformation.ts           | This code is importing the necessary dependencies for using react-query and defines a function to fetch token information from the LibreClient service. It also exports a custom hook named "useTokenInformation" that uses react-query to fetch and manage the token information data.                                                                                                                                                                          |
| useSwapTokens.ts                 | The code snippet defines a custom hook called "useSwapTokens" that utilizes React Query. It facilitates token swapping functionality by making a mutation request to a remote API using the "LibreClient" service. Upon a successful swap, it triggers a notification, invalidates certain query caches, and adds a delay for API updates.                                                                                                                       |
| usePoolSupply.ts                 | The code snippet is a custom hook called usePoolSupply that uses React Query to fetch pool supply data from an external API using the getPoolSupply function. It returns the query result along with any potential errors. The IPoolSupply type represents the structure of the data returned from the API.                                                                                                                                                      |
| useClaimMintRush.ts              | The provided code snippet is a React custom hook that uses the react-query library to handle the claim of a "Mint Rush" in a farming application. It triggers a mutation to the LibreClient API, and upon success, updates the query cache by invalidating relevant queries. It also notifies the user with a transaction ID through the NotificationContext.                                                                                                    |
| useUserMintRush.ts               | The provided code snippet is for a React Hook called "useUserMintRush" that uses the react-query library. It fetches data from a remote API using getUserMintRush function, which takes an accountName as a parameter. The fetched data is then returned as a result of the hook, along with an error object if any.                                                                                                                                             |
| useGetBitcoinWrappingAddress.ts  | This code snippet utilizes the `react-query` library to fetch a Bitcoin wrapping address from an API client. It exports a hook `useGetBitcoinWrappingAddress` that wraps the query, returning the result and any potential errors.                                                                                                                                                                                                                               |
| useUserLiquidity.ts              | This code snippet is a custom React hook called "useUserLiquidity" that uses the react-query library. It fetches user liquidity data from a remote server using the LibreClient service and provides the data and error handling through the UseQueryResult object.                                                                                                                                                                                              |
| keys.ts                          | The code snippet defines constants for various request endpoints in a system. These endpoints include tokens, user tokens, wrapping addresses, token information, pool supply, user pool supply, user mint rush, and user BTC address.                                                                                                                                                                                                                           |
| useTransferTokens.ts             | The provided code snippet is a custom hook called `useTransferTokens` that utilizes `react-query` to handle a mutation for transferring tokens. It sends a request to the `LibreClient` API to transfer tokens based on the provided payload. On success, it triggers a notification with the transaction ID, invalidates the cache for token information and user tokens, and adds a delay of 1 second to allow the API to update.                              |
| useRemoveLiquidity.ts            | The code snippet defines a custom hook called `useRemoveLiquidity` that utilizes `react-query` hooks for handling a mutation to remove liquidity. It uses the `LibreClient` service to make the API call and updates query caches using `queryClient.invalidateQueries` on successful completion.                                                                                                                                                                |
| useGetLightningFee.ts            | The provided code snippet is a custom React hook that utilizes the `useMutation` function from the "react-query" library. It fetches the lightning fee using the `ChainClient.fetchLightningFee` method, taking an `invoice` and an `accountName` as parameters. This hook can be used to handle the asynchronous logic for fetching the lightning fee in a React component.                                                                                     |
| useUserTokens.ts                 | This code snippet is a React custom hook that fetches and manages user tokens. It makes use of the React Query library to handle data fetching and caching. The hook retrieves tokens belonging to a specific account, combines token data with stake data, filters and sorts the tokens, and adds additional properties to the token objects. The hook also includes options for automatic refetching and notification on change.                               |
| useTransferToAddress.ts          | The provided code snippet defines a custom hook called "useTransferToAddress" which facilitates transferring tokens to an address. It utilizes react-query to handle mutations and trigger queries to update token information and user tokens. It also incorporates a notification feature and adds a delay to allow the API to update before executing the success callback.                                                                                   |
| useUserBTCAddress.ts             | The code snippet uses the React Query library to create a custom hook, useUserBTCAddress. This hook utilizes the useMutation function to fetch a user's BTC address from an API client. The accountName parameter is passed to the fetchUserBTCAddress function.                                                                                                                                                                                                 |
| useGetLightningWrappingAdress.ts | The provided code snippet is for fetching lightning wrapping addresses using React Query. It includes a function to make an API call and a custom hook to handle the query state and data retrieval.                                                                                                                                                                                                                                                             |

</details>

<details closed><summary>Emptystates</summary>

| File                   | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ---------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| EmptyConnectWallet.tsx | This code snippet is a React component called "EmptyConnectWallet". It renders a wrapper div with a border and background color, containing an image, a title, a description, and a connect button. The component uses styled-components for styling and react-lottie for rendering animations. It also imports and uses a ConnectWalletContext from a provider. The handleLogin function is called when the connect button is clicked. |

</details>

<details closed><summary>Audit</summary>

| File                       | Summary                                                                                                                                                                                                                                                                                                                                                                                             |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| OnChainValidationTable.tsx | The provided code snippet is a React component that renders a table using the react-virtualized library. It fetches data from an API endpoint using the useAuditList hook, and displays the data in a table format with columns for "PNetwork BTC Address" and "Balance Amount". It also includes functionality for sorting and rendering the table efficiently by using virtualization techniques. |
| ProofOfAssetsTable.tsx     | The provided code snippet is a React component that renders a table displaying audit statistics for proof of assets. It fetches data using a custom hook and then uses React Virtualized components to render the table. The component also includes styling and custom rendering logic for the table headers and cells.                                                                            |
| useAuditList.ts            | The code snippet imports react-query hooks and defines a custom hook called useAuditList. It uses the useQuery hook to fetch audit list data from the APIClient and returns the result. The useAuditList hook can be used to easily fetch and manage audit list data in React components.                                                                                                           |
| useAuditStats.ts           | This code snippet is a custom hook called useAuditStats that leverages the React Query library. It utilizes the useQuery hook to fetch audit statistics from an API using APIClient.fetchAuditStats. The fetched data is then returned as a result for further use in the application.                                                                                                              |
| keys.ts                    | The code snippet exports two constants, "RQ_AUDIT_STATS" and "RQ_AUDIT_LIST", which likely represent request names or types.                                                                                                                                                                                                                                                                        |

</details>

<details closed><summary>Farming</summary>

| File                    | Summary                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| FarmingTableRow.tsx     | The code snippet provides a React component called "FarmingTableRow" that renders a row in a table for farming data. It includes various styled components, handles user actions, and displays data related to farming stats and user information. The component also has the ability to expand and display additional actions when clicked.                                                                            |
| FarmingHarvestBlock.tsx | This code snippet defines a React component called "FarmingHarvestBlock". It renders a UI block displaying information about a farming pool, such as the amount of rewards earned and the ability to harvest those rewards. The component utilizes various hooks and styled components to achieve its functionality. It also includes logic for calculating and updating the rewards earned in real-time.               |
| FarmingActionRow.tsx    | The code snippet is a React component that renders a row of actions for a farming feature. It includes buttons to stake, update stake, and claim rewards. It also displays relevant information such as the amount staked and unclaimed rewards. The component is styled using styled-components.                                                                                                                       |
| FarmingTable.tsx        | This code snippet defines a `FarmingTable` component that displays farming statistics in a table format. It uses various hooks to fetch data related to farming and user liquidity. The table headers and rows are styled using `styled-components` library. The component also handles loading and error states.                                                                                                       |
| USDPriceText.tsx        | The code snippet imports the useLPToUSD hook and defines a functional component called USDPriceText. It takes in two props-amount (a number) and pool (a string). The component uses the useLPToUSD hook to convert the amount to USD based on the specified pool. The converted price is then rendered as the component's output.                                                                                      |
| FarmingStakeModal.tsx   | This code snippet is a React component that represents a modal for staking LP (Liquidity Provider) tokens. It interacts with various hooks and components to manage the UI state, handle user input, and perform staking operations. It also includes error handling and validation.                                                                                                                                    |
| useFarmingWithdraw.ts   | This code snippet defines a custom hook called useFarmingWithdraw. It uses React Query and a NotificationContext to handle a mutation for withdrawing from a farming pool. It invalidates several queries on success and displays a notification if the transaction is successful.                                                                                                                                      |
| useFarmingStake.ts      | The provided code snippet is a React hook that allows users to stake a certain quantity in a farming contract. It uses the react-query library to handle mutations and query invalidations. Upon successful staking, it triggers notifications, updates farming statistics, and invalidates related queries.                                                                                                            |
| keys.ts                 | The code snippet defines constants for two API request types: "userFarming" and "farmingStats".                                                                                                                                                                                                                                                                                                                         |
| useFarmingClaim.ts      | The code snippet exports a React hook called useFarmingClaim that is used to manage the farming claim functionality. It utilizes react-query and the LibreClient service to make API requests. When a claim is successful, it invalidates relevant query caches and displays a notification.                                                                                                                            |
| useUserFarming.ts       | This code snippet provides a custom hook, useUserFarming, that makes a query to retrieve farming data for a specific user. It utilizes the React Query library to handle the asynchronous request and caching of the data. The getUserFarming function fetches the data from an API using the APIClient service. The hook returns a UseQueryResult object containing the fetched farming data and any potential errors. |
| useFarmingStats.ts      | This code snippet defines a custom hook called "useFarmingStats" that uses the react-query library. It fetches farming statistics from an API and returns the result as a query. The hook can be used to easily fetch and manage farming stats data in a React component.                                                                                                                                               |

</details>

<details closed><summary>Sendreceive</summary>

| File                 | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| -------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| UserListSkeleton.tsx | The provided code snippet defines styled components for a user list skeleton. It creates a list container with vertical flex layout, styled list items with a margin bottom, and styled items with a shimmer animation effect. The items consist of a styled image and a styled div. This code is used to render a user list skeleton component.                                                                                                         |
| AmountSendMax.tsx    | This code snippet defines a React component called "AmountSendMax" that allows users to send the maximum amount of a selected token. It fetches user tokens, selects the current user's token, and updates the amount to be sent when clicked. The component is styled and uses context to manage state and handle click events.                                                                                                                         |
| SendDetails.tsx      | The code defines a React component called'SendDetails' which displays different details related to a transaction. It uses styled components for styling and useContext to access data from the WalletSendContext provider. The component renders multiple sections for recipient, fee, total, and expected amounts, each with its own header and content. The active prop determines whether the content is displayed or not.                            |
| SendToSelect.tsx     | The code snippet is a React component that handles the UI for sending money. It uses styled-components for styling and useContext to access the recipient from the WalletSendContext. It renders different components based on the presence of a recipient, including input fields for the amount and recipient, a user list, and submit and details components for sending the money.                                                                   |
| SendModal.tsx        | The code snippet is a React component that renders a modal for sending a transaction. It uses the WalletSendContext to access the resetState function, which is called when the modal is opened or closed. The modal includes the SendToSelect component for selecting where to send the transaction.                                                                                                                                                    |
| AmountSendInput.tsx  | This code snippet defines a React component called "AmountSendInput" that handles the input for sending a certain amount of tokens or USD. It includes functionalities such as switching between token and USD values, converting between them based on exchange rates, displaying the converted price, and updating the input value accordingly. It also allows the user to enter the maximum available balance or switch between token and USD values. |
| SendSubmit.tsx       | The provided code snippet defines a component called "SendSubmit" that is responsible for rendering a form for sending a message. It uses styled-components for styling and various hooks and context to manage state and handle user interactions. The form includes a textarea for entering a message and a button for submitting the form.                                                                                                            |
| UserList.tsx         | The code snippet is a React component that renders a list of users. It uses styled-components for styling and the next/image package for rendering an SVG icon. It fetches user data from a context provider and displays it in a list format. The list items are clickable and trigger a function to set the selected user as the recipient. While the data is being fetched, a loading skeleton is displayed.                                          |
| SendInput.tsx        | The code snippet defines a component called SendInput that renders an input field, along with an image icon and error messages. It uses styled-components for styling and useContext/useMemo hooks for state management. The component also utilizes custom hooks and a context provider for handling input changes and error handling.                                                                                                                  |

</details>

<details closed><summary>Myreferrals</summary>

| File                    | Summary                                                                                                                                                                                                                                                                                                                                                                                                      |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| MyReferralsSection.tsx  | This code snippet is a React component that displays a section for managing referrals. It fetches the user's referrals, and if there are no referrals, it displays an empty state. It also includes a button to invite others and a modal for sending referral invites. The code utilizes styled-components for styling and useState for managing state.                                                     |
| ReferralsEmptyState.tsx | This code snippet is a React component that represents an empty state for a referrals feature. It displays an animation, a title, a description, and a button to invite friends. Clicking the button opens a modal for sending referral invites.                                                                                                                                                             |
| MyReferralTable.tsx     | This code snippet defines the MyReferralsTable component, which renders a virtualized table with columns for rank, libre name, earnings, and people joined. It uses react-virtualized library for efficient rendering of large datasets. The component handles loading more rows and displays data using appropriate formatting.                                                                             |
| ReferralInviteModal.tsx | This code snippet defines a React component called ReferralInviteModal that displays a modal window. The modal contains a title, a text description, and a button. The component receives two props: "open" to determine if the modal is visible, and "handleOnClose" to handle the closing of the modal.The component uses styled-components for styling and the useTheme hook to access the current theme. |

</details>

<details closed><summary>Stakes</summary>

| File                 | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| StakesSection.tsx    | The code snippet is a React component that renders a section for displaying stakes. It imports various dependencies such as lodash, moment, next/image, and styled-components. The component fetches user stakes data using a custom hook and renders a table of stakes. It also includes a button for creating a new stake and a modal for stake creation. The component handles the opening and closing of the modal using state hooks.                                                                                                                                                                                                                    |
| StakeEmptyState.tsx  | The provided code snippet is a React component that renders a stake empty state UI. It includes a Lottie animation, a title, a description, and a "New Stake" button. The styling is done using styled-components.                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| StakeTable.tsx       | This code snippet defines a React component called StakeTable. It renders a table that displays stake information. The table has columns for claim date, APY, initial stake, payout, and action. The data for the table is provided through the "data" prop. The component uses various dependencies and hooks from external libraries including react-virtualized, styled-components, and moment. It also imports and uses other custom components and utility functions. The table supports infinite scrolling and lazy loading of data. It also provides buttons for claiming stakes, emergency unstaking, and displays formatted dates and token values. |
| useClaimStake.ts     | The code snippet is a custom React Hook called "useClaimLibre" that utilizes the "useMutation" hook from the React Query library. It connects to a service called "LibreClient" to perform a "claimLibre" action with the provided index parameter. Upon successful completion, it invalidates the "RQ_USER_STAKE_STATS" and "RQ_USER_STAKES" queries in the query client.                                                                                                                                                                                                                                                                                   |
| useUserStakes.ts     | The code snippet uses the React Query library to manage and fetch user stakes data. It defines a custom hook'useUserStakes' that takes an'accountName' parameter. It uses the'useQuery' hook to fetch user stakes data using the'getUserStakes' function. The fetched data is returned as a result along with any potential errors.                                                                                                                                                                                                                                                                                                                          |
| useUnstakeLibre.ts   | This code snippet defines a custom hook called useUnstakedLibre. It utilizes the react-query library to handle mutations. When the hook is called, it initializes a mutation function that unstakes a specific Libre item based on the provided index. Upon successful execution, it invalidates two query caches related to user stakes and stake statistics.                                                                                                                                                                                                                                                                                               |
| keys.ts              | The code snippet defines two constants: "RQ_USER_STAKES" and "RQ_USER_STAKE_STATS".These constants likely represent key names or identifiers used for requesting user stake data and stake statistics, respectively.                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| useUserStakeStats.ts | The code snippet defines a custom hook in React that utilizes the react-query library. It fetches user stake statistics from an API and returns the result using the useQuery hook. The result is then used in the component that calls this hook.                                                                                                                                                                                                                                                                                                                                                                                                           |
| useStakeLibre.ts     | The code snippet is a custom hook called `useStakeLibre` that utilizes the `useMutation` and `useQueryClient` hooks from React Query library. It handles the mutation for staking a certain quantity of an item for a specified duration using the `LibreClient` service. It invalidates two query caches (`RQ_USER_STAKES` and `RQ_USER_STAKE_STATS`) upon successful mutation.                                                                                                                                                                                                                                                                             |

</details>

<details closed><summary>Mytokens</summary>

| File                       | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| -------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| LightningQRCodeModal.tsx   | The provided code snippet defines a LightningQRCodeModal component that displays a QR code with a Bitcoin Lightning invoice. It also includes a countdown timer for the invoice expiration, the invoice amount, and a copy button. The component is styled using styled-components library and utilizes the react-qrcode-logo package for rendering the QR code.                                                                                      |
| BitcoinQRCodeContent.tsx   | This code snippet is a React component that generates a Bitcoin QR code, displays the Bitcoin address, and allows users to copy the address to the clipboard. It also includes loading animation while fetching the Bitcoin address.                                                                                                                                                                                                                  |
| MyTokensTableStatic.tsx    | This code snippet is a React component that renders a table of user tokens. It uses the react-virtualized library for efficient rendering and includes features like infinite scrolling and dynamic table sizing. The component also handles button clicks, opens modals, and redirects users to external links based on token names and query parameters.                                                                                            |
| AddBalanceModal.tsx        | This code snippet is a React component that renders a modal for adding balance. It includes a navigation bar with two tabs (Bitcoin and Lightning) and displays the content based on the active tab. The Bitcoin tab includes a QR code and a notification. The Lightning tab includes a different QR code. The modal can be closed through a callback function.                                                                                      |
| MyTokensSection.tsx        | This code snippet creates a component called `MyTokensSection` that displays a section containing token data. It uses various hooks and providers to fetch and display the data. The component also includes a header with a title and description, as well as a total value section. The content rendered depends on whether the data is static or dynamic. The component is styled using the `styled-components` library.                           |
| LightningQRCodeContent.tsx | This code snippet is a React component that renders either a "LightningAmountModal" or a "LightningQRCodeModal" based on the value of the "invoice" state. If the "invoice" state is empty, it renders the "LightningAmountModal" and passes it the necessary props. Otherwise, it renders the "LightningQRCodeModal" and passes it the necessary props.                                                                                              |
| LightningAmountModal.tsx   | This code snippet defines a React component called "LightningAmountModal" that allows users to input an amount and create a lightning invoice. It includes styled components for rendering the input field, error messages, and a confirmation button. It also makes use of React hooks like useRef, useState, and useEffect for handling user input and API requests.                                                                                |
| MyTokensTable.tsx          | The provided code snippet is a React component called "MyTokensTable" that renders a table of user tokens. It uses external libraries like Next.js, react-virtualized, and styled-components. The table displays information such as token name, total value, staked payout, unstaked amount, and options to receive or send tokens. It also includes a modal for adding balance to a token. The table is responsive and supports infinite scrolling. |

</details>

<details closed><summary>Validators</summary>

| File                 | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| -------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| VoteModal.tsx        | This code snippet is a React component that renders a modal for voting on a producer. It uses styled-components for styling, next/image for displaying images, and several custom hooks for data fetching and authentication. It checks the user's token balance and displays different content based on whether they have enough tokens to vote or not. If the user has enough tokens, it renders a confirmation message and a button to submit the vote. |
| ValidatorsTable.tsx  | This code snippet defines a React component called "ValidatorsTable" that displays a table of validators. It uses the react-virtualized library for efficient rendering of large datasets. The table includes columns for rank, validator name, location, total votes, votes percentage, and an action button for voting. It also includes functionality for handling user voting and opening a modal for voting.                                          |
| useVoteProducer.ts   | The code snippet is a custom hook that uses react-query to handle voting for a producer. It exports a function called useVoteProducer, which returns a mutation function that calls the LibreClient.voteProducer API. When the vote is successful, it invalidates the "RQ_VOTES" query using the queryClient.                                                                                                                                              |
| useValidators.ts     | This code snippet is a custom hook named useValidators that utilizes the React Query library to fetch and cache a list of validators. It calls the getValidators function which internally makes an HTTP request to the APIClient service. The result is returned through the useQuery hook for easy consumption by components.                                                                                                                            |
| useValidatorVotes.ts | This code snippet defines a custom React Hook called "useValidatorVotes". It uses the React Query library to fetch and manage data related to validator votes. The "getValidatorVotes" function is responsible for making an asynchronous API call to fetch the votes for a given account name. The "useValidatorVotes" function returns a React Query result object that provides the fetched data and handles error cases.                               |
| keys.ts              | The code snippet defines two constants, "RQ_VALIDATORS" and "RQ_VOTES", representing the core functionalities of a system. "RQ_VALIDATORS" likely represents a feature related to validating data, while "RQ_VOTES" suggests a feature related to voting or rating.                                                                                                                                                                                        |

</details>

<details closed><summary>Public</summary>

| File             | Summary                                                                                                                                                                                                                                                                                        |
| ---------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| site.webmanifest | This code snippet defines the core properties of a Progressive Web App (PWA) called "Libre Dashboard". It includes the app's name, icons in different sizes, theme and background colors, and the display mode. The app will be displayed as a standalone application with a white background. |

</details>

<details closed><summary>Hooks</summary>

| File                          | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| useSwapTokenData.ts           | This code snippet provides a React hook called `useSwapTokenData` that allows for switching between base token data and user tokens based on the login status. It retrieves the token data using two different queries (`useTokens` and `useUserTokens`) and returns the appropriate token data based on the login status. The returned object includes the token data and a flag indicating if the tokens are still loading.                                                                                                                |
| useDisabledTokens.ts          | This code snippet is a function called useDisabledTokens that takes in two user tokens (tokenFrom and tokenTo) and an optional array of available tokens. It determines and returns the disabled tokens for both tokenFrom and tokenTo based on certain conditions on the available tokens. The disabled tokens are stored in the disabledFromTokens and disabledToTokens arrays, respectively.                                                                                                                                              |
| useLPToUSD.ts                 | The provided code snippet is a React custom hook called `useLPToUSD`. It calculates the USD value of a given token amount in a specified pool. It relies on two other custom hooks, `usePoolSupply` and `useExchangeRates`, to fetch data needed for the calculation. The calculated value is returned as a string in the format "$X.XX". The code snippet accounts for different environments and handles cases where required data is missing. The resultant USD value is returned or "--" is returned if the calculation is not possible. |
| useIsUsernameValid.ts         | The provided code snippet is a custom React hook that can be used to validate a username in a form. It uses the lodash `debounce` function to delay the validation request to the server by 500 milliseconds, reducing unnecessary calls. The `useIsUsernameValid` hook returns a debounce function `handleChange` that can be used as an event handler to update the validity state of the username input in real-time. The state `isValid` indicates whether the entered username is valid or not.                                         |
| useTokenPriceConversion.ts    | The provided code snippet is a React hook called `useTokenPriceConversion` that calculates token price conversion based on the input parameters `fromToken`, `toToken`, `amount`, and `pool`. It makes use of the `useTokenInformation` hook to fetch token data and performs calculations to determine the price per token, slippage, and minimum expected amount. The results are then returned as a response object.                                                                                                                      |
| useTokenTableData.ts          | The code snippet defines a custom hook called useTokenTableData. It fetches data related to a user's liquidity, farming, and tokens. It then combines and transforms the data to create token table data. The hook returns the token data along with flags indicating loading and fetching statuses. (327 characters)                                                                                                                                                                                                                        |
| usePoolPriceConversion.ts     | The code snippet provides a function called "usePoolPriceConversion" that calculates various conversion rates and balance checks for a token swap in a pool. It takes input parameters related to the pool, user tokens, and desired token amounts, and returns a response object with converted token prices, maximum asset amounts, and a balance error message if applicable.                                                                                                                                                             |
| usePrevious.ts                | The provided code snippet exports a custom hook called "usePrevious" which is used to store and return the previous value of a given variable. It utilizes the "useEffect" and "useRef" hooks from React to update and store the previous value whenever the value changes. The hook returns the previous value as a string.                                                                                                                                                                                                                 |
| useSwapMarketMaker.ts         | This code snippet defines a custom hook called `useSwapMarketMaker` that handles the logic for swapping tokens in a market-making context. It uses various dependencies and custom query hooks to fetch market offers, create orders, and calculate prices. The hook returns the market price, slippage, and the best market offer, along with a loading state.                                                                                                                                                                              |
| useValidateBitcoin.ts         | The code snippet is a function that validates a Bitcoin address and extracts the amount if provided. It removes any "bitcoin:" prefix and parses the amount from the address string. It then validates the address using a Bitcoin address validation library. If the address starts with "bc1p", it sets the validity to false and flags it as a Taproot address. The function returns the validated address, the extracted amount, and the validity and Taproot flags.                                                                     |
| useValidateEthereum.ts        | This code snippet validates an Ethereum address and extracts an optional amount from a provided URL. It uses the ethers library to check if the address is valid and returns the updated address, amount, and a boolean indicating if the address is valid or not.                                                                                                                                                                                                                                                                           |
| useUSDConversion.ts           | The code snippet provides a hook called "useUSDConversion" that takes an array of user tokens as input. It uses another hook called "useExchangeRates" to retrieve exchange rate data. It then calculates and returns the total value of the user's tokens in USD by multiplying the token's total value with the corresponding exchange rate.                                                                                                                                                                                               |
| useComponentVisible.ts        | This code snippet is a custom React hook that manages the visibility of a component. It sets up an event listener to track clicks outside of the component, and updates the visibility state accordingly. It returns a reference to the component, the visibility state, and a function to update the visibility state.                                                                                                                                                                                                                      |
| useClickedOutside.ts          | This code snippet provides a custom React hook, `useClickedOutside`, which triggers a callback function when a click event occurs outside a specified element referenced by the `ref` object. It adds an event listener for `mousedown` events on the document, checks if the clicked target is outside the specified element using the `contains` method, and calls the callback function accordingly. The event listener is cleaned up when the component unmounts.                                                                        |
| usePoolsTableData.ts          | The provided code snippet is a custom hook called "usePoolsTableData" that fetches data from different queries using React hooks. It retrieves pool statistics, token information, and exchange rates. It then processes and formats this data to be used in a table. The hook returns the processed data as well as a loading state.                                                                                                                                                                                                        |
| useDetectSwipe.ts             | This code snippet provides a custom hook called `useDetectSwipe` that detects swipe gestures on touch devices. It tracks whether a swipe is performed to the left or right and returns the corresponding boolean values (`swipeLeft` and `swipeRight`). It achieves this by listening for touch events and calculating the difference in coordinates to determine the direction of the swipe.                                                                                                                                                |
| useInputPriceConversion.ts    | The code snippet implements a function called `useInputPriceConversion` that calculates the converted prices of two tokens based on pool supply and user input. It takes various parameters such as pool supply, pool type, user tokens, input tokens, and amounts. The function uses the provided data to find the prices of the input tokens in the pool, and then calculates the converted prices of the tokens based on the user input amounts. The function returns the converted prices in a structured format.                        |
| useMintCalculation.ts         | The provided code calculates the base interest rate (baseAPY) and the final interest rate (finalAPY) based on lockUpDays and mintBonus parameters. If either parameter is missing, it returns default values of 0. The baseAPY is calculated using a formula, and the finalAPY is calculated by multiplying the baseAPY with the mintBonus.                                                                                                                                                                                                  |
| useGetLightningInvoiceInfo.ts | This code snippet defines a custom hook in React called "useGetLightningInvoiceInfo". It takes in a Lightning network invoice as input and returns information about the invoice, such as the amount in satoshis, expiration status, the actual invoice, and its expiry time. The hook makes use of the "light-bolt11-decoder" library to decode the invoice. The loading state is also managed within the hook.                                                                                                                             |
| useBitcoinInvoice.ts          | This code snippet is a custom hook named useBitcoinInvoice that retrieves and manages a Bitcoin address for a user. It uses various query hooks to fetch, generate, cache, and handle the Bitcoin address. The hook returns the Bitcoin address and loading status.                                                                                                                                                                                                                                                                          |

</details>

<details closed><summary>Markus</summary>

| File                    | Summary                                                                                                                                                                                                                                                                                                                                                                             |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| useMarkusOffers.ts      | This code snippet is a custom hook called "useMarkusOffers" that uses the React Query library to fetch and manage data. It fetches Markus offers from a remote server using the provided identifier, with a limit on the number of fetch attempts. It also controls the interval at which the data should be refetched based on the length of the fetched data.                     |
| useCreateMarkusOrder.ts | This code snippet uses the react-query library to create a custom hook called useCreateMarkusOrder. It utilizes the useMutation hook to handle the mutation logic. When the mutation is successful, the onSuccess callback is triggered, which updates the query data using queryClient.setQueryData. The code is designed to create a Markus order using the MarkusClient service. |
| keys.ts                 | The provided code snippet defines two constants that represent the names of different request types in a system: "markusOrder" and "markusOffers". These constants can be used to identify specific requests and their corresponding functionalities within the codebase.                                                                                                           |

</details>

<details closed><summary>Wallet</summary>

| File              | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ----------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| useSearchUsers.ts | The provided code snippet utilizes the react-query library to implement a custom hook called `useSearchUsers`. This hook queries the cache for search user data using the key `RQ_SEARCH_USERS` and returns the query result with the type `ISearchUser[]` and error type `Error`.                                                                                                                                                                       |
| useUserSearch.ts  | The code snippet provides a custom hook called "useUserSearch" that uses React Query to handle fetching user search data. It takes in an account name and an array of properties to notify on change. The hook executes the "getUserSearch" function to fetch the data based on the account name, and returns the result and error state. The hook is enabled only if there is an account name provided.                                                 |
| keys.ts           | The code snippet exports a constant variable "RQ_SEARCH_USERS" that represents a search functionality for users.                                                                                                                                                                                                                                                                                                                                         |
| useSendInput.tsx  | The provided code snippet defines a custom hook called `useSendInput`. It manages the input value, error, and behavior related to sending transactions. It utilizes other custom hooks for validating bitcoin and ethereum addresses, and fetching information about a Lightning invoice. It also includes functionality for debouncing the input value, handling input changes, and checking the input for valid addresses and appropriate token types. |
| index.tsx         | The provided code snippet is a React component that renders a Wallet page. It imports various components for displaying different sections of the wallet, such as tokens, stakes, and referrals. It also uses styled-components for styling the layout. The component checks if the user is authenticated and renders different content accordingly.                                                                                                     |

</details>

<details closed><summary>Mints</summary>

| File                | Summary                                                                                                                                                                                                                                                                                                                                                                             |
| ------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| useUserMintStats.ts | The code snippet defines a hook called useUserMintStats, which uses React Query to fetch user mint statistics. It makes an API request to retrieve mint stats based on the provided account name. The result is stored in a query cache and returned as a UseQueryResult.                                                                                                           |
| useMintLibre.ts     | The code snippet defines a custom hook, "useMintLibre", that utilizes the react-query library. It sets up a mutation function that calls the "mintLibre" method from LibreClient with the provided parameters. On successful execution, it invalidates two specific queries after a 3-second delay. The hook can be used to manage the minting of "Libre" with flexible parameters. |
| useUserMints.ts     | This code snippet defines a custom hook named useUserMints that uses React Query to fetch and cache user mints data. It makes an asynchronous API call to fetch user mints based on the provided account name and returns the result as a query result object. The code exports the hook for use in other components.                                                               |
| keys.ts             | The code snippet defines two constants: "RQ_USER_MINTS" and "RQ_USER_MINT_STATS". These constants likely represent keys or identifiers used for making requests related to user mints and user mint statistics in an application.                                                                                                                                                   |

</details>

<details closed><summary>Exchangerates</summary>

| File                | Summary                                                                                                                                                                                                                                                                                                                                            |
| ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| useExchangeRates.ts | This code snippet implements a React Hook called `useExchangeRates` that utilizes the `useQuery` hook from the `react-query` library. It fetches exchange rates for various tokens and calculates farming statistics based on the fetched data. The `useExchangeRates` hook returns the exchange rates and farming stats as a result of the query. |
| keys.ts             | The code snippet exports a constant variable `RQ_EXCHANGE_RATES` that stores the string "exchangeRates". This variable can be used throughout the codebase for referencing the exchange rates.                                                                                                                                                     |

</details>

<details closed><summary>Stats</summary>

| File                   | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ---------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| useAirdropStats.ts     | This code snippet uses React Query to fetch airdrop statistics from an API. The `useAirdropStats` function returns a `UseQueryResult` object that handles the query, and the `getAirdropStats` function fetches the data from the API using APIClient.                                                                                                                                                                                                      |
| useStakeStats.ts       | The code snippet is a custom hook that utilizes the react-query library to fetch and cache the stake statistics data. It exports a function called "useStakeStats" which returns a query result object containing the fetched data and any potential error. The data is fetched using the "getStakeStats" function by making a request to the "fetchStakeStats" API endpoint provided by the APIClient service.                                             |
| keys.ts                | The code snippet defines constants for different types of statistics related to application, staking, airdrop, and minting. These constants can be used to request specific types of statistics from an API or data source.                                                                                                                                                                                                                                 |
| useApplicationStats.ts | This code snippet defines a custom hook called useApplicationStats that uses the useQuery hook from React Query to fetch application statistics from an API. The fetched data is typed as IApplicationStats and returned as a result of the hook.                                                                                                                                                                                                           |
| useMintStats.ts        | The provided code snippet is a custom hook called "useMintStats" that uses the React Query library to fetch and manage data related to mint statistics. It makes use of the "useQuery" hook to handle the asynchronous data fetching and caching. The "getMintStats" function is responsible for making the API call to fetch the mint statistics data. The hook returns a "UseQueryResult" object that contains the fetched data and any potential errors. |

</details>

<details closed><summary>Proposals</summary>

| File                     | Summary                                                                                                                                                                                                                                                                                                                                          |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| useProposalProperties.ts | The code snippet uses react-query to fetch and cache data related to proposal properties. It defines a function to get DAO information using the LibreClient service and exports a custom hook that uses the useQuery hook to fetch the data and handle loading, error, and caching.                                                             |
| useProposal.ts           | The provided code snippet is a custom React Hook named "useProposal" that utilizes the react-query library. It fetches a proposal by its ID using the "getProposalById" function and returns the query result. The React Query key is composed of "RQ_PROPOSAL" and the proposal ID.                                                             |
| useVoteAgainst.ts        | The code snippet defines a custom hook named useVoteAgainst that utilizes React Query's useMutation and useQueryClient hooks. It performs a mutation to vote against a proposal using the LibreClient service. On success, it invalidates the queries for votes and the specific proposal in the query cache.                                    |
| useVoteFor.ts            | This code snippet is a custom React Hook that uses React Query's useMutation and useQueryClient hooks. It defines a useVoteFor function that takes a proposalId as a parameter and returns a mutation function. When the mutation is successful, it invalidates two queries in the query cache, one for votes and one for the specific proposal. |
| keys.ts                  | This code snippet defines constants for different types of requests related to proposals, votes, and proposal properties.                                                                                                                                                                                                                        |
| useVotes.ts              | The code snippet is a custom hook called "useVotes" that uses the react-query library to fetch votes for a given proposal ID. It makes an asynchronous call to the "getVotesById" function using the LibreClient service, and returns the result as a query using the useQuery hook. The query is identified by the keys [RQ_VOTES, proposalId]. |
| useProposals.ts          | This code snippet uses the React Query library to fetch and manage a list of proposals. The useProposals custom hook executes a query to fetch the proposals using the getProposals function, which makes an API call using LibreClient. The fetched data is then returned as a result of the query.                                             |

</details>

<details closed><summary>Pages</summary>

| File                   | Summary                                                                                                                                                                                                                                                                                                                                    |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| index.tsx              | This code snippet is a React component that renders a dashboard page. It includes a styled layout with a header, sections for chain activity, tokens, and pools, and utilizes various components for page title and header details.                                                                                                        |
| \_error.js             | The code snippet defines a custom error component for handling errors in Next.js. It integrates with Sentry for error capturing and uses the built-in Next.js error component. It provides a custom error page for server-side and client-side errors, and includes error handling for data-fetching methods.                              |
| farming.tsx            | This code snippet sets up a Farming page in a web application. It includes components for the page header, page title, header details, and a farming table. The components are styled using styled-components.                                                                                                                             |
| mint.tsx               | The provided code snippet is a React component that renders a Mint page. It fetches and displays various mint statistics and user-specific mint information. It also handles the opening and closing of modals for minting and adding balance. The component is styled using Styled Components and uses Next.js for server-side rendering. |
| audit.tsx              | This code snippet defines the layout and structure of an audit page. It includes components for displaying proof of assets and on-chain validation data. The page also includes a title, description, and header details.                                                                                                                  |
| validators.tsx         | The code snippet is a React component that displays a list of validators. It fetches validator data and votes from an API, allows sorting of the data based on rank, and renders a table with the sorted data. The component also includes a page title, a header with details, and a section wrapper for the content.                     |
| swap.tsx               | This code snippet implements a page component for a swapping feature. It uses Next.js and React to render the UI and handle routing. The component includes a toggle switch to switch between Swap and Pool modes, and renders the corresponding components accordingly. It also sets the initial state based on the router query.         |
| [username].tsx         | The provided code snippet defines a functional component called "UserDetails". It renders a styled wrapper component with a header section that includes a page title component. The content section includes a "MyTokensSection" component with the "isStatic" prop set.                                                                  |
| sentry_sample_error.js | The code snippet defines a Next.js home page component that displays a title, description, and a button. Clicking the button throws an error. This component also includes a link to the Sentry documentation for more information.                                                                                                        |
| \_app.tsx              | This code snippet is the entry point for a Next.js application. It sets up the Next.js app with various providers for authentication, window size, and notifications. It also includes styling components and sets up a query client for data fetching.                                                                                    |

</details>

<details closed><summary>Services</summary>

| File            | Summary                                                                                                                                                                                                                                                                                                                                              |
| --------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| MarkusClient.ts | The provided code snippet is a TypeScript module that exports a class called MarkusClient. It includes functionalities to create a Markus order using an API endpoint and fetch Markus offers using a JSON-RPC call to a specific table. The class constructor initializes the API endpoint and JSON-RPC client.                                     |
| APIClient.ts    | The provided code snippet is an API client class that allows fetching various statistics and data from an API endpoint. It includes functions for fetching application stats, stake stats, airdrop stats, referral stats, user stakes stats, mint stats, user mints, token data, validator data, audit stats, farming stats, and user BTC addresses. |
| ChainClient.ts  | The provided code snippet defines a ChainClient class that serves as an interface to interact with a blockchain. It has methods to search users, cache a BTC address, register a lightning payment, and fetch lightning fees. It uses the axios library to make HTTP requests to the blockchain endpoint.                                            |

</details>

---

## 🚀 Getting Started

### ✔️ Prerequisites

Before you begin, ensure that you have the following prerequisites installed:

> - `ℹ️ Requirement 1`
> - `ℹ️ Requirement 2`
> - `ℹ️ ...`

### 📦 Installation

1. Clone the libre-dashboard repository:

```sh
git clone https://gitlab.com/libre-tech/libre-dashboard.git
```

2. Change to the project directory:

```sh
cd libre-dashboard
```

3. Install the dependencies:

```sh
npm install
```

### 🎮 Using libre-dashboard

First, run the development server:

```bash
npm run start:dev
# or
yarn start:dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

For deployment:

```sh
npm run build && node dist/main.js
````

### 🧪 Running Tests

```sh
npm test
```

---

## Dockerfile Usage

This repository includes a Dockerfile that allows you to containerize and run the application.

### Prerequisites

Make sure you have Docker installed on your system. If you don't have Docker, you can download and install it from the official website: [Docker Official Website](https://www.docker.com/get-started).

### Building the Docker Image

To build the Docker image, navigate to the root of the project where the `Dockerfile` is located, and run the following command:

```sh
docker build -f Dockerfile -t your_image_name \
--build-arg SENTRY_AUTH_TOKEN="your_sentry_auth_token" \
--build-arg NEXT_PUBLIC_ENV="production" \
--build-arg NEXT_PUBLIC_LIBRE_API_ENDPOINT="https://libre-dashboard-api.edenia.cloud" \
--build-arg NEXT_PUBLIC_LIBRE_CHAIN_ENDPOINT="https://server.production.bitcoinlibre.io" \
--build-arg NEXT_PUBLIC_MARKUS_API_ENDPOINT="https://markus-api.libre.rocks" \
--build-arg NEXT_PUBLIC_LIBRE_APP_NAME="libre-dashboard" \
--build-arg NEXT_PUBLIC_NODE_URL="https://lb.libre.org" \
.
```

## 🗺 Roadmap

> - [x] `ℹ️  Open Source Dashboard`
> - [ ] `ℹ️ ...`
> - [ ] `ℹ️ ...`

---

## 🤝 Contributing

Contributions are always welcome! Please follow these steps:

1. Fork the project repository. This creates a copy of the project on your account that you can modify without affecting the original project.
2. Clone the forked repository to your local machine using a Git client like Git or GitHub Desktop.
3. Create a new branch with a descriptive name (e.g., `new-feature-branch` or `bugfix-issue-123`).

```sh
git checkout -b new-feature-branch
```

4. Make changes to the project's codebase.
5. Commit your changes to your local branch with a clear commit message that explains the changes you've made.

```sh
git commit -m 'Implemented new feature.'
```

6. Push your changes to your forked repository on GitHub using the following command

```sh
git push origin new-feature-branch
```

7. Create a new pull request to the original project repository. In the pull request, describe the changes you've made and why they're necessary.
   The project maintainers will review your changes and provide feedback or merge them into the main branch.

---

## 📄 License

This project is licensed under the `ℹ️  MITT` License. See the [LICENSE](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/adding-a-license-to-a-repository) file for additional info.

---

## 👏 Acknowledgments

> - `ℹ️  List any resources, contributors, inspiration, etc.`

---
