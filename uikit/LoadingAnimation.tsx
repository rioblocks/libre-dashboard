import styled, { css, keyframes } from "styled-components";

const rotation = keyframes`
  from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(359deg);}
`;

const Spinner = styled.div<{
  size: "tiny" | "small" | "medium" | "large";
  colorRGB?: string;
}>`
  height: 60px;
  width: 60px;
  -webkit-animation: ${rotation} 1s infinite linear;
  -moz-animation: ${rotation} 1s infinite linear;
  -o-animation: ${rotation} 1s infinite linear;
  animation: ${rotation} 1s infinite linear;
  border: 6px solid rgba(0, 0, 0, 0.2);
  border-radius: 100%;

  &:before {
    content: "";
    display: block;
    position: absolute;
    left: -6px;
    top: -6px;
    height: 100%;
    width: 100%;
    border-top: 6px solid
      ${(p) => (p.colorRGB ? `rgba(${p.colorRGB}, 0.8)` : `rgba(0, 0, 0, 0.8)`)};
    border-left: 6px solid transparent;
    border-bottom: 6px solid transparent;
    border-right: 6px solid transparent;
    border-radius: 100%;
  }

  ${(p) =>
    p.size === "medium" &&
    css`
      height: 40px;
      width: 40px;
      border: 5.2px solid
        ${p.colorRGB ? `rgba(${p.colorRGB}, 0.2)` : `rgba(0, 0, 0, 0.2)`};

      &:before {
        left: -5px;
        top: -5px;
        border-top: 5px solid
          ${p.colorRGB ? `rgba(${p.colorRGB}, 0.2)` : `rgba(0, 0, 0, 0.2)`};
        border-left: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-right: 5px solid transparent;
      }
    `}

  ${(p) =>
    p.size === "small" &&
    css`
      height: 25px;
      width: 25px;
      border: 4.4px solid
        ${p.colorRGB ? `rgba(${p.colorRGB}, 0.2)` : `rgba(0, 0, 0, 0.2)`};

      &:before {
        left: -4px;
        top: -4px;
        border-top: 4px solid
          ${p.colorRGB ? `rgba(${p.colorRGB}, 0.4)` : `rgba(0, 0, 0, 0.4)`};
        border-left: 4px solid transparent;
        border-bottom: 4px solid transparent;
        border-right: 4px solid transparent;
      }
    `}

  ${(p) =>
    p.size === "tiny" &&
    css`
      height: 14px;
      width: 14px;
      border: 3.4px solid
        ${p.colorRGB ? `rgba(${p.colorRGB}, 0.5)` : `rgba(0, 0, 0, 0.2)`};

      &:before {
        left: -3px;
        top: -3px;
        border-top: 3px solid
          ${p.colorRGB ? `rgba(${p.colorRGB}, 0.4)` : `rgba(0, 0, 0, 0.1)`};
        border-left: 3px solid transparent;
        border-bottom: 3px solid transparent;
        border-right: 3px solid transparent;
      }
    `}
`;

interface ILoadingAnimation {
  size: "tiny" | "small" | "medium" | "large";
  colorRGB?: string;
}

export const LoadingAnimation = ({ size, colorRGB }: ILoadingAnimation) => {
  return <Spinner size={size} colorRGB={colorRGB} />;
};
