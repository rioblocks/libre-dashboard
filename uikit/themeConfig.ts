import { DefaultTheme } from "styled-components";

const Colors = {
  primaryBlack: "#0d0d0d",
  primaryOrange: "#ff8c00",
  orangeGradient:
    "linear-gradient(90deg, rgba(252,131,89,1) 0%, rgba(252,193,103,1) 100%)",
  background: "#fafafd",
  black: "#04021d",
  borderGrey: "#dedee2",
  lightGrey: "#9695a0",
  red: "#cd324c",
  green: "#32cdac",
};

export const defaultTheme: DefaultTheme = {
  titleText: Colors.black,
  titleGradient: Colors.orangeGradient,
  greyText: Colors.lightGrey,
  textAccentOrange: Colors.primaryOrange,
  descriptionText: "#686777",

  buttonOrange: Colors.orangeGradient,
  buttonOrangeText: "#ffffff",
  buttonWhite: "#ffffff",
  buttonWhiteText: Colors.black,
  buttonRed: "#faebed",
  buttonRedText: Colors.red,
  buttonGrey: "#ebebed",
  buttonGreyText: "#b1b1b9",

  headerBackground: Colors.primaryBlack,
  headerTextColor: "#ffffff",
  headerTextHover: Colors.orangeGradient,
  headerInactiveColor: "rgba(255, 255, 255, 0.5)",

  backgroundColor: Colors.background,

  leftAnnouncementBackground: Colors.primaryBlack,
  rightAnnouncementBackground: Colors.primaryOrange,

  detailBlockBackground: "#ffffff",
  detailBlockBorder: Colors.borderGrey,
  detailBlockTitle: "#777685",
  detailBlockDetails: Colors.black,
  detailBlockAccentRed: Colors.red,
  detailBlockAccentGreen: Colors.green,

  tableHeader: "#777685",

  labelPurpleBackground: "#e7e5fe",
  labelPurple: "#1b0aae",
  labelGreenBackground: "#ebfaf7",
  labelGreen: "#32cdac",
  labelOrangeBackground: "#fff3e5",
  labelOrange: "#ff8c00",
  labelGreyBackground: "#f2f2f2",
  labelGrey: "#9695a0",
  labelRedBackground: "#faebed",
  labelRed: "#d6556a",

  inputBorder: "#c0c0c7",
  inputErrorBorder: Colors.red,

  notificationWarningBackground: "#fff8e6",
  notificationWarningText: "#b38000",
  notificationSuccessBackground: Colors.orangeGradient,
  notificationSuccessText: "#ffffff",
  errorWarningBackground: Colors.red,
  errorWarningText: "#ffffff",

  progressBarBackground: Colors.borderGrey,
  progressBarGreen: Colors.green,
  progressBarRed: Colors.red,

  sliderBarBackground: Colors.primaryOrange,
  sliderBarInactive: Colors.borderGrey,
};
