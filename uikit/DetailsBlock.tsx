import Image from "next/image";
import { ReactElement } from "react";
import styled, { css } from "styled-components";
import { DetailsBlockSkeleton } from "../uikit/DetailsBlockSkeleton";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div<{ totalBlocks?: number; mobile?: boolean }>`
  display: flex;
  flex-direction: flex;
  align-items: center;
  flex: 1;
  padding: 24px;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: ${(p) => p.theme.detailBlockBackground};

  ${(p) =>
    p.totalBlocks === 1 &&
    css`
      flex: 0 1 30%;
    `}

  @media (max-width: ${MediaQueryWidths.medium}px) {
    flex: 0 1 100%;

    &:not(:last-of-type) {
      margin-bottom: 20px;
    }
  }
`;

const Col = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
`;

const Title = styled.span`
  font-size: 12.8px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.detailBlockTitle};
  margin-right: 5px;
`;

const Details = styled.span`
  display: flex;
  font-size: 27.7px;
  font-weight: 600;
  line-height: 1.21;
  color: ${(p) => p.theme.detailBlockDetails};
  margin-top: 10px;

  > span {
    margin-right: 10px !important;
  }
`;

interface IDetailsBlock {
  title: string;
  details: string;
  image?: ReactElement;
  hasInfoIcon?: boolean;
  totalBlocks?: number;
  loading?: boolean;
}

export const DetailsBlock = ({
  title,
  details,
  image,
  hasInfoIcon = false,
  totalBlocks = 3,
  loading = false,
}: IDetailsBlock) => {
  return (
    <Wrapper totalBlocks={totalBlocks}>
      <Col>
        <Row>
          <Title>{title}</Title>
          {hasInfoIcon && (
            <Image src="/icons/info-outline.svg" height={16} width={16} />
          )}
        </Row>
        <Details>
          {loading ? (
            <DetailsBlockSkeleton hasImg={!!image} />
          ) : (
            <>
              {image}
              {details}
            </>
          )}
        </Details>
      </Col>
    </Wrapper>
  );
};
