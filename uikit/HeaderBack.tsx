import Image from "next/image";
import Link from "next/link";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  cursor: pointer;
`;

const HeaderBackText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.detailBlockTitle};
`;

export const HeaderBack = ({ link }) => {
  return (
    <Wrapper>
      <Image src="/icons/chevron-left.svg" width={24} height={24} />
      <Link href={link}>
        <HeaderBackText>Back</HeaderBackText>
      </Link>
    </Wrapper>
  );
};
