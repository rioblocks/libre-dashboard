import styled from "styled-components";

const Wrapper = styled.div<{ iconSize?: number }>`
  display: flex;
  align-items: center;
  flex-shrink: 0;

  img {
    width: ${(p) => (p.iconSize ? `${p.iconSize}px` : "30px")};
    height: ${(p) => (p.iconSize ? `${p.iconSize}px` : "30px")};
    position: relative;
    z-index: 2;

    &:nth-child(2) {
      margin-left: -9px;
      position: relative;
      z-index: 1;
      width: ${(p) => (p.iconSize ? `${p.iconSize}px` : "30px")};
      height: ${(p) => (p.iconSize ? `${p.iconSize}px` : "30px")};
    }

    &:nth-child(3) {
      margin-left: -9px;
      position: relative;
      z-index: 0;
      width: ${(p) => (p.iconSize ? `${p.iconSize}px` : "30px")};
      height: ${(p) => (p.iconSize ? `${p.iconSize}px` : "30px")};
    }
  }
`;

interface IIconPairs {
  imgSrc1: string;
  imgSrc2?: string;
  imgSrc3?: string;
  iconSize?: number;
}

export const IconPairs = ({
  imgSrc1,
  imgSrc2,
  imgSrc3,
  iconSize,
}: IIconPairs) => {
  return (
    <Wrapper iconSize={iconSize}>
      <img src={imgSrc1} />
      {imgSrc2 && <img src={imgSrc2} />}
      {imgSrc3 && <img src={imgSrc3} />}
    </Wrapper>
  );
};
