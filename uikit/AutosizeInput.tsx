import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";

const EmptyInput = styled.input`
  border: none;
  outline: none;
  height: 100px;
  width: 100%;
  text-align: center;
  font-size: 50px;

  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  ::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

const InputContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  width: 100%;
`;

const Label = styled.span`
  position: absolute;
  top: 12px;
  left: -20px;
  font-size: 30px;
  font-weight: bold;
`;

const InputLabel = styled.span`
  display: flex;
  align-items: center;
  height: 100%;
  text-align: center;
  font-size: 50px;
  font-weight: 300;
  margin-left: 5px;
`;

interface AutosizeInputProps {
  type?: "text" | "number";
  placeholder?: string;
  value: string;
  initialValue: string;
  unit: JSX.Element;
  label?: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onFocus?: (event: React.FocusEvent<HTMLInputElement>) => void;
}

export const AutosizeInput = ({
  type = "text",
  placeholder,
  value,
  initialValue,
  unit,
  label,
  onChange,
  onFocus,
}: AutosizeInputProps) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const labelRef = useRef<HTMLInputElement>(null);
  const labelValueRef = useRef<HTMLInputElement>(null);
  const [size, setSize] = useState<number>(4);

  const handleOnFocus = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.target.select();
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange(event);
    const length = event.target.value.length + 0.5;
    setSize(length > 0.5 ? length : 4);
    const fontSize = length > 5 ? 50 - length * 1.5 : 50;
    event.target.style.fontSize = `${fontSize}px`;
    if (!labelRef.current) return;
    labelRef.current.style.fontSize = `${fontSize - fontSize * 0.4}px`;
    1;
    labelRef.current.style.top = `${12 + event.target.value.length}px`;
    labelRef.current.style.left = `-${20 - event.target.value.length}px`;
    if (!labelValueRef.current) return;
    labelValueRef.current.style.fontSize = `${fontSize}px`;
  };

  const handleLabelFocus = () => {
    inputRef.current.focus();
  };

  useEffect(() => {
    if (initialValue) {
      const length = initialValue.length + 0.5;
      setSize(length > 0.5 ? length : 4);
      if (!inputRef.current) return;
      const fontSize = length > 5 ? 50 - length * 1.5 : 50;
      inputRef.current.style.fontSize = `${fontSize}px`;
      if (!labelRef.current) return;
      labelRef.current.style.fontSize = `${fontSize - fontSize * 0.4}px`;
      1;
      labelRef.current.style.top = `${12 + initialValue.length}px`;
      labelRef.current.style.left = `-${20 - initialValue.length}px`;
      if (!labelValueRef.current) return;
      labelValueRef.current.style.fontSize = `${fontSize}px`;
    }
  }, [initialValue, labelRef, labelValueRef]);

  return (
    <InputContainer>
      {unit && <Label ref={labelRef}>{unit}</Label>}
      <EmptyInput
        ref={inputRef}
        type={type}
        value={value}
        onChange={handleInputChange}
        onFocus={handleOnFocus}
        style={{ width: `${size}ch`, padding: 0 }}
        pattern="[0-9]"
        placeholder={placeholder}
      />
      {label && (
        <InputLabel onClick={handleLabelFocus} ref={labelValueRef}>
          {label}
        </InputLabel>
      )}
    </InputContainer>
  );
};
