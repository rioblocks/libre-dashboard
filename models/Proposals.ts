// PROPOSAL STATUS
// DRAFT = 1
// ACTIVE = 2
// SUCCEEDED = 3
// DEFEATED = 4
// CANCELED = 5
// COMPLETED = 6

export interface IStatus {
  [key: number]: string;
}

export interface IProposal {
  amount: string;
  created_on: Date;
  creator: string;
  detail: string;
  expires_on: Date;
  name: string;
  receiver: string;
  status: number;
  title: string;
  url: string;
  votes_against: number;
  votes_for: number;
}

export interface IVote {
  is_for: number;
  quantity: number;
  voter: string;
}

export interface IProposalProperties {
  approver: string;
  funding_account: string;
  minimum_balance_to_create_proposals: string;
  proposal_cost: string;
  vote_threshold: number;
  voting_days: number;
}
