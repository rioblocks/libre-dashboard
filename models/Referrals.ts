export interface IReferralStats {
  count: number;
  bitcoinEarning: number;
  libreEarning: number;
}

export interface IMyReferral {
  ranking: number;
  earnings: number;
  accountName: string;
  referred: number;
}

export interface ILeaderboardReferral {
  rank: number;
  referrees: number;
  name: string;
}
