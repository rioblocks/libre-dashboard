export interface IDaoStats {
  total_btc: number;
  total_libre: number;
  total_usdt: number;
  voting_power: number;
}

export interface IUserDaoPower {
  account: string;
  last_update: string;
  voting_power: string;
}
