export interface ITableRowRequest {
  code: string;
  scope: string;
  table: string;
  limit?: number;
  lowerBound?: string;
  upperBound?: string;
}

export interface ICurrencyBalanceRequest {
  code: string;
  account: string;
  symbol: string;
}
