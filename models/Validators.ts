export interface IValidators {
  rank: number;
  name: string;
  location: string;
  totalVotes: number;
  percentage: string;
}

export interface IValidatorVote {
  votedFor: string;
}
