enum TokenName {
  PBTC = "PBTC",
  BTCL = "BTCL",
}

type Token = {
  supply: string;
  max_supply: string;
  issuer: string;
};

export type IAuditStats = {
  [key in TokenName]: Token;
};

type AudityData = {
  address: string;
  balance: string;
};
export interface IAuditList {
  last_update: Date;
  addresses: AudityData[];
}
