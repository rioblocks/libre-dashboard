import {
  createContext,
  FunctionComponent,
  useCallback,
  useEffect,
  useState,
} from "react";
import styled, { keyframes } from "styled-components";
import { Notification } from "../uikit/Notification";

const slideIn = keyframes`
  from { left: -200px; opacity: 0; }
  to   { left: 20px; opacity: 1; }
`;

const slideOut = keyframes`
  from { left: 20px; opacity: 1; }
  to   { left: -500px; opacity: 0; }
`;

const Wrapper = styled.div`
  position: fixed;
  bottom: 20px;
  left: 20px;
  margin-right: 20px;
  z-index: 99999;

  animation: ${slideIn} 0.5s ease forwards, ${slideOut} 0.5s 5.5s ease forwards;

  @media (max-width: 800px) {
    width: calc(100% - 40px);
  }
`;

const LinkText = styled.span`
  text-decoration: underline;
  cursor: pointer;
  word-wrap: break-word;
`;

export const NotificationContext = createContext<{
  isOpen: boolean;
  handleOnOpen: () => void;
  handleOnClose: () => void;
}>({
  isOpen: false,
  handleOnOpen: () => {},
  handleOnClose: () => {},
});

export const NotificationProvider: FunctionComponent = ({ children }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [data, setData] = useState<any>(null);

  useEffect(() => {
    if (!isOpen) return;
    setTimeout(() => {
      setIsOpen(false);
    }, 6000);
  }, [isOpen]);

  const handleOnOpen = useCallback((data) => {
    if (data) setData(data);
    setIsOpen(true);
  }, []);

  const handleOnClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  const handleLink = () =>
    window.open(
      `https://${
        process.env.NEXT_PUBLIC_ENV === "development" ? "testnet" : "lb"
      }.libre.org/v2/explore/transaction/${data}`,
      "_blank"
    );

  const renderNotificationLink = () => {
    return (
      <>
        Success! Transaction ID: &nbsp;<LinkText>{data}</LinkText>
      </>
    );
  };

  const value = {
    isOpen,
    handleOnClose,
    handleOnOpen,
  };

  return (
    <NotificationContext.Provider value={value}>
      {children}
      {isOpen && (
        <Wrapper>
          <Notification
            type="success"
            text={renderNotificationLink()}
            onClick={data ? handleLink : () => {}}
          />
        </Wrapper>
      )}
    </NotificationContext.Provider>
  );
};
