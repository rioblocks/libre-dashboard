import axios from "axios";
import { pBTC } from "ptokens-pbtc";

class APIClient {
  APIEndpoint: string | undefined;

  constructor() {
    this.APIEndpoint = process.env.NEXT_PUBLIC_LIBRE_API_ENDPOINT;
  }

  // APPLICATION STATS ------------------- //
  fetchApplicationStats = async () => {
    try {
      const response = await axios.get(`${this.APIEndpoint}/stats/chain`);
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/stats/chain`, e.message);
    }
  };

  fetchStakeStats = async () => {
    try {
      const response = await axios.get(`${this.APIEndpoint}/stats/stake`);
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/stats/stake`, e.message);
    }
  };

  fetchAirdropStats = async () => {
    try {
      const response = await axios.get(`${this.APIEndpoint}/stats/airdrop`);
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/stats/airdrop`, e.message);
    }
  };

  // REFERRALS ------------------ //
  fetchReferralStats = async (name: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/referrals/stats/${name}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/referrals/stats/${name}`, e.message);
    }
  };

  fetchMyReferrals = async (name: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/referrals/user/${name}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/referrals/user/${name}`, e.message);
    }
  };

  fetchReferralLeaderboard = async () => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/referrals/leaderboard`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/referrals/leaderboard`, e.message);
    }
  };

  // STAKES ----------------------- //
  fetchUserStakeStats = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/stakes/${accountName}/stats`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/stakes/${accountName}/stats`, e.message);
    }
  };

  // MINTS ----------------------- //
  fetchMintStats = async () => {
    try {
      const response = await axios.get(`${this.APIEndpoint}/mints/stats`);
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/stats/mint`, e.message);
    }
  };

  fetchUserMints = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/mints/${accountName}/list`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/mints/${accountName}/list`, e.message);
    }
  };

  fetchUserMintStats = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/mints/${accountName}/stats`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/mints/${accountName}/stats`, e.message);
    }
  };

  // TOKENS ----------------------- //
  fetchTokens = async () => {
    try {
      const response = await axios.get(`${this.APIEndpoint}/tokens`);
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/tokens`, e.message);
    }
  };

  fetchWrappingAdresses = async (accountName: string, amount: number) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/wrapping/${accountName}?amount=${amount}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/tokens/${accountName}`, e.message);
    }
  };

  fetchLightningWrappingAdresses = async (
    accountName: string,
    amount: number
  ) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/wrapping/lightning/${accountName}?amount=${amount}`
      );
      return response.data;
    } catch (e) {
      console.log(
        `${this.APIEndpoint}/wrapping/lightning/${accountName}?amount=${amount}`,
        e.message
      );
    }
  };

  fetchBitcoinWrappingAdresses = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/wrapping/bitcoin/${accountName}`
      );
      return response.data;
    } catch (e) {
      console.log(
        `${this.APIEndpoint}/wrapping/bitcoin/${accountName}`,
        e.message
      );
    }
  };

  // VALIDATORS ----------------------- //
  fetchValidators = async () => {
    try {
      const response = await axios.get(`${this.APIEndpoint}/producers`);
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/producers`, e.message);
    }
  };

  fetchValidatorVotes = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/voted/${accountName}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/voted/${accountName}`, e.message);
    }
  };

  // AUDIT --------------------------- //
  fetchAuditStats = async () => {
    try {
      const response = await axios.post(
        `https://lb.libre.org/v1/chain/get_currency_stats`,
        {
          code: "btc.ptokens",
          symbol: "pbtc",
        }
      );
      return response.data;
    } catch (e) {
      console.log(
        `https://lb.libre.org/v1/chain/get_currency_stats`,
        e.message
      );
    }
  };

  fetchAuditList = async () => {
    try {
      const response = await axios.get(
        `https://libre-chain.gitlab.io/pbtc-audit/index.json`
      );
      return response.data;
    } catch (e) {
      console.log(
        `https://libre-chain.gitlab.io/pbtc-audit/index.json`,
        e.message
      );
    }
  };

  // FARMING --------------------------- //
  fetchFarmingStats = async () => {
    try {
      const response = await axios.get(`${this.APIEndpoint}/farming/stats`);
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/farming/stats`, e.message);
      throw new Error(e.message);
    }
  };

  fetchUserFarming = async (accountName: string) => {
    try {
      const response = await axios.get(
        `${this.APIEndpoint}/farming/${accountName}`
      );
      return response.data;
    } catch (e) {
      console.log(`${this.APIEndpoint}/farming/${accountName}`, e.message);
    }
  };

  fetchUserBTCAddress = async (accountName: string) => {
    const _pbtc = new pBTC({
      nativeBlockchain: "bitcoin",
      nativeNetwork: "mainnet",
      hostBlockchain: "libre",
      hostNetwork: "mainnet",
      eosRpc: " http://lb.libre.org",
    });
    const depositAddressObject = await _pbtc.getDepositAddress(accountName);
    const depositAddress = depositAddressObject.value;
    return depositAddress;
  };
}

export default new APIClient();
