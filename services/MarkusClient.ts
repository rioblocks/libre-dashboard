import axios from "axios";
import { JsonRpc } from "eosjs";
import { CHAIN_CONFIG } from "../utils/constants";

export interface ICreateMarkusOrderPayload {
  account: string;
  orderAsset: {
    quantity: string;
    contract: string;
  };
  minReceivable: {
    quantity: string;
    contract: string;
  };
  expiry: number;
}
export interface ICreateMarkusOrderResponse {
  identifier: string;
  tx: string;
}

class MarkusClient {
  APIEndpoint: string | undefined;
  rpc: JsonRpc;

  constructor() {
    this.APIEndpoint = process.env.NEXT_PUBLIC_MARKUS_API_ENDPOINT;
    this.rpc = new JsonRpc(CHAIN_CONFIG().hyperion.url);
  }

  createMarkusOrder = async (payload: ICreateMarkusOrderPayload) => {
    try {
      const response = await axios.post(
        `${this.APIEndpoint}/createOrder`,
        payload
      );
      return response.data as ICreateMarkusOrderResponse;
    } catch (e) {
      console.log(`${this.APIEndpoint}/createOrder`, e.message);
    }
  };

  fetchMarkusOffers = async (identifier: string) => {
    try {
      const { rows } = await this.rpc.get_table_rows({
        json: true,
        code: "mrmarkus",
        scope: identifier,
        table: "offers",
      });
      return rows;
    } catch (e) {
      console.log(`${this.APIEndpoint}/createOrder`, e.message);
    }
  };
}

export default new MarkusClient();
