import LibreLink, {
  Link,
  LinkSession,
  LinkTransport,
} from "@libre-chain/libre-link";
import LibreLinkBrowserTransport from "@libre-chain/libre-link-browser-transport";
import { ChainId } from "@libre-chain/libre-signing-request";
import AnchorLink from "anchor-link";
import AnchorLinkBrowserTransport from "anchor-link-browser-transport";
import axios from "axios";
import { JsonRpc } from "eosjs";
import moment from "moment";
import { WalletType } from "../models/Client";
import { ICurrencyBalanceRequest, ITableRowRequest } from "../models/Contract";
import { IMarkusOffer } from "../models/Markus";
import {
  ITransferToAddress,
  ITransferToken,
  IUserToken,
  liquidityTokenContracts,
} from "../models/Tokens";
import { generateProposalId } from "../utils";
import {
  CHAIN_CONFIG,
  PROPOSALS_CONTRACT,
  PROPOSALS_TOKEN,
  PROPOSALS_TRANSFER_TOKEN,
} from "../utils/constants";

export interface IWalletBalances {
  [key: string]: string;
}
export interface User {
  actor: string;
  permission: string;
}

interface WalletResponse {
  user: User;
  error: string;
}

interface IStakeLibre {
  quantity: string;
  duration: string;
}

interface IUnstakeLibre {
  index: number;
}

export interface IMintLibre {
  quantity: string;
  duration: string;
  symbol: string;
}

interface IClaimLibre {
  index: number;
}

export interface ISwapTokens {
  fromTokenName: string;
  toTokenName: string;
  fromTokenAmount: string;
  toTokenAmount: string;
  pool: string;
  markusOffer: IMarkusOffer | null;
}

export interface IAddLiquidity {
  toBuy: string;
  maxAsset1: string;
  maxAsset2: string;
  tokenFrom: IUserToken;
  tokenTo: IUserToken;
}

export interface IRemoveLiquidity {
  toSell: string;
  minAsset1: string;
  minAsset2: string;
  tokenFrom: IUserToken;
  tokenTo: IUserToken;
}

export interface IFarmingStake {
  quantity: string;
}

export interface IFarmingWithdraw {
  quantity: string;
}

class LibreClient {
  appName: string;
  session: LinkSession | null;
  link: Link | AnchorLink | null;
  transport: LinkTransport | null;
  rpc: JsonRpc | null;
  hyperionUrl: string | null;
  chainId: ChainId | null;

  constructor() {
    this.appName = String(process.env.NEXT_PUBLIC_LIBRE_APP_NAME);
    this.session = null;
    this.transport = new LibreLinkBrowserTransport();
    this.hyperionUrl = String(CHAIN_CONFIG().hyperion.url);
    this.chainId = String(CHAIN_CONFIG().hyperion.chainId) as any;
    this.link = new LibreLink({
      transport: this.transport,
      chains: [
        {
          chainId: String(CHAIN_CONFIG().hyperion.chainId),
          nodeUrl: String(CHAIN_CONFIG().hyperion.url),
        },
      ],
      scheme: "libre",
    });
    this.rpc = new JsonRpc(CHAIN_CONFIG().hyperion.url, { fetch });
  }

  login = async ({ type }: { type: WalletType }): Promise<WalletResponse> => {
    try {
      if (type === WalletType.Anchor) {
        this.link = new AnchorLink({
          transport: new AnchorLinkBrowserTransport(),
          chains: [
            {
              chainId: String(CHAIN_CONFIG().hyperion.chainId),
              nodeUrl: String(CHAIN_CONFIG().hyperion.url),
            },
          ],
        });
      }
      if (type === WalletType.Libre) {
        this.link = new LibreLink({
          transport: new LibreLinkBrowserTransport(),
          chains: [
            {
              chainId: String(CHAIN_CONFIG().hyperion.chainId),
              nodeUrl: String(CHAIN_CONFIG().hyperion.url),
            },
          ],
          scheme: "libre",
        });
      }
      const identity = await this.link.login(
        String(process.env.NEXT_PUBLIC_LIBRE_APP_NAME)
      );
      this.session = identity.session;
      localStorage.setItem(
        "dapp-user-auth",
        JSON.stringify({
          actor: identity.session.auth.actor.toString(),
          permission: identity.session.auth.permission.toString(),
          expiration: moment().add(20, "minutes"),
          type,
        })
      );

      return {
        user: {
          actor: identity.session.auth.actor.toString(),
          permission: identity.session.auth.permission.toString(),
        },
        error: "",
      };
    } catch (e) {
      return {
        user: { actor: "", permission: "" },
        error: e.message || "An error has occurred while logging in",
      };
    }
  };

  logout = async () => {
    if (!this.link || !this.session)
      return localStorage.removeItem("dapp-user-auth");
    await this.link.removeSession(
      String(process.env.NEXT_PUBLIC_LIBRE_APP_NAME),
      this.session.auth,
      this.chainId
    );
    this.session = null;
    localStorage.removeItem("dapp-user-auth");
  };

  restoreSession = async () => {
    try {
      if (!this.link)
        throw new Error("An error has occurred while restoring a session");

      let userAuth = localStorage.getItem("dapp-user-auth");

      if (userAuth) {
        userAuth = JSON.parse(userAuth);
        // LOGOUT USER IF CONNECTION HAS EXPIRED
        if (moment().isAfter(userAuth.expiration)) {
          await this.logout();
          return {
            user: null,
            error: "",
          };
        }
        if (userAuth.type === WalletType.Anchor) {
          this.link = new AnchorLink({
            transport: new AnchorLinkBrowserTransport(),
            chains: [
              {
                chainId: String(this.chainId),
                nodeUrl: String(this.hyperionUrl),
              },
            ],
          });
        }
        if (userAuth.type === WalletType.Libre) {
          this.link = new LibreLink({
            transport: new LibreLinkBrowserTransport(),
            chains: [
              {
                chainId: String(this.chainId),
                nodeUrl: String(this.hyperionUrl),
              },
            ],
            scheme: "libre",
          });
        }
        this.session = await this.link.restoreSession(
          String(process.env.NEXT_PUBLIC_LIBRE_APP_NAME),
          userAuth,
          this.chainId
        );
      }

      if (!this.session)
        throw new Error("An error has occurred while restoring a session");

      return {
        user: {
          actor: this.session.auth.actor.toString(),
          permission: this.session.auth.permission.toString(),
        },
        error: "",
      };
    } catch (e) {
      return {
        user: null,
        error: e.message || "An error has occurred while restoring a session",
      };
    }
  };

  getProposals = async (proposalId?: string) => {
    try {
      const { rows } = await this.rpc.get_table_rows({
        scope: PROPOSALS_CONTRACT,
        code: PROPOSALS_CONTRACT,
        json: true,
        table: "proposals",
        lower_bound: proposalId || "",
        upper_bound: proposalId || "",
        limit: 1000,
      });

      return rows;
    } catch (err) {
      console.warn(err);
      return [];
    }
  };

  getVotes = async (proposalId?: string) => {
    try {
      const { rows } = await this.rpc.get_table_rows({
        code: PROPOSALS_CONTRACT,
        scope: proposalId,
        json: true,
        table: "votes",
        limit: 500,
      });

      return rows;
    } catch (err) {
      console.warn(err);
      return [];
    }
  };

  getUsername = async (username: string) => {
    try {
      const result = await this.rpc?.get_account(username);
      return result;
    } catch (err) {
      console.warn(err);
      return null;
    }
  };

  createProposal = async ({
    receiver = this.session?.auth.actor,
    title,
    detail,
    amount,
    url,
    cost,
  }) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to create a proposal");
      }
      let proposalId = generateProposalId();

      let unique = false;
      while (!unique) {
        const proposalNameUsed = await this.getProposals(proposalId);
        if (proposalNameUsed.length === 0) {
          unique = true;
        } else {
          proposalId = generateProposalId();
        }
      }

      const action = [
        {
          account: PROPOSALS_CONTRACT,
          name: "create",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            creator: this.session.auth.actor,
            receiver,
            name: proposalId,
            title,
            detail,
            amount: `${amount} ${PROPOSALS_TOKEN}`,
            url: url ?? "",
          },
        },
        {
          account: PROPOSALS_TRANSFER_TOKEN,
          name: "transfer",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            to: PROPOSALS_CONTRACT,
            from: this.session.auth.actor,
            quantity: cost,
            memo: `payment:${proposalId}`,
          },
        },
      ];

      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );

      return {
        success: true,
        transactionId: result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to create a proposal.",
      };
    }
  };

  stakeLibre = async ({ quantity, duration }: IStakeLibre) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to stake your Libre");
      }
      const action = [
        {
          account: "eosio.token",
          name: "transfer",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            to: "stake.libre",
            from: this.session.auth.actor,
            symbol: "LIBRE",
            quantity: `${quantity} LIBRE`,
            memo: `stakefor:${duration}`,
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to stake your Libre.",
      };
    }
  };

  unstakeLibre = async ({ index }: IUnstakeLibre) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to unstake your Libre");
      }
      const action = [
        {
          account: "stake.libre",
          name: "unstake",
          data: {
            account: this.session.auth.actor,
            index,
          },
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to stake your Libre.",
      };
    }
  };

  claimLibre = async ({ index }: IClaimLibre) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to unstake your Libre");
      }
      const action = [
        {
          account: "stake.libre",
          name: "claim",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            account: this.session.auth.actor,
            index,
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to stake your Libre.",
      };
    }
  };

  mintLibre = async ({ quantity, duration, symbol }: IMintLibre) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to contribute your Libre");
      }
      const contractName =
        process.env.NEXT_PUBLIC_ENV === "development"
          ? "eosio.token"
          : "btc.ptokens";
      const action = [
        {
          account: contractName,
          name: "transfer",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            to: "stake.libre",
            from: this.session.auth.actor,
            symbol,
            quantity: `${quantity} ${symbol}`,
            memo: `contributefor:${duration}`,
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to contribute your Libre.",
      };
    }
  };

  voteFor = async (proposalId: string) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to vote for a proposal");
      }

      const action = [
        {
          account: PROPOSALS_CONTRACT,
          name: "votefor",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            voter: this.session.auth.actor,
            proposal: proposalId,
          },
        },
      ];

      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );

      return {
        success: true,
        transactionId: result.processed.id,
        proposalId: proposalId,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to vote for a proposal.",
      };
    }
  };

  voteAgainst = async (proposalId: string) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to vote against a proposal");
      }

      const action = [
        {
          account: PROPOSALS_CONTRACT,
          name: "voteagainst",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            voter: this.session.auth.actor,
            proposal: proposalId,
          },
        },
      ];

      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );

      return {
        success: true,
        transactionId: result.processed.id,
        proposalId: proposalId,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to vote against a proposal.",
      };
    }
  };

  voteProducer = async (producer: string) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to vote for producer");
      }
      const action = [
        {
          account: "eosio",
          name: "voteproducer",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            voter: this.session.auth.actor,
            producers: [producer],
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to vote for the producer",
      };
    }
  };

  transferTokens = async ({
    tokenName,
    to,
    from,
    quantity,
    memo,
  }: ITransferToken) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to swap");
      }
      const contractName =
        process.env.NEXT_PUBLIC_ENV === "development"
          ? "eosio.token"
          : tokenName === "PBTC"
          ? "btc.ptokens"
          : tokenName === "PUSDT"
          ? "usdt.ptokens"
          : "eosio.token";
      const action = [
        {
          account: contractName,
          name: "transfer",
          authorization: [
            {
              actor: from,
              permission: "active",
            },
          ],
          data: {
            from: from,
            to: to,
            quantity,
            memo: memo,
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message || "An error has occured while attempting to swap tokens",
      };
    }
  };

  transferToAddress = async ({
    from,
    quantity,
    memo,
    tokenContract,
    chainId,
  }: ITransferToAddress) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to swap");
      }
      const action = [
        {
          account: tokenContract,
          name: "redeem2",
          data: {
            sender: from,
            quantity,
            memo,
            user_data: "",
            chain_id: chainId,
          },
          authorization: [
            {
              actor: from,
              permission: "active",
            },
          ],
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message || "An error has occured while attempting to swap tokens",
      };
    }
  };

  swapTokens = async ({
    fromTokenName,
    toTokenName,
    fromTokenAmount,
    toTokenAmount,
    pool,
    markusOffer,
  }: ISwapTokens) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to swap");
      }
      const contractName =
        process.env.NEXT_PUBLIC_ENV === "development"
          ? "eosio.token"
          : fromTokenName === "PBTC"
          ? "btc.ptokens"
          : fromTokenName === "PUSDT"
          ? "usdt.ptokens"
          : "eosio.token";
      const contractTo = markusOffer ? "mrmarkus" : "swap.libre";
      const memo = markusOffer
        ? `fill:${markusOffer.orderId}:${markusOffer.offerId}`
        : `exchange: ${pool}, ${toTokenAmount} ${toTokenName}, Trade ${fromTokenAmount} ${fromTokenName} for ${toTokenName}`;
      const action = [
        {
          account: contractName,
          name: "transfer",
          data: {
            from: this.session.auth.actor,
            to: contractTo,
            quantity: `${fromTokenAmount} ${fromTokenName}`,
            memo,
          },
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message || "An error has occured while attempting to swap tokens",
      };
    }
  };

  addLiquidity = async ({
    toBuy,
    maxAsset1,
    maxAsset2,
    tokenFrom,
    tokenTo,
  }: IAddLiquidity) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to vote for producer");
      }
      const accountName = "swap.libre";
      const action = [
        {
          account: accountName,
          name: "openext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            payer: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenFrom.symbol
                ],
              sym: `${tokenFrom.precision},${tokenFrom.symbol}`,
            },
          },
        },
        {
          account: accountName,
          name: "openext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            payer: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenTo.symbol
                ],
              sym: `${tokenTo.precision},${tokenTo.symbol}`,
            },
          },
        },
        {
          account:
            liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
              tokenFrom.symbol
            ],
          name: "transfer",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            from: this.session.auth.actor,
            to: accountName,
            quantity: maxAsset1,
            memo: "",
          },
        },
        {
          account:
            liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
              tokenTo.symbol
            ],
          name: "transfer",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            from: this.session.auth.actor,
            to: accountName,
            quantity: maxAsset2,
            memo: "",
          },
        },
        {
          account: accountName,
          name: "addliquidity",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            to_buy: toBuy,
            max_asset1: maxAsset1,
            max_asset2: maxAsset2,
          },
        },
        {
          account: accountName,
          name: "closeext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            to: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenFrom.symbol
                ],
              sym: `${tokenFrom.precision},${tokenFrom.symbol}`,
            },
            memo: "",
          },
        },
        {
          account: accountName,
          name: "closeext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            to: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenTo.symbol
                ],
              sym: `${tokenTo.precision},${tokenTo.symbol}`,
            },
            memo: "",
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message || "An error has occured while attempting to swap tokens",
      };
    }
  };

  removeLiquidity = async ({
    toSell,
    minAsset1,
    minAsset2,
    tokenFrom,
    tokenTo,
  }: IRemoveLiquidity) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to vote for producer");
      }
      const accountName = "swap.libre";
      const action = [
        {
          account: accountName,
          name: "openext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            payer: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenFrom.symbol
                ],
              sym: `${tokenFrom.precision},${tokenFrom.symbol}`,
            },
          },
        },
        {
          account: accountName,
          name: "openext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            payer: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenTo.symbol
                ],
              sym: `${tokenTo.precision},${tokenTo.symbol}`,
            },
          },
        },
        {
          account: accountName,
          name: "remliquidity",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            to_sell: toSell,
            min_asset1: minAsset1,
            min_asset2: minAsset2,
          },
        },
        {
          account: accountName,
          name: "closeext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            to: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenFrom.symbol
                ],
              sym: `${tokenFrom.precision},${tokenFrom.symbol}`,
            },
            memo: "",
          },
        },
        {
          account: accountName,
          name: "closeext",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            user: this.session.auth.actor,
            to: this.session.auth.actor,
            ext_symbol: {
              contract:
                liquidityTokenContracts[process.env.NEXT_PUBLIC_ENV][
                  tokenTo.symbol
                ],
              sym: `${tokenTo.precision},${tokenTo.symbol}`,
            },
            memo: "",
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message || "An error has occured while attempting to swap tokens",
      };
    }
  };

  getTableRows = async ({
    code,
    scope,
    table,
    limit = 10,
    lowerBound,
    upperBound,
  }: ITableRowRequest): Promise<any[]> => {
    if (!this.rpc) return [];
    const response = await this.rpc.get_table_rows({
      json: true, // Get the response as json
      code,
      scope,
      table,
      limit, // Maximum number of rows that we want to get
      reverse: false, // Optional: Get reversed data
      show_payer: false, // Optional: Show ram payer,
      lower_bound: lowerBound,
      upper_bound: upperBound,
    });
    return response.rows;
  };

  async getCurrencyBalance({
    code,
    account,
    symbol,
  }: ICurrencyBalanceRequest): Promise<any> {
    const response = await this.rpc?.get_currency_balance(
      code,
      account,
      symbol
    );
    return response;
  }

  getUserLiquidity = async (accountName: string) => {
    let data = {
      BTCUSD: 0,
      BTCLIB: 0,
    };
    const response = await this.rpc.get_table_rows({
      code: "swap.libre",
      table: "accounts",
      scope: accountName,
      json: true,
    });
    if (!response) return data;
    response.rows.forEach((r) => {
      data = {
        ...data,
        [r.balance.split(" ")[1]]: r.balance.split(" ")[0],
      };
    });
    return data;
  };

  // STAKES ----------------------- //
  fetchUserStakes = async (accountName: string) => {
    try {
      const response = await this.getStakeByAccount(accountName);
      return response;
    } catch (e) {
      console.log("getStakeByAccount", e.message);
    }
  };

  getTokenInformation = async () => {
    const response1 = await this.rpc.get_table_rows({
      json: true, // Get the response as json
      code: "swap.libre", // Contract that we target
      scope: "BTCUSD", // Account that owns the data
      table: "stat", // Table name
      limit: 10, // Maximum number of rows that we want to get
      reverse: false, // Optional: Get reversed data
      show_payer: false, // Optional: Show ram payer
    });
    const response2 = await this.rpc.get_table_rows({
      json: true, // Get the response as json
      code: "swap.libre", // Contract that we target
      scope: "BTCLIB", // Account that owns the data
      table: "stat", // Table name
      limit: 10, // Maximum number of rows that we want to get
      reverse: false, // Optional: Get reversed data
      show_payer: false, // Optional: Show ram payer
    });
    return [...response1.rows, ...response2.rows];
  };

  getDAOInformation = async () => {
    const response = await this.rpc.get_table_rows({
      code: "dao.libre",
      table: "params2",
      scope: "dao.libre",
      json: true,
    });
    return response.rows[0];
  };

  farmingStake = async ({ quantity }: IFarmingStake) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to update your stake");
      }
      const action = [
        {
          account: "swap.libre",
          name: "transfer",
          data: {
            from: this.session.auth.actor,
            to: "farm.libre",
            quantity,
            memo: "",
          },
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to stake your Libre.",
      };
    }
  };

  farmingWithdraw = async ({ quantity }: IFarmingStake) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to update your stake");
      }
      const action = [
        {
          account: "farm.libre",
          name: "withdraw",
          data: {
            account: this.session.auth.actor,
            quantity,
          },
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to stake your Libre.",
      };
    }
  };

  farmingClaim = async (pool: string) => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to update your stake");
      }
      const action = [
        {
          account: "reward.libre",
          name: "claim",
          data: {
            account: this.session.auth.actor,
            pool,
          },
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to stake your Libre.",
      };
    }
  };

  getUserMintRush = async (accountName: string) => {
    const response = await this.rpc.get_table_rows({
      code: "mint.libre",
      table: "account",
      scope: "mint.libre",
      lower_bound: accountName,
      upper_bound: accountName,
      json: true,
    });
    return response.rows;
  };

  mintRushClaim = async () => {
    try {
      if (!this.session) {
        throw new Error("Must be logged in to claim your tokens");
      }
      const action = [
        {
          account: "mint.libre",
          name: "claim",
          authorization: [
            {
              actor: this.session.auth.actor,
              permission: "active",
            },
          ],
          data: {
            account: this.session.auth.actor,
          },
        },
      ];
      const result = await this.session.transact(
        { actions: action },
        { broadcast: true }
      );
      return {
        success: true,
        transactionId: result && result.processed && result.processed.id,
      };
    } catch (e) {
      return {
        success: false,
        error:
          e.message ||
          "An error has occured while attempting to claim you tokens.",
      };
    }
  };

  // DAO
  getDaoPower = async () => {
    const response = await this.rpc.get_table_rows({
      code: "stake.libre",
      table: "power",
      scope: "stake.libre",
      json: true,
      limit: 1000,
    });
    return response.rows;
  };

  getDaoCurrency = async ({
    code,
    symbol,
  }: {
    code: string;
    symbol: string;
  }) => {
    const response = await axios.post(
      `${this.hyperionUrl}/v1/chain/get_currency_balance`,
      {
        code,
        account: "dao.libre",
        symbol,
      }
    );
    return response.data[0];
  };

  getUserDaoPower = async (accountName: string) => {
    const response = await this.rpc.get_table_rows({
      code: "stake.libre",
      table: "power",
      scope: "stake.libre",
      lower_bound: accountName,
      upper_bound: accountName,
      json: true,
    });
    return response.rows;
  };

  async getStakeByAccount(account: string) {
    const result = await axios.post(
      `${CHAIN_CONFIG().hyperion.url}/v1/chain/get_table_rows`,
      {
        code: "stake.libre",
        table: "stake",
        scope: "stake.libre",
        json: true,
        index_position: 2,
        lower_bound: account,
        upper_bound: account,
        key_type: "name",
        limit: 1000,
      }
    );

    return result.data.rows.filter(
      (row: { account: string }) => row.account === account
    );
  }

  async fetchUserTokens(account: string) {
    try {
      const response = await axios.get(
        `${CHAIN_CONFIG().hyperion.url}/v2/state/get_tokens?account=${account}`
      );
      return response.data;
    } catch (e) {
      console.log(
        `${CHAIN_CONFIG().hyperion.url}/v2/state/get_tokens?account=${account}`,
        e.message
      );
    }
  }
}

export default new LibreClient();
