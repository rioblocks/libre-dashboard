import axios from "axios";
import { GetTableByScopeResult } from "eosjs/dist/eosjs-rpc-interfaces";
import { IPNetworkFeesResponse } from "../models/Fees";

export const PNETWORK_PRECISION = 1000000000000000000;
export const LIBRE_MAINNET = "0x026776fa";

class ChainClient {
  chainEndpoint: string;

  constructor() {
    this.chainEndpoint = String(process.env.NEXT_PUBLIC_LIBRE_CHAIN_ENDPOINT);
  }

  searchUsers = async (query: string): Promise<any[] | undefined> => {
    try {
      const result: GetTableByScopeResult = await axios.post(
        `${this.chainEndpoint}/search`,
        { query, limit: 1000 }
      );
      return result.data.rows;
    } catch (e) {
      console.log(`${this.chainEndpoint}/search`, e.message);
    }
  };

  cacheBTCAddress = async ({ accountName, address, network }) => {
    const result = await axios.post(
      `${this.chainEndpoint}/ptokens/ptoken-wrapping`,
      {
        account: accountName,
        network: network,
        address: address,
      }
    );

    return result.data;
  };

  registerLightningPayment = async ({
    invoice,
    accountName,
  }: {
    invoice: string;
    accountName: string;
  }) => {
    const result = await axios.post(
      `${this.chainEndpoint}/lightning-payments/register`,
      {
        paymentRequest: invoice.invoice,
        account: accountName,
      }
    );
    return result.data;
  };

  fetchLightningFee = async ({
    invoice,
    accountName,
  }: {
    invoice: string;
    accountName: string;
  }) => {
    const result = await axios.post(
      `${this.chainEndpoint}/lightning-payments/fee-v2`,
      {
        paymentRequest: invoice,
        accountName,
      }
    );
    return result.data;
  };

  getPNetworkFees = async (
    tokenSymbol: string,
    amount: number
  ): Promise<IPNetworkFeesResponse> => {
    const response = await axios.get(
      `${this.chainEndpoint}/ptokens/fees/${tokenSymbol}`
    );

    return response.data;
  };

  async calculateFees(tokenSymbol: string): Promise<any> {
    const apiUrl = "https://pnetwork-node-2a.eu.ngrok.io/v3/";

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        jsonrpc: "2.0",
        method: "node_getSupportedChainsByAsset",
        params: [tokenSymbol],
        id: 1,
      }),
    };

    const data = await fetch(apiUrl, requestOptions);
    const response = await data.json();
    return response;
  }
}

export default new ChainClient();
