import type { AppProps } from "next/app";
import Head from "next/head";
import Script from "next/script";
import { useCallback, useEffect, useState } from "react";
import { QueryCache, QueryClient, QueryClientProvider } from "react-query";
import "react-virtualized/styles.css";
import styled, { ThemeProvider } from "styled-components";
import { MobileMenu } from "../components/MobileMenu";
import { NavBar } from "../components/NavBar";
import { AuthProvider } from "../providers/AuthProvider";
import { ConnectWalletProvider } from "../providers/ConnectWalletProvider";
import { NotificationProvider } from "../providers/NotificationProvider";
import { WindowSizeProvider } from "../providers/WindowSizeProvider";
import "../styles/globals.css";
import { defaultTheme } from "../uikit/themeConfig";
import { CHAIN_CONFIG, MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const Container = styled.div`
  min-height: 100vh;
  width: 100%;
  background: ${(p) => p.theme.backgroundColor};
  display: flex;
  justify-content: center;
`;

const Content = styled.div`
  max-width: 1200px;
  width: 100%;
  padding: 0 20px;

  @media (max-width: ${MediaQueryWidths.small}px) {
    padding: 0 5px;
  }
`;

export const queryClient = new QueryClient();
export const queryCache = new QueryCache();

function MyApp({ Component, pageProps }: AppProps) {
  const [hyperionUrl, setHyperionUrl] = useState("");
  const [chainId, setChainId] = useState("");
  const [mobileMenuOpen, setMobileMenuOpen] = useState<boolean>(false);

  const initializeApp = async () => {
    localStorage.setItem("hyperionUrl", CHAIN_CONFIG().hyperion.url);
    localStorage.setItem("chainId", CHAIN_CONFIG().hyperion.chainId);
    setHyperionUrl(CHAIN_CONFIG().hyperion.url);
    setChainId(CHAIN_CONFIG().hyperion.chainId);
  };

  const handleMobileMenuClose = useCallback(() => {
    setMobileMenuOpen(false);
  }, []);

  const handleMobileMenuToggle = useCallback(() => {
    setMobileMenuOpen(!mobileMenuOpen);
  }, [mobileMenuOpen]);

  useEffect(() => {
    initializeApp();
  }, []);

  if (!hyperionUrl || !chainId) return <></>;

  return (
    <Wrapper id="mainAppElement">
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;600&display=swap"
          rel="stylesheet"
        />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0"
        />
        <meta
          property="og:image"
          content="https://dashboard.libre.org/libre-dashboard.png"
        />
        <meta property="og:title" content="Libre Chain Dashboard" />
        <meta
          property="og:description"
          content="Libre chain core governance functions for the LIBRE coin: Stake. Vote. DAO. Mint Rush."
        />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
      </Head>
      <WindowSizeProvider>
        <QueryClientProvider client={queryClient}>
          <AuthProvider>
            <ThemeProvider theme={defaultTheme}>
              <NotificationProvider>
                <ConnectWalletProvider>
                  <div id="modal-root"></div>
                  <NavBar handleMobileMenuToggle={handleMobileMenuToggle} />
                  <MobileMenu
                    open={mobileMenuOpen}
                    handleClose={handleMobileMenuClose}
                  />
                  <Container>
                    <Content>
                      <Component {...pageProps} />
                    </Content>
                  </Container>
                </ConnectWalletProvider>
              </NotificationProvider>
            </ThemeProvider>
          </AuthProvider>
        </QueryClientProvider>
      </WindowSizeProvider>
      <Script
        strategy="afterInteractive"
        src={`https://www.googletagmanager.com/gtag/js?id=G-RHEGJ6SMJH`}
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-RHEGJ6SMJH');
          `}
      </Script>
    </Wrapper>
  );
}

export default MyApp;
