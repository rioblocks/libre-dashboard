import { orderBy, shuffle } from "lodash";
import Head from "next/head";
import Image from "next/image";
import { useMemo, useState } from "react";
import styled, { css } from "styled-components";
import { HeaderDetails } from "../components/HeaderDetails";
import { PageTitle } from "../components/PageTitle";
import ValidatorsTable from "../components/validators/ValidatorsTable";
import useValidators from "../hooks/queries/validators/useValidators";
import useValidatorVotes from "../hooks/queries/validators/useValidatorVotes";
import { useAuthContext } from "../providers/AuthProvider";
import { SectionEmpty } from "../uikit/SectionEmpty";
import { SectionWrapper } from "../uikit/Sections";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: 30px;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
`;

const HeaderRow = styled.div`
  display: flex;
  align-items: center;
`;

const HeaderDescription = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  margin-top: 10px;
  color: ${(p) => p.theme.descriptionText};
`;

const SortDescription = styled.span`
  font-size: 12px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.descriptionText};
  text-transform: uppercase;
  margin-right: 10px;
`;

const SortOption = styled.div<{ disabled?: boolean }>`
  display: flex;
  align-items: center;
  color: ${(p) => p.theme.buttonOrangeText};
  background: ${(p) => p.theme.buttonOrange};
  font-size: 12px;
  padding: 5px 10px;
  border-radius: 20px;
  cursor: pointer;

  ${(p) =>
    p.disabled &&
    css`
      color: ${(p) => p.theme.buttonGreyText};
      background: ${(p) => p.theme.buttonGrey};
    `}
`;

const SortImg = styled.div<{ down?: boolean }>`
  display: flex;
  align-items: center;
  ${(p) =>
    !p.down &&
    css`
      transform: rotate(180deg);
      margin-top: 0px;
    `}
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
`;

const Break = styled.div`
  height: 20px;
`;

enum SortTypes {
  Shuffle = "shuffle",
  RankAsc = "rankasc",
  RankDesc = "rankdesc",
}

export default function Validators() {
  const { currentUser } = useAuthContext();
  const { data, isLoading } = useValidators();
  const { data: validatorVotes } = useValidatorVotes(currentUser.actor);

  const [sort, setSort] = useState<SortTypes>(SortTypes.Shuffle);

  const sortedData = useMemo(() => {
    if (!data) return [];
    if (sort === SortTypes.Shuffle) return shuffle(data);
    if (sort === SortTypes.RankAsc) return orderBy(data, "rank");
    if (sort === SortTypes.RankDesc) return orderBy(data, "rank").reverse();
    return data;
  }, [data, sort]);

  const renderRankSort = () => {
    const rankDisabled =
      sort !== SortTypes.RankAsc && sort !== SortTypes.RankDesc;
    const rankSort =
      sort === SortTypes.RankAsc ? SortTypes.RankDesc : SortTypes.RankAsc;
    const rankChevronDown =
      sort === SortTypes.Shuffle || sort === SortTypes.RankAsc;
    return (
      <SortOption disabled={rankDisabled} onClick={() => setSort(rankSort)}>
        Rank
        <SortImg down={rankChevronDown}>
          <Image
            src={
              sort !== SortTypes.RankAsc && sort !== SortTypes.RankDesc
                ? "/icons/nav-chevron-down-grey.svg"
                : "/icons/nav-chevron-down.svg"
            }
            width={20}
            height={20}
          />
        </SortImg>
      </SortOption>
    );
  };

  const renderContent = () => {
    if ((!data || !data.length) && !isLoading) return <SectionEmpty />;
    return (
      <ValidatorsTable
        isLoading={isLoading}
        data={sortedData}
        votes={validatorVotes}
      />
    );
  };

  return (
    <>
      <Head>
        <title>LIBRE | Validators</title>
      </Head>
      <Wrapper>
        <Header>
          <PageTitle title="Validators" />
          <HeaderDetails />
        </Header>
        <SectionWrapper>
          <Header>
            <HeaderCol>
              <Title>Validators</Title>
              <HeaderDescription>
                Using your votes choose 1 validator.
              </HeaderDescription>
            </HeaderCol>
            <HeaderCol>
              <HeaderRow>
                <SortDescription>sort:</SortDescription>
                {renderRankSort()}
              </HeaderRow>
            </HeaderCol>
          </Header>
          <Break />
          {renderContent()}
        </SectionWrapper>
      </Wrapper>
    </>
  );
}
