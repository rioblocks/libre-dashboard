import Head from "next/head";
import styled from "styled-components";
import { EmptyConnectWallet } from "../../components/emptystates/EmptyConnectWallet";
import { HeaderDetails } from "../../components/HeaderDetails";
import { PageTitle } from "../../components/PageTitle";
import { MyReferralsSection } from "../../components/wallet/myreferrals/MyReferralsSection";
import { MyTokensSection } from "../../components/wallet/mytokens/MyTokensSection";
import { StakesSection } from "../../components/wallet/stakes/StakesSection";
import { useAuthContext } from "../../providers/AuthProvider";

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: ${(p) => p.theme.backgroundColor};
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Content = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
`;

const Break = styled.div`
  flex-shrink: 0;
  height: 24px;
`;

export default function Wallet() {
  const { currentUser, isLoadingUser } = useAuthContext();

  if (isLoadingUser) return <></>;

  const renderContent = () => {
    if (!currentUser || !currentUser.actor) {
      return (
        <>
          <Break />
          <EmptyConnectWallet />
        </>
      );
    }
    if (currentUser.actor && currentUser.permission !== "active") {
      return (
        <Wrapper>
          <Header>
            <PageTitle title="Wallet" />
            <HeaderDetails />
          </Header>
          <Content>
            <MyTokensSection isStatic />
          </Content>
        </Wrapper>
      );
    }
    return (
      <Wrapper>
        <Header>
          <PageTitle title="Wallet" />
          <HeaderDetails />
        </Header>
        <Content>
          <MyTokensSection />
          <Break />
          <StakesSection />
          <Break />
          <MyReferralsSection />
          <Break />
        </Content>
      </Wrapper>
    );
  };

  return (
    <>
      <Head>
        <title>LIBRE | Wallet</title>
      </Head>
      {renderContent()}
    </>
  );
}
