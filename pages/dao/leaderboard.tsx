import Head from "next/head";
import styled from "styled-components";
import { TopAccountsTable } from "../../components/dao/TopAccountsTable";
import { PageTitle } from "../../components/PageTitle";
import useDaoStats from "../../hooks/queries/dao/useDaoStats";
import { DetailsBlock } from "../../uikit/DetailsBlock";
import { HeaderBack } from "../../uikit/HeaderBack";

const Block = styled.div`
  padding: 24px 24px 0;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
`;

const BlockContainer = styled.div`
  display: flex;
`;

const Break = styled.div`
  height: 20px;
`;

export default function Leaderboard() {
  const { data } = useDaoStats();
  return (
    <>
      <Head>
        <title>{`LIBRE | DAO | LeaderBoard`}</title>
      </Head>
      <Break />
      <Break />
      <HeaderBack link="/dao" />
      <PageTitle title="Voting Power Leaderboard" />
      <BlockContainer>
        <DetailsBlock
          title="Total Voting Power"
          details={String(data?.voting_power.toLocaleString("en-US"))}
          totalBlocks={1}
        />
      </BlockContainer>
      <Break />
      <PageTitle title="Top Accounts by Voting Power" />
      <Block>
        <TopAccountsTable />
      </Block>
      <Break />
    </>
  );
}
