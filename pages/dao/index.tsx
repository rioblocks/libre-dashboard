import Head from "next/head";
import { useContext, useState } from "react";
import styled from "styled-components";
import { DaoModal } from "../../components/dao/DaoModal";
import { DaoProposals } from "../../components/dao/DaoProposals";
import { GovernanceOverviewSection } from "../../components/dao/GovernanceOverviewSection";
import { MyVotingPowerSection } from "../../components/dao/MyVotingPowerSection";
import { TopAccountsSection } from "../../components/dao/TopAccountsSection";
import { useAuthContext } from "../../providers/AuthProvider";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

export default function DAO() {
  const { handleOnOpen: handleLoginOpen } = useContext(ConnectWalletContext);
  const { currentUser } = useAuthContext();

  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const handleModalClose = () => setModalOpen(false);
  const handleModalOpen = async () => {
    if (!currentUser || !currentUser.actor) {
      handleLoginOpen();
      return;
    }
    setModalOpen(true);
  };

  return (
    <>
      <Head>
        <title>LIBRE | DAO</title>
      </Head>
      <Wrapper>
        <GovernanceOverviewSection />
        <MyVotingPowerSection handleModalOpen={handleModalOpen} />
        <DaoProposals handleModalOpen={handleModalOpen} />
        <TopAccountsSection />
      </Wrapper>
      <DaoModal open={modalOpen} handleOnClose={handleModalClose} />
    </>
  );
}
