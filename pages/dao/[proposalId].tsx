import moment from "moment";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { useMemo } from "react";
import styled, { useTheme } from "styled-components";
import useDaoStats from "../../hooks/queries/dao/useDaoStats";
import useProposal from "../../hooks/queries/proposals/useProposal";
import useProposalProperties from "../../hooks/queries/proposals/useProposalProperties";
import { useVoteAgainst } from "../../hooks/queries/proposals/useVoteAgainst";
import { useVoteFor } from "../../hooks/queries/proposals/useVoteFor";
import useVotes from "../../hooks/queries/proposals/useVotes";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";
import { HeaderBack } from "../../uikit/HeaderBack";
import { Label } from "../../uikit/Label";
import { ProgressBar } from "../../uikit/ProgressBar";
import { getProposalStatus, renderVotesPercentage } from "../../utils";
import { MediaQueryWidths } from "../../utils/constants";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Header = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 32px;
  margin-bottom: 40px;
`;

const Title = styled.span`
  font-size: 39.8px;
  font-weight: 600;
  line-height: 1.21;
  color: ${(p) => p.theme.titleText};
  display: inline-block;
  margin: 10px 0;
`;

const HeaderRow = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  min-height: 2rem;
`;

const RowText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 2rem;
  color: ${(p) => p.theme.detailBlockTitle};
  margin-left: 8px;
`;

const Blocks = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  gap: 20px;

  @media (max-width: ${MediaQueryWidths.small}px) {
    flex-direction: column;
  }
`;

const Block = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 24px 24px 24px 24px;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: ${(p) => p.theme.detailBlockBackground};
`;

const BlockHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const BlockTitle = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const ProgressRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 12px 0 20px;
`;

const ProgressText = styled.span`
  font-size: 10.2px;
  font-weight: 600;
  line-height: 1.56;
  color: ${(p) => p.theme.labelGrey};
  text-transform: uppercase;
`;

const Border = styled.div`
  height: 1px;
  width: 100%;
  background: #ebebed;
`;

const List = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 0 0;
  margin-bottom: 10px;
  max-height: 400px;
  overflow: auto;
`;

const ListRow = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 0;
`;

const ListItem = styled.span`
  font-size: 16px;
  font-weight: 500;
  line-height: 1.5;
  color: #313046;
`;

const Details = styled.div`
  padding: 24px;
  margin-top: 24px;
  border-radius: 16px;
  box-shadow: 0 0.5px 2px 0 rgba(104, 103, 119, 0.16),
    0 0 1px 0 rgba(49, 48, 70, 0.08);
  background-color: #fff;
  width: 100%;
`;

const DetailsTitle = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
  margin-bottom: 20px;
  display: inline-block;
`;

const DetailsText = styled.span`
  font-size: 16px;
  font-weight: 500;
  line-height: 1.5;
  color: #313046;
  display: block;
  margin-top: 20px;
`;

const BtnContainer = styled.div`
  display: flex;
  margin-top: auto;

  button {
    width: 100%;
    text-align: center;
  }
`;

const ImgBreak = styled.div`
  width: 8px;
`;

export default function ProposalDetails() {
  const theme = useTheme();
  const { query } = useRouter();
  const { currentUser } = useAuthContext();

  const { data: proposal } = useProposal(query.proposalId);
  const { data: votes } = useVotes(query.proposalId);
  const { mutate: voteFor } = useVoteFor();
  const { mutate: voteAgainst } = useVoteAgainst();
  const { data: proposalProperties } = useProposalProperties();
  const { data: daoStats } = useDaoStats();

  const handleVoteFor = () => voteFor(query.proposalId);
  const handleVoteAgainst = () => voteAgainst(query.proposalId);

  const userVote = useMemo(() => {
    if (!currentUser || !currentUser.actor) return null;
    if (!votes || !votes.length) return null;
    const vote = votes.find((v) => v.voter === currentUser.actor);
    if (!vote) return null;
    return vote;
  }, [votes, currentUser]);

  const neededVotesToPass = useMemo(() => {
    if (!proposalProperties || !daoStats) return null;
    return `${Number(
      (proposalProperties.vote_threshold / 100) * daoStats.voting_power
    ).toLocaleString("en-US", {
      maximumFractionDigits: 0,
    })} minimum votes needed to pass`;
  }, [proposalProperties, daoStats]);

  const renderAccountVotes = (isFor: boolean) => {
    if (!votes || !votes.length) return <></>;
    return votes
      .filter((vote) => (isFor ? vote.is_for === 1 : vote.is_for === 0))
      .map((vote) => {
        return (
          <ListRow key={vote.voter}>
            <ListItem>{vote.voter}</ListItem>
            <ListItem>{Number(vote.quantity).toLocaleString("en-US")}</ListItem>
          </ListRow>
        );
      });
  };

  const renderWalletsFor = () => {
    const voteCount = votes?.filter((vote) => vote.is_for === 1).length;
    return `${voteCount} ${voteCount === 1 ? "wallet" : "wallets"}`;
  };

  const renderWalletsAgainst = () => {
    const voteCount = votes?.filter((vote) => vote.is_for === 0).length;
    return `${voteCount} ${voteCount === 1 ? "wallet" : "wallets"}`;
  };

  const renderVoteForButton = () => {
    if (!proposal) return <></>;
    if (proposal.status === 2) {
      if (userVote && userVote.is_for !== 0) {
        return (
          <BtnContainer>
            <MediumButton
              disabled
              color={theme.buttonGreyText}
              backgroundColor={theme.buttonGrey}
              text={
                <>
                  <Image
                    src={"/icons/checkmark-circle.svg"}
                    height={16}
                    width={16}
                  />
                  <ImgBreak />
                  Vote For
                </>
              }
              onClick={handleVoteFor}
            />
          </BtnContainer>
        );
      }
      return (
        <BtnContainer>
          <MediumButton
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            text="Vote For"
            onClick={handleVoteFor}
          />
        </BtnContainer>
      );
    }
    return <></>;
  };

  const renderVoteAgainstButton = () => {
    if (!proposal) return <></>;
    if (proposal.status === 2) {
      if (userVote && userVote.is_for === 0) {
        return (
          <BtnContainer>
            <MediumButton
              disabled
              color={theme.buttonGreyText}
              backgroundColor={theme.buttonGrey}
              text={
                <>
                  <Image
                    src={"/icons/checkmark-circle.svg"}
                    height={16}
                    width={16}
                  />
                  <ImgBreak />
                  Vote Against
                </>
              }
              onClick={handleVoteAgainst}
            />
          </BtnContainer>
        );
      }
      return (
        <BtnContainer>
          <MediumButton
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            text="Vote Against"
            onClick={handleVoteAgainst}
          />
        </BtnContainer>
      );
    }
    return <></>;
  };

  if (!proposal) return <></>;

  return (
    <>
      <Head>
        <title>{`LIBRE | DAO | ${proposal.title}`}</title>
      </Head>
      <Wrapper>
        <Header>
          <HeaderBack link="/dao" />
          <Title>{proposal.title}</Title>
          <HeaderRow>
            <Label
              color={theme.labelPurple}
              backgroundColor={theme.labelPurpleBackground}
            >
              {getProposalStatus(proposal.status)}
            </Label>
            <RowText>{moment(proposal.expires_on).fromNow()}</RowText>
            {neededVotesToPass && (
              <>
                <RowText>-</RowText>
                <RowText>{neededVotesToPass}</RowText>
              </>
            )}
          </HeaderRow>
        </Header>
        <Blocks>
          <Block>
            <BlockHeader>
              <BlockTitle>For</BlockTitle>
              <BlockTitle>
                {Number(proposal.votes_for).toLocaleString("en-US")}
              </BlockTitle>
            </BlockHeader>
            <ProgressBar
              color={theme.progressBarGreen}
              percentage={renderVotesPercentage({ isFor: true, proposal })}
            />
            <ProgressRow>
              <ProgressText>{renderWalletsFor()}</ProgressText>
              <ProgressText>Votes</ProgressText>
            </ProgressRow>
            <Border />
            <List>{renderAccountVotes(true)}</List>
            {renderVoteForButton()}
          </Block>
          <Block>
            <BlockHeader>
              <BlockTitle>Against</BlockTitle>
              <BlockTitle>
                {Number(proposal.votes_against).toLocaleString("en-US")}
              </BlockTitle>
            </BlockHeader>
            <ProgressBar
              color={theme.progressBarRed}
              percentage={renderVotesPercentage({ isFor: false, proposal })}
            />
            <ProgressRow>
              <ProgressText>{renderWalletsAgainst()}</ProgressText>
              <ProgressText>Votes</ProgressText>
            </ProgressRow>
            <Border />
            <List>{renderAccountVotes(false)}</List>
            {renderVoteAgainstButton()}
          </Block>
        </Blocks>
        <Details>
          <DetailsTitle>Receiver</DetailsTitle>
          <Border />
          <DetailsText>{proposal.receiver}</DetailsText>
        </Details>
        <Details>
          <DetailsTitle>Amount</DetailsTitle>
          <Border />
          <DetailsText>{proposal.amount}</DetailsText>
        </Details>
        <Details>
          <DetailsTitle>Details</DetailsTitle>
          <Border />
          <DetailsText>{proposal.detail}</DetailsText>
        </Details>
        {proposal.url && (
          <Details>
            <DetailsTitle>Url</DetailsTitle>
            <Border />
            <DetailsText>
              <a href={proposal.url} target="_blank">
                {proposal.url}
              </a>
            </DetailsText>
          </Details>
        )}
      </Wrapper>
    </>
  );
}
