import styled from "styled-components";
import { PageTitle } from "../components/PageTitle";
import { MyTokensSection } from "../components/wallet/mytokens/MyTokensSection";

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: ${(p) => p.theme.backgroundColor};
`;

const Content = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export default function UserDetails() {
  return (
    <Wrapper>
      <Header>
        <PageTitle title="Wallet" />
      </Header>
      <Content>
        <MyTokensSection isStatic />
      </Content>
    </Wrapper>
  );
}
