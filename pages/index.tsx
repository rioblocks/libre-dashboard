import Head from "next/head";
import styled from "styled-components";
import { ChainActivitySection } from "../components/dashboard/chainactivity/ChainActivitySection";
import { PoolsSection } from "../components/dashboard/pools/PoolsSection";
import { TokensSection } from "../components/dashboard/tokens/TokensSection";
import { HeaderDetails } from "../components/HeaderDetails";
import { PageTitle } from "../components/PageTitle";

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: ${(p) => p.theme.backgroundColor};
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 30px;
`;

const Break = styled.div`
  height: 24px;
`;

export default function Dashboard() {
  return (
    <div>
      <Head>
        <title>LIBRE | Dashboard</title>
        <meta name="description" content="LIBRE DeFi Dashboard" />
        <meta property="og:title" content="Libre DeFi Dashboard" />
        <meta
          property="og:image"
          content="https://uploads-ssl.webflow.com/6352f1cf299a9bb136925a0e/6413656a71485460fc9e98ab_dashboard-og.png"
        />
      </Head>
      <Wrapper>
        <Header>
          <PageTitle title="Dashboard" />
          <HeaderDetails />
        </Header>
        <Content>
          <ChainActivitySection />
          <Break />
          <TokensSection />
          <Break />
          <PoolsSection />
        </Content>
      </Wrapper>
    </div>
  );
}
