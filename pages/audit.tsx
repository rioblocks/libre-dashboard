import Head from "next/head";
import styled from "styled-components";
import OnChainValidationTable from "../components/audit/OnChainValidationTable";
import ProofOfAssetsTable from "../components/audit/ProofOfAssetsTable";
import { HeaderDetails } from "../components/HeaderDetails";
import { PageTitle } from "../components/PageTitle";
import { SectionWrapper } from "../uikit/Sections";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: 30px;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const SectionHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0 30px;
`;

const Description = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: #9695a0;
  margin-top: -20px;
  margin-bottom: 30px;
  max-width: 500px;

  @media (max-width: ${MediaQueryWidths.small}px) {
    margin-top: -10px;
    padding: 15px 15px 0;
  }
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
`;

const Break = styled.div`
  height: 32px;
`;

export default function Audit() {
  return (
    <>
      <Head>
        <title>LIBRE | Audit</title>
      </Head>
      <Wrapper>
        <Header>
          <HeaderCol>
            <PageTitle title="Audit" />
            <Description>
              PBTC on Libre is 100% backed by Bitcoin using non-affiliated
              PNetwork decentralized pegging nodes. This page is updated every
              24 hours.
            </Description>
          </HeaderCol>
          <HeaderDetails />
        </Header>
        <SectionWrapper>
          <SectionHeader>
            <HeaderCol>
              <Title>Proof of Assets</Title>
            </HeaderCol>
          </SectionHeader>
          <ProofOfAssetsTable />
        </SectionWrapper>
        <Break />
        <SectionWrapper>
          <SectionHeader>
            <HeaderCol>
              <Title>On-Chain Validation</Title>
            </HeaderCol>
          </SectionHeader>
          <OnChainValidationTable />
        </SectionWrapper>
      </Wrapper>
    </>
  );
}
