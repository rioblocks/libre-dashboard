import Image from "next/image";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import {
  AutoSizer,
  Column,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled, { useTheme } from "styled-components";
import { displayTokenPrecisions, IUserToken } from "../../../models/Tokens";
import { useAuthContext } from "../../../providers/AuthProvider";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { MediumButton } from "../../../uikit/Button";
import { IconPairs } from "../../../uikit/IconPairs";
import {
  headerRenderer,
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../../uikit/Table";
import { formatCurrencyWithPrecision } from "../../../utils";
import { TokenWithUSDValue } from "../../TokenWithUSDValue";
import { AddBalanceModal } from "./AddBalanceModal";

const CellItem = styled.span`
  display: flex;
  align-items: center;
  font-size: 15px;
  color: ${(p) => p.theme.titleText};
  font-weight: 500;
`;

const AssetIcon = styled.div`
  margin-right: 15px;
`;

const CellColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

const Btn = styled.div`
  display: flex;
`;

interface IMyTokensTable {
  data: IUserToken[];
  isLoading: boolean;
}

export const MyTokensTable = ({ data, isLoading }: IMyTokensTable) => {
  const theme = useTheme();
  const [addBalanceModalOpen, setAddBalanceModalOpen] = useState(false);
  const { resolveRowStyle, tableHeight, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 5,
  });
  const { currentUser } = useAuthContext();
  const router = useRouter();
  const { handleSendOpen } = useContext(WalletSendContext);

  useEffect(() => {
    if (
      router.query &&
      router.query.receive &&
      router.query.receive === "btc"
    ) {
      setTimeout(() => {
        setAddBalanceModalOpen(true);
      }, 500);
    }
  }, [router.query]);

  const isRowLoaded = ({ index }: { index: number }) => !!data[index];

  const loadMoreRows = async () => {
    return;
  };

  const handleReceiveButtonClick = (currencyName: string) => {
    if (currencyName === "PBTC" || currencyName === "BTCL") {
      setAddBalanceModalOpen(true);
    }
    if (currencyName === "LIBRE") {
      window.open("https://spindrop.libre.org/", "_blank");
    }
    if (currencyName === "PUSDT" || currencyName === "USDL") {
      window.open(
        "https://dapp.ptokens.io/swap?asset=usdt&from=eth&to=libre",
        "_blank"
      );
    }
  };

  const handleSendButtonClick = (currencyName: string) => {
    if (currencyName === "PBTC" || currencyName === "BTCL") {
      window.open(
        "https://dapp.ptokens.io/swap?asset=btc&from=libre&to=btc",
        "_blank"
      );
    }
    if (currencyName === "LIBRE") {
      window.open("https://spindrop.libre.org/", "_blank");
    }
    if (currencyName === "PUSDT" || currencyName === "USDL") {
      window.open(
        "https://dapp.ptokens.io/swap?asset=usdt&from=libre&to=eth",
        "_blank"
      );
    }
  };

  const handleModalClose = () => {
    if (
      router.query &&
      router.query.receive &&
      router.query.receive === "btc"
    ) {
      router.replace("/wallet", undefined, { shallow: true });
    }
    setAddBalanceModalOpen(false);
  };

  const handleLink = (tokenName: string) => {
    window.open(
      `https://www.libreblocks.io/address/${currentUser.actor}`,
      "_blank"
    );
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "name") {
      let icon = "/icons/libre-asset-icon.svg";
      if (String(cellData) === "LIBRE") icon = "/icons/libre-asset-icon.svg";
      if (String(cellData) === "PUSDT") icon = "/icons/usdt-asset-icon.png";
      if (String(cellData) === "PBTC") icon = "/icons/btc-asset-icon.svg";
      if (String(cellData) === "BTCUSD" || String(cellData) === "BTCLIB") {
        return (
          <CellItem>
            <AssetIcon>
              <IconPairs
                imgSrc1={"/icons/btc-asset-icon.svg"}
                imgSrc2={
                  String(cellData) === "BTCLIB"
                    ? "/icons/libre-asset-icon.svg"
                    : "/icons/usdt-asset-icon.png"
                }
              />
            </AssetIcon>
            {cellData}
          </CellItem>
        );
      }
      return (
        <CellItem>
          <AssetIcon>
            <Image src={icon} width={33} height={32} alt={icon} />
          </AssetIcon>
          {cellData}
        </CellItem>
      );
    }
    if (dataKey === "addAsset") {
      if (rowData.name === "BTCUSD" || rowData.name === "BTCLIB") return <></>;
      if (rowData.name === "LIBRE") return <></>;
      return (
        <CellItem style={{ justifyContent: "flex-end" }}>
          <Btn>
            <MediumButton
              text="Receive"
              onClick={() => handleReceiveButtonClick(cellData)}
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
            />
          </Btn>
        </CellItem>
      );
    }
    if (dataKey === "sendAsset") {
      if (rowData.name === "BTCUSD" || rowData.name === "BTCLIB") return <></>;
      if (rowData.name === "LIBRE")
        return (
          <CellItem style={{ justifyContent: "flex-end" }}>
            <Btn>
              <MediumButton
                text="Send"
                onClick={() => handleSendOpen(rowData)}
                color={theme.buttonOrangeText}
                backgroundColor={theme.buttonOrange}
              />
            </Btn>
          </CellItem>
        );
      return (
        <CellItem style={{ justifyContent: "flex-end" }}>
          <Btn>
            <MediumButton
              text="Send"
              onClick={() => handleSendOpen(rowData)}
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
            />
          </Btn>
        </CellItem>
      );
    }

    if (dataKey === "total") {
      if (rowData.name === "PBTC" || rowData.name === "BTCL") {
        return (
          <CellItem>
            <TokenWithUSDValue
              link={() => handleLink(rowData.name)}
              tokenSymbol={rowData.name}
              value={cellData}
            />
          </CellItem>
        );
      }
      if (rowData.name === "PUSDT" || rowData.name === "USDL") {
        return (
          <CellItem>
            <CellColumn>
              <TokenWithUSDValue
                link={() => handleLink(rowData.name)}
                tokenSymbol={rowData.name}
                value={formatCurrencyWithPrecision(
                  Number(cellData),
                  displayTokenPrecisions[rowData.name]
                )}
              />
            </CellColumn>
          </CellItem>
        );
      }
      if (rowData.name === "LIBRE") {
        return (
          <CellItem>
            <CellColumn>
              <TokenWithUSDValue
                link={() => handleLink(rowData.name)}
                tokenSymbol={rowData.name}
                value={formatCurrencyWithPrecision(
                  Number(cellData),
                  displayTokenPrecisions[rowData.name]
                )}
              />
            </CellColumn>
          </CellItem>
        );
      }
      if (!cellData) return <CellItem>--</CellItem>;
      return (
        <CellItem>
          <TokenWithUSDValue
            tokenSymbol={rowData.name}
            value={formatCurrencyWithPrecision(
              Number(cellData),
              displayTokenPrecisions[rowData.name]
            )}
          />
        </CellItem>
      );
    }

    if (dataKey === "total_staked_payout") {
      if (
        rowData.name === "PBTC" ||
        rowData.name === "BTCL" ||
        rowData.name === "USDL" ||
        rowData.name === "PUSDT"
      ) {
        return <CellItem>--</CellItem>;
      }
      if (!cellData) return <CellItem>--</CellItem>;
      return (
        <CellItem>
          <CellColumn>
            <TokenWithUSDValue
              tokenSymbol={rowData.name}
              value={formatCurrencyWithPrecision(
                Number(cellData),
                displayTokenPrecisions[rowData.name]
              )}
            />
          </CellColumn>
        </CellItem>
      );
    }

    if (dataKey === "unstaked") {
      if (!cellData) return <CellItem>--</CellItem>;
      if (rowData.name === "PBTC" || rowData.name === "BTCL") {
        return (
          <CellItem>
            <CellColumn>
              <TokenWithUSDValue tokenSymbol={rowData.name} value={cellData} />
            </CellColumn>
          </CellItem>
        );
      }
      return (
        <CellItem>
          <CellColumn>
            <TokenWithUSDValue
              tokenSymbol={rowData.name}
              value={formatCurrencyWithPrecision(
                Number(cellData),
                displayTokenPrecisions[rowData.name]
              )}
            />
          </CellColumn>
        </CellItem>
      );
    }

    return <CellItem>{cellData ? cellData : "--"}</CellItem>;
  };

  return (
    <TableWrapper height={tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={1}
          >
            {({ onRowsRendered, registerChild }) => (
              <Table
                ref={registerChild}
                width={width < 800 ? 800 : width}
                height={height}
                headerHeight={HEADER_HEIGHT}
                rowHeight={ROW_HEIGHT}
                rowCount={data.length}
                rowGetter={({ index }) => data[index]}
                rowStyle={({ index }) =>
                  resolveRowStyle(index + 1 === data.length)
                }
                onRowsRendered={onRowsRendered}
                noRowsRenderer={noRowsRenderer}
                overscanRowCount={5}
              >
                <Column
                  label="Asset"
                  dataKey={"name"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Total"
                  dataKey={"total"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Staked Payout"
                  dataKey={"total_staked_payout"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Unstaked"
                  dataKey={"unstaked"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label=""
                  dataKey={"addAsset"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label=""
                  dataKey={"sendAsset"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
              </Table>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
      <AddBalanceModal
        open={addBalanceModalOpen}
        handleOnClose={handleModalClose}
      />
    </TableWrapper>
  );
};
