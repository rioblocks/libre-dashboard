import Image from "next/image";
import { useContext, useMemo, useState } from "react";
import styled from "styled-components";
import useUserTokens from "../../../hooks/queries/tokens/useUserTokens";
import { useExchangeRates } from "../../../hooks/useExchangeRates";
import { useAuthContext } from "../../../providers/AuthProvider";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { AutosizeInput } from "../../../uikit/AutosizeInput";
import { formatRoundedDownTokenPrecision } from "../../../utils";
import { SendWalletValueTypes } from "../../../utils/constants";
import { AmountSendMax } from "./AmountSendMax";

const InputContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin: 0px 0 30px;
  position: relative;
`;

const InputLeft = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  position: absolute;
  right: 0;
  cursor: pointer;
  margin: 1px;
  margin-top: 25px;
`;

const InputLeftCircle = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: #f5f5f6;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  margin: 1px;
`;

const InputLeftText = styled.span`
  margin-top: 3px;
  font-size: 13px;
  color: ${(p) => p.theme.buttonGreyText};
`;

const Container = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
`;

const Token = styled.span`
  display: flex;
  position: absolute;
  bottom: -10px;
  color: ${(p) => p.theme.greyText};
`;

const Balance = styled.span`
  color: #c0c0c7;
  font-weight: 400;
  font-size: 14px;
`;

const SendAll = styled.span`
  color: ${(p) => p.theme.buttonOrangeText};
  font-size: 14px;
  background: ${(p) => p.theme.buttonOrange};
  padding: 5px 20px;
  border-radius: 20px;
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;

export const AmountSendInput = () => {
  const [value, setValue] = useState<string>("0.00");
  const [valueType, setValueType] = useState<string>(
    SendWalletValueTypes.TOKEN
  );
  const [initialValue, setInitialValue] = useState<string>("0.00");

  const {
    selectedToken,
    setAmount,
    setValueType: setContextValueType,
  } = useContext(WalletSendContext);
  const { data: exchangeRates } = useExchangeRates();
  const { currentUser } = useAuthContext();
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });

  const tokenData = useMemo(() => {
    if (!userTokens) return;
    return userTokens.find((t) => t.symbol === selectedToken.symbol);
  }, [userTokens, selectedToken]);

  const handleOnChange = ({ target }) => {
    const inputValue = target.value;
    const regex = /^\d*\.?\d*$/; // Regular expression to match decimal values

    if (regex.test(inputValue)) {
      setValue(inputValue);
    }
    if (valueType === SendWalletValueTypes.TOKEN)
      return setAmount(target.value);
    if (!exchangeRates || !value) return;
    const rate = exchangeRates[selectedToken.symbol];
    const newValue = formatRoundedDownTokenPrecision({
      value: Number(Number(target.value) / rate),
      precision: selectedToken.precision,
    });
    setAmount(newValue);
  };

  const handleSwitchType = () => {
    let prevType = valueType;
    if (valueType === SendWalletValueTypes.TOKEN) {
      setValueType(SendWalletValueTypes.USD);
      setContextValueType(SendWalletValueTypes.USD);
    } else {
      setValueType(SendWalletValueTypes.TOKEN);
      setContextValueType(SendWalletValueTypes.TOKEN);
    }

    if (!exchangeRates || !value) return;
    const rate = exchangeRates[selectedToken.symbol];

    if (prevType === SendWalletValueTypes.USD) {
      const newValue = formatRoundedDownTokenPrecision({
        value: Number(Number(value) / rate),
        precision: selectedToken.precision,
      });
      setInitialValue(newValue);
      setAmount(newValue);
      return setValue(newValue);
    }

    setValue(Number(Number(value) * rate).toFixed(2));
    setInitialValue(Number(Number(value) * rate).toFixed(2));
  };

  const handleMaxClick = (maxAmount: number) => {
    if (valueType === SendWalletValueTypes.USD) {
      if (!exchangeRates || !value) return;
      const rate = exchangeRates[selectedToken.symbol];
      const newValue = Number(Number(maxAmount) * rate).toFixed(2);
      setValue(newValue);
      return setInitialValue(newValue);
    }
    setValue(String(maxAmount));
    setInitialValue(String(maxAmount));
  };

  const convertedPrice = useMemo(() => {
    if (!exchangeRates || !selectedToken) return "";
    const rate = exchangeRates[selectedToken.symbol];
    if (!rate) return "";
    if (valueType === SendWalletValueTypes.TOKEN)
      return `$${Number(rate * Number(value)).toLocaleString("en-US", {
        maximumFractionDigits: 2,
      })}`;
    return Number(Number(value) / rate).toLocaleString("en-US", {
      maximumFractionDigits: selectedToken.precision,
    });
  }, [exchangeRates, selectedToken, value, valueType]);

  return (
    <>
      <InputContainer>
        <Container>
          <AutosizeInput
            placeholder="0.00"
            unit={valueType !== SendWalletValueTypes.TOKEN ? <>$</> : <></>}
            label={
              valueType === SendWalletValueTypes.TOKEN
                ? selectedToken.symbol
                : undefined
            }
            initialValue={initialValue}
            onChange={handleOnChange}
            value={value}
          />
        </Container>
        <Token>
          {convertedPrice}{" "}
          {valueType !== SendWalletValueTypes.TOKEN && selectedToken.symbol}
        </Token>
        <InputLeft onClick={handleSwitchType}>
          <InputLeftCircle>
            <Image
              src="/icons/swap-icon.svg"
              width={18}
              height={18}
              style={{ opacity: 0.5 }}
            />
          </InputLeftCircle>
          <InputLeftText>
            {valueType === SendWalletValueTypes.TOKEN
              ? SendWalletValueTypes.USD
              : selectedToken.symbol}
          </InputLeftText>
        </InputLeft>
      </InputContainer>
      <Row>
        <Balance>{`Balance: ${tokenData?.unstaked} ${selectedToken.symbol}`}</Balance>
        <AmountSendMax handleChange={handleMaxClick}>
          <SendAll>Send All</SendAll>
        </AmountSendMax>
      </Row>
    </>
  );
};
