import { useContext } from "react";
import styled from "styled-components";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { AmountSendInput } from "./AmountSendInput";
import { SendDetails } from "./SendDetails";
import { SendInput } from "./SendInput";
import { SendSubmit } from "./SendSubmit";
import { UserList } from "./UserList";

const Wrapper = styled.div`
  margin-top: 20px;
  position: relative;
`;

export const SendToSelect = () => {
  const { recipient, expected } = useContext(WalletSendContext);

  return (
    <Wrapper>
      <AmountSendInput />
      {!recipient && (
        <>
          <SendInput />
          <UserList />
        </>
      )}
      <SendDetails active={!!recipient} />
      <SendSubmit active={true} />
    </Wrapper>
  );
};
