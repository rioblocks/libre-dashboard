import { useContext } from "react";
import styled, { css } from "styled-components";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { MediaQueryWidths } from "../../../utils/constants";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  transition: 400ms all ease;
`;

const Content = styled.div<{ active?: boolean }>`
  display: flex;
  flex-direction: column;
  border-radius: 16px;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  background: ${(p) => p.theme.detailBlockBackground};
  max-width: 552px;
  padding: 12px 24px;
  min-height: 100px;
  width: 100%;
  transition: 500ms all ease;
  opacity: 0;
  pointer-events: none;
  display: none;
  margin-top: 10px;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    padding: 10px;
  }

  ${(p) =>
    p.active &&
    css`
      min-height: 0;
      opacity: 1;
      pointer-events: all;
      display: flex;
    `}
`;

const Header = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
`;

const SelectedTitle = styled.span``;
const Recipient = styled.span`
  color: ${(p) => p.theme.buttonGreyText};
  word-break: break-word;
`;

export const SendDetails = ({ active }: { active: boolean }) => {
  const {
    selectedToken,
    recipient,
    setRecipient,
    fee,
    total,
    expected,
    setSearchQuery,
  } = useContext(WalletSendContext);

  const handleRecipientClick = () => {
    setRecipient(null);
    setSearchQuery("");
  };

  const renderFee = () => {
    let unit =
      recipient?.type === "ethereum"
        ? process.env.NEXT_PUBLIC_ENV === "development"
          ? "USDL"
          : "PUSDT"
        : "SATS";
    return `${fee} ${unit}`;
  };

  return (
    <>
      <Wrapper onClick={handleRecipientClick} style={{ cursor: "pointer" }}>
        <Content active={active}>
          <Header>
            <SelectedTitle>Recipient</SelectedTitle>
            <Recipient>
              {recipient?.type === "lightning"
                ? recipient.destination.invoice
                : recipient?.destination}
            </Recipient>
          </Header>
        </Content>
      </Wrapper>
      <Wrapper>
        <Content active={active}>
          <Header>
            <SelectedTitle>Fee</SelectedTitle>
            <Recipient>{renderFee()}</Recipient>
          </Header>
        </Content>
      </Wrapper>
      <Wrapper>
        <Content active={active}>
          <Header>
            <SelectedTitle>Total</SelectedTitle>
            <Recipient>{`${total} ${selectedToken.symbol}`}</Recipient>
          </Header>
        </Content>
      </Wrapper>
      {expected > 0 ? (
        <Wrapper>
          <Content active={active}>
            <Header>
              <SelectedTitle>Expected</SelectedTitle>
              <Recipient>
                {expected} {selectedToken.symbol}
              </Recipient>
            </Header>
          </Content>
        </Wrapper>
      ) : (
        <></>
      )}
    </>
  );
};
