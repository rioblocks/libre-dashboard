import { useContext } from "react";
import styled from "styled-components";
import useUserTokens from "../../../hooks/queries/tokens/useUserTokens";
import { useAuthContext } from "../../../providers/AuthProvider";
import { WalletSendContext } from "../../../providers/WalletSendProvider";

const Container = styled.div`
  text-align: center;
  cursor: pointer;
`;

export const AmountSendMax = ({ handleChange, children }) => {
  const { currentUser } = useAuthContext();
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });
  const { selectedToken, setAmount } = useContext(WalletSendContext);

  const handleClick = () => {
    const token = userTokens?.find((t) => t.symbol === selectedToken.symbol);
    if (!token) return;
    setAmount(token.unstaked);
    handleChange(token.unstaked);
  };

  return <Container onClick={handleClick}>{children}</Container>;
};
