import React, { useState } from "react";
import Lottie from "react-lottie";
import styled, { useTheme } from "styled-components";
import inviteAnimation from "../../../uikit/animations/invite-animation.json";
import { MediumButton } from "../../../uikit/Button";
import { ReferralInviteModal } from "./ReferralInviteModal";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  border-radius: 16px;
  background: #ffffff;
  min-height: 400px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ImageContainer = styled.div`
  margin-bottom: 24px;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  text-align: center;
  color: #04021d;
`;

const Description = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  text-align: center;
  color: ${(p) => p.theme.descriptionText};
  max-width: 294px;
  margin: 20px 0 30px;
`;

const BtnContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 144px;

  button {
    width: 100%;
  }
`;

export const ReferralsEmptyState = () => {
  const theme = useTheme();
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: inviteAnimation,
  };

  return (
    <Wrapper>
      <Content>
        <ImageContainer>
          <Lottie options={defaultOptions} width={121} height={121} />
        </ImageContainer>
        <Title>Give LIBRE, get LIBRE</Title>
        <Description>
          Invite your friends to LIBRE and earn LIBRE for everyone who downloads
          the app.
        </Description>
        <BtnContainer>
          <MediumButton
            text="Invite"
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            onClick={() => setModalOpen(true)}
          />
        </BtnContainer>
      </Content>
      <ReferralInviteModal
        open={modalOpen}
        handleOnClose={() => setModalOpen(false)}
      />
    </Wrapper>
  );
};
