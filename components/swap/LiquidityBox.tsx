import Image from "next/image";
import { useRouter } from "next/router";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import styled from "styled-components";
import { useAddLiquidity } from "../../hooks/queries/tokens/useAddLiquidity";
import { usePoolSupply } from "../../hooks/tokens/usePoolSupply";
import { useInputPriceConversion } from "../../hooks/useInputPriceConversion";
import { usePoolPriceConversion } from "../../hooks/usePoolPriceConversion";
import { useSwapTokenData } from "../../hooks/useSwapTokenData";
import { IUserToken, liquidityTokenPrecisions } from "../../models/Tokens";
import { NotificationContext } from "../../providers/NotificationProvider";
import { LoadingAnimation } from "../../uikit/LoadingAnimation";
import { Notification } from "../../uikit/Notification";
import { formatRoundedDownTokenPrecision } from "../../utils";
import {
  MAX_ASSET_FEE_MULTIPLIER,
  MediaQueryWidths,
} from "../../utils/constants";
import { CurrencySelector } from "./CurrencySelector";
import { LiquidityButton } from "./LiquidityButton";
import { LiquidityDetailsBox } from "./LiquidityDetailsBox";
import { SwapAmountInput } from "./SwapAmountInput";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 100%;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 40px;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    margin-top: 10px;
  }
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 16px;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  padding: 24px;
  background: ${(p) => p.theme.detailBlockBackground};
  max-width: 552px;
  width: 100%;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    padding: 10px;
  }
`;

const Title = styled.span`
  font-size: 27.7px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const Description = styled.span`
  color: ${(p) => p.theme.descriptionText};
  font-size: 14px;
  margin-top: 10px;

  a {
    text-decoration: underline;
  }
`;

const Box = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  position: relative;
`;

const Row = styled.div`
  width: 100%;
  padding: 20px 16px;
  border: solid 1px #ebebed;
  border-radius: 8px;
`;

const RowLine = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;

  &:last-child {
    margin-top: 10px;
  }
`;

const RowHeader = styled.span`
  display: flex;
  align-items: center;
  color: ${(p) => p.theme.detailBlockTitle};
  font-weight: 500;
  font-size: 14px;
`;

const HeadRight = styled.div`
  margin-left: 5px;
`;

const MiddleContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  height: 72px;
`;

const MiddleAction = styled.div`
  width: 48px;
  height: 48px;
  padding: 12px;
  border: solid 1px #ebebed;
  border-radius: 50%;
  background-color: ${(p) => p.theme.detailBlockBackground};
  position: absolute;
  display: flex;
  align-self: center;
  cursor: pointer;
`;

const ButtonContainer = styled.div`
  width: 100%;
  margin-top: 20px;

  button {
    width: 100%;
  }
`;

const NotificationContainer = styled.div`
  width: 100%;
  margin: 20px 0 0;
`;

const emptyTokenFrom = {
  apy: 0,
  enabled: true,
  name: process.env.NEXT_PUBLIC_ENV === "development" ? "BTCL" : "PBTC",
  staked: 0,
  symbol: process.env.NEXT_PUBLIC_ENV === "development" ? "BTCL" : "PBTC",
  total: 0,
  unstaked: 0,
  precision: 0,
  icon: "",
  total_staked_payout: 0,
};

const emptyTokenTo = {
  apy: 0,
  enabled: true,
  name: process.env.NEXT_PUBLIC_ENV === "development" ? "USDL" : "PUSDT",
  staked: 0,
  symbol: process.env.NEXT_PUBLIC_ENV === "development" ? "USDL" : "PUSDT",
  total: 0,
  unstaked: 0,
  precision: 0,
  icon: "",
  total_staked_payout: 0,
};

export default function LiquidityBox() {
  const router = useRouter();

  const { handleOnOpen: handleNotification } = useContext(NotificationContext);

  const { mutateAsync: addLiquidity } = useAddLiquidity();
  const poolSupply = usePoolSupply();
  const { tokens, isLoadingTokens } = useSwapTokenData();

  const [activeTokenFrom, setActiveTokenFrom] =
    useState<IUserToken>(emptyTokenFrom);
  const [activeTokenTo, setActiveTokenTo] = useState<IUserToken>(emptyTokenTo);
  const [fromTokenValue, setFromTokenValue] = useState<string>("0.00");
  const [toTokenValue, setToTokenValue] = useState<string>("0.00");
  const [error, setError] = useState<boolean>(false);
  const [pool, setPool] = useState<string>("BTCUSD");

  const { toBuy, maxAsset1, maxAsset2, hasBalanceError } =
    usePoolPriceConversion({
      poolSupply: poolSupply,
      pool,
      userTokens: tokens,
      input1Token: activeTokenFrom,
      input2Token: activeTokenTo,
      amount1: Number(fromTokenValue),
      amount2: Number(toTokenValue),
    });

  const handleFormSubmit = ({ type, value }) => {
    if (error) setError(false);
    if (type === "to") {
      const { convertedToken1Price } = useInputPriceConversion({
        poolSupply: poolSupply,
        pool:
          activeTokenFrom.symbol === "LIBRE" || activeTokenTo.symbol === "LIBRE"
            ? "BTCLIB"
            : "BTCUSD",
        userTokens: tokens,
        input1Token: activeTokenFrom,
        input2Token: activeTokenTo,
        amount1: Number(fromTokenValue),
        amount2: Number(value),
      });
      setFromTokenValue(String(convertedToken1Price));
      return setToTokenValue(value);
    }
    const { convertedToken2Price } = useInputPriceConversion({
      poolSupply: poolSupply,
      pool:
        activeTokenFrom.symbol === "LIBRE" || activeTokenTo.symbol === "LIBRE"
          ? "BTCLIB"
          : "BTCUSD",
      userTokens: tokens,
      input1Token: activeTokenFrom,
      input2Token: activeTokenTo,
      amount1: Number(value),
      amount2: Number(toTokenValue),
    });
    setToTokenValue(String(convertedToken2Price));
    return setFromTokenValue(value);
  };

  const handleSetActiveTokenFrom = useCallback(
    (token: IUserToken) => {
      setActiveTokenFrom(token);
    },
    [activeTokenFrom]
  );

  const handleSetActiveTokenTo = useCallback(
    (token: IUserToken) => {
      setActiveTokenTo(token);
    },
    [activeTokenTo]
  );

  const handleSubmit = async () => {
    const toBuyValue = formatRoundedDownTokenPrecision({
      value: Number(toBuy),
      precision: liquidityTokenPrecisions[process.env.NEXT_PUBLIC_ENV][pool],
    });
    const maxAsset1Value = formatRoundedDownTokenPrecision({
      value: Number(maxAsset1),
      precision: activeTokenFrom.precision,
    });
    const maxAsset2Value = formatRoundedDownTokenPrecision({
      value: Number(maxAsset2),
      precision: activeTokenTo.precision,
    });

    if (!activeTokenFrom || !activeTokenFrom.symbol || !toBuyValue) {
      return setError(true);
    }

    const payload = {
      toBuy: `${toBuyValue} ${pool}`,
      maxAsset1: `${maxAsset1Value} ${activeTokenFrom.symbol}`,
      maxAsset2: `${maxAsset2Value} ${activeTokenTo.symbol}`,
      tokenFrom: activeTokenFrom,
      tokenTo: activeTokenTo,
    };

    const response = await addLiquidity(payload);

    if (response && response.success) {
      setFromTokenValue("0.00");
      setToTokenValue("0.00");
      setTimeout(() => {
        handleNotification(response.transactionId);
      }, 1000);
      return;
    }
  };

  const tokensAvailableforLiquidity = useMemo(() => {
    if (!tokens) return [];
    return tokens.filter((t) => {
      if (
        t.symbol === "PBTC" ||
        t.symbol === "PUSDT" ||
        t.symbol === "BTCL" ||
        t.symbol === "USDL" ||
        t.symbol === "LIBRE"
      )
        return true;
      return false;
    });
  }, [tokens]);

  const renderError = () => {
    if (!error) return <></>;
    return (
      <NotificationContainer>
        <Notification
          type="error"
          text="There was an error. Please try again."
        />
      </NotificationContainer>
    );
  };

  const token1MaxValue = useMemo(() => {
    return activeTokenFrom.unstaked * (1 - (MAX_ASSET_FEE_MULTIPLIER - 1));
  }, [maxAsset1, activeTokenFrom]);

  useEffect(() => {
    if (!tokens || !tokens.length) {
      setActiveTokenFrom(emptyTokenFrom);
      return setActiveTokenTo(emptyTokenTo);
    }
    const fromToken = tokens.find((t) => {
      if (process.env.NEXT_PUBLIC_ENV === "development") {
        return t.symbol === "BTCL";
      }
      return t.symbol === "PBTC";
    });
    if (fromToken) setActiveTokenFrom(fromToken);
    const toToken = tokens.find((t) => {
      if (process.env.NEXT_PUBLIC_ENV === "development") {
        return t.symbol === "USDL";
      }
      return t.symbol === "PUSDT";
    });
    if (toToken) setActiveTokenTo(toToken);
  }, [tokens]);

  useEffect(() => {
    if (!tokens || !tokens.length) return;
    if (activeTokenFrom.symbol === activeTokenTo.symbol)
      setActiveTokenTo(tokens.find((t) => t.symbol !== activeTokenTo.symbol));
    if (activeTokenFrom.symbol === "LIBRE" || activeTokenTo.symbol === "LIBRE")
      setPool("BTCLIB");
    else setPool("BTCUSD");
  }, [activeTokenFrom, activeTokenTo]);

  useEffect(() => {
    if (router.query && router.query.pool && router.query.pool == "BTCLIB") {
      const token = tokens?.find((t) => t.symbol === "LIBRE");
      if (!token) return;
      setActiveTokenTo(token);
    }
  }, [router.query, tokens]);

  return (
    <Container>
      <Content>
        <Title>Pool</Title>
        <Description>
          Add liquidity to receive LP tokens.&nbsp;View{" "}
          <a href="http://info.libre.org/" target="_blank">
            analytics
          </a>{" "}
          here.
        </Description>
        <Box>
          <Row>
            <RowLine>
              <RowHeader>Input 1</RowHeader>
              <RowHeader>
                {`Balance:`}
                <HeadRight>
                  {isLoadingTokens ? (
                    <LoadingAnimation size="tiny" />
                  ) : (
                    activeTokenFrom.unstaked.toLocaleString("en-us", {
                      minimumFractionDigits: activeTokenFrom.precision,
                    })
                  )}
                </HeadRight>
              </RowHeader>
            </RowLine>
            <RowLine>
              <SwapAmountInput
                enabled
                showMax
                maxValue={token1MaxValue}
                type="from"
                token={activeTokenFrom}
                handleSubmit={handleFormSubmit}
                initialValue={fromTokenValue}
              />
              <CurrencySelector
                disabled
                tokens={tokensAvailableforLiquidity}
                activeToken={activeTokenFrom}
                onChange={handleSetActiveTokenFrom}
              />
            </RowLine>
          </Row>
          <MiddleContainer>
            <MiddleAction>
              <Image src="/icons/plus.svg" width={40} height={40} />
            </MiddleAction>
          </MiddleContainer>
          <Row>
            <RowLine>
              <RowHeader>Input 2</RowHeader>
              <RowHeader>
                {`Balance:`}
                <HeadRight>
                  {isLoadingTokens ? (
                    <LoadingAnimation size="tiny" />
                  ) : (
                    activeTokenTo.unstaked.toLocaleString("en-us", {
                      minimumFractionDigits: activeTokenTo.precision,
                    })
                  )}
                </HeadRight>
              </RowHeader>
            </RowLine>
            <RowLine>
              <SwapAmountInput
                enabled
                type="to"
                token={activeTokenTo}
                handleSubmit={handleFormSubmit}
                initialValue={toTokenValue}
              />
              <CurrencySelector
                tokens={tokensAvailableforLiquidity}
                activeToken={activeTokenTo}
                onChange={handleSetActiveTokenTo}
                disabledTokens={[activeTokenFrom.symbol]}
              />
            </RowLine>
          </Row>
        </Box>
        {renderError()}
        <ButtonContainer>
          <LiquidityButton
            activeTokenTo={activeTokenTo}
            toTokenValue={toTokenValue}
            fromTokenValue={fromTokenValue}
            hasBalanceError={hasBalanceError}
            handleSubmit={handleSubmit}
          />
        </ButtonContainer>
      </Content>
      <LiquidityDetailsBox />
    </Container>
  );
}
