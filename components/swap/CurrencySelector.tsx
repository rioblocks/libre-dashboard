import Image from "next/image";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { useClickedOutside } from "../../hooks/useClickedOutside";
import { IUserToken } from "../../models/Tokens";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import { findTokenSymbol } from "../../utils";

const Wrapper = styled.div`
  flex-shrink: 0;
`;

const Container = styled.div`
  position: relative;
  z-index: 100;
`;

const Selector = styled.div`
  display: flex;
  align-items: center;
  display: flex;
  align-items: center;
  padding: 8px;
  border-radius: 8px;
  background-color: #f5f5f6;
  cursor: pointer;
`;

const SelectorText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  text-align: center;
  color: ${(p) => p.theme.titleText};
  margin: 0 8px;
  -webkit-tap-highlight-color: transparent;
  user-select: none;
`;

const Dropdown = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  right: 0;
  margin-top: 5px;
  width: 140px;
  border-radius: 8px;
`;

const DropdownItem = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 8px;
  cursor: pointer;
  background: #f5f5f6;

  &:hover {
    background: #f7f7f7;
  }

  &:first-of-type {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
  }

  &:last-child {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }
`;

interface ICurrencySelector {
  disabled?: boolean;
  activeToken: IUserToken;
  tokens: IUserToken[] | undefined;
  disabledTokens?: string[];
  showConnectWallet?: boolean;
  onChange?: (token: IUserToken) => void;
}

export const CurrencySelector = ({
  disabled,
  activeToken,
  tokens,
  disabledTokens,
  showConnectWallet,
  onChange,
}: ICurrencySelector) => {
  const wrapperRef = useRef(null);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [dropdownDisabled, setDropdownDisabled] = useState<boolean>(false);
  const { handleOnOpen } = useContext(ConnectWalletContext);

  const handleToggleOpen = () => {
    if (showConnectWallet) return handleOnOpen();
    setIsOpen(!isOpen);
  };

  const handleClickedOutside = useCallback(() => {
    if (!isOpen) return;
    setIsOpen(false);
  }, [isOpen]);

  const handleSubmit = (token: IUserToken) => {
    if (!onChange) return;
    onChange(token);
    setIsOpen(false);
  };

  const renderActiveToken = () => {
    return (
      <>
        {activeToken.symbol && (
          <Image
            src={findTokenSymbol(activeToken.symbol)}
            width={24}
            height={24}
          />
        )}
        <SelectorText>{activeToken.symbol}</SelectorText>
      </>
    );
  };

  const renderDropdown = () => {
    if (!isOpen || disabled) return <></>;
    if (!tokens || !tokens.length) return <></>;
    const filteredTokens = tokens.filter((t) => {
      if (disabledTokens && disabledTokens.length)
        return (
          !disabledTokens.includes(t.symbol) && t.symbol !== activeToken.symbol
        );
      return t.symbol !== activeToken.symbol;
    });
    return (
      <Dropdown>
        {filteredTokens.map((t) => (
          <DropdownItem key={t.name} onClick={() => handleSubmit(t)}>
            <Image src={findTokenSymbol(t.symbol)} width={24} height={24} />
            <SelectorText>{t.name}</SelectorText>
          </DropdownItem>
        ))}
      </Dropdown>
    );
  };

  useClickedOutside(wrapperRef, handleClickedOutside);

  useEffect(() => {
    if (disabledTokens?.includes(activeToken.symbol)) {
      const safeToken = tokens?.find((t) => !disabledTokens.includes(t.symbol));
      if (!safeToken) return;
      handleSubmit(safeToken);
    }
    if (disabledTokens?.length === Number(tokens?.length) - 1) {
      setDropdownDisabled(true);
    } else {
      if (dropdownDisabled) setDropdownDisabled(false);
    }
  }, [disabledTokens, tokens, activeToken]);

  return (
    <Wrapper ref={wrapperRef}>
      <Container>
        <Selector onClick={handleToggleOpen}>
          {renderActiveToken()}
          {!disabled && !dropdownDisabled && (
            <Image
              src={isOpen ? "/icons/chevron-up.svg" : "/icons/chevron-down.svg"}
              width={16}
              height={16}
            />
          )}
        </Selector>
        {renderDropdown()}
      </Container>
    </Wrapper>
  );
};
