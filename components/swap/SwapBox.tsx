import Image from "next/image";
import { useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import { useSwapTokens } from "../../hooks/queries/tokens/useSwapTokens";
import { useDisabledTokens } from "../../hooks/useDisabledTokens";
import { useSwapMarketMaker } from "../../hooks/useSwapMarketMaker";
import { useSwapTokenData } from "../../hooks/useSwapTokenData";
import { useTokenPriceConversion } from "../../hooks/useTokenPriceConversion";
import { IUserToken, emptyTokenFrom, emptyTokenTo } from "../../models/Tokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { ISwapTokens } from "../../services/LibreClient";
import { LoadingAnimation } from "../../uikit/LoadingAnimation";
import { Notification } from "../../uikit/Notification";
import { formatRoundedDownTokenPrecision } from "../../utils";
import { MediaQueryWidths } from "../../utils/constants";
import { CurrencySelector } from "./CurrencySelector";
import { SwapAmountInput } from "./SwapAmountInput";
import { SwapButton } from "./SwapButton";
import { SwapPrice } from "./SwapPrice";
import { SwapTokenDetailsBox } from "./SwapTokenDetailsBox";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 100%;
  justify-content: flex-start;
  align-items: center;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    margin-top: 10px;
  }
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 16px;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  padding: 24px;
  background: ${(p) => p.theme.detailBlockBackground};
  max-width: 552px;
  width: 100%;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    padding: 10px;
  }
`;

const Title = styled.span`
  font-size: 27.7px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const Description = styled.span`
  color: ${(p) => p.theme.descriptionText};
  font-size: 14px;
  margin-top: 10px;

  a {
    text-decoration: underline;
  }
`;

const Box = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  position: relative;
`;

const Row = styled.div`
  width: 100%;
  padding: 20px 16px;
  border: solid 1px #ebebed;
  border-radius: 8px;
`;

const RowLine = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;

  &:last-child {
    margin-top: 10px;
  }
`;

const RowHeader = styled.span`
  display: flex;
  align-items: center;
  color: ${(p) => p.theme.detailBlockTitle};
  font-weight: 500;
  font-size: 14px;
`;

const HeadRight = styled.div`
  margin-left: 5px;
`;

const MiddleContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  height: 72px;
`;

const MiddleAction = styled.div`
  width: 48px;
  height: 48px;
  padding: 12px;
  border: solid 1px #ebebed;
  border-radius: 50%;
  background-color: ${(p) => p.theme.detailBlockBackground};
  position: absolute;
  display: flex;
  align-self: center;
  cursor: pointer;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-top: 20px;

  button {
    width: 100%;
  }
`;

const NotificationContainer = styled.div`
  width: 100%;
  margin: 20px 0 0;
`;

const PriceInfo = styled.span`
  text-align: center;
  margin-top: 10px;
  font-size: 12px;
  color: ${(p) => p.theme.labelGrey};
`;

export default function SwapBox() {
  const [activeTokenFrom, setActiveTokenFrom] =
    useState<IUserToken>(emptyTokenFrom);
  const [activeTokenTo, setActiveTokenTo] = useState<IUserToken>(emptyTokenTo);
  const [fromTokenValue, setFromTokenValue] = useState<string>("0.00");
  const [toTokenValue, setToTokenValue] = useState<string>("0.00");
  const [pool, setPool] = useState<string>("BTCUSD");
  const [error, setError] = useState<boolean>(false);

  const { mutateAsync: swapTokens } = useSwapTokens();
  const { tokens, isLoadingTokens } = useSwapTokenData();
  const { currentUser } = useAuthContext();

  const { slippage, minExpectedAmount } = useTokenPriceConversion({
    fromToken: activeTokenFrom,
    toToken: activeTokenTo,
    amount: fromTokenValue,
    pool,
  });
  const { disabledFromTokens, disabledToTokens } = useDisabledTokens({
    tokenFrom: activeTokenFrom,
    tokenTo: activeTokenTo,
    availableTokens: tokens,
  });
  const {
    marketPrice,
    marketSlippage,
    markusOffer,
    isLoading: isFetchingPrice,
  } = useSwapMarketMaker({
    slippage,
    minExpectedAmount,
    fromToken: activeTokenFrom,
    toToken: activeTokenTo,
    fromTokenValue: fromTokenValue,
  });

  const handleSetActiveTokenFrom = useCallback(
    (token: IUserToken) => {
      setActiveTokenFrom(token);
    },
    [activeTokenFrom]
  );

  const handleSetActiveTokenTo = useCallback(
    (token: IUserToken) => {
      setActiveTokenTo(token);
    },
    [activeTokenTo]
  );

  const handleFormSubmit = useCallback(({ type, value }) => {
    if (error) setError(false);
    if (type === "to") setToTokenValue(value);
    return setFromTokenValue(value);
  }, []);

  const handleTokenSwap = () => {
    if (!tokens || !tokens.length) return;
    setActiveTokenTo(activeTokenFrom);
    setActiveTokenFrom(activeTokenTo);
  };

  const handleSwapSubmit = async () => {
    const fromValue = formatRoundedDownTokenPrecision({
      value: Number(fromTokenValue),
      precision: activeTokenFrom.precision,
    });

    if (
      !activeTokenFrom ||
      !activeTokenFrom.symbol ||
      !fromValue ||
      !marketPrice
    ) {
      return setError(true);
    }

    let response;

    const payload: ISwapTokens = {
      fromTokenName: activeTokenFrom.symbol,
      toTokenName: activeTokenTo.symbol,
      fromTokenAmount: fromValue,
      toTokenAmount: formatRoundedDownTokenPrecision({
        value: Number(0.0),
        precision: activeTokenTo.precision,
      }),
      pool,
      markusOffer,
    };

    response = await swapTokens(payload);

    if (response && response.success) {
      setFromTokenValue("0.00");
      setToTokenValue("0.00");
      return;
    }
  };

  const renderDetailsBox = () => {
    if (!fromTokenValue || Number(fromTokenValue) == 0) return <></>;
    return (
      <SwapTokenDetailsBox
        slippage={marketSlippage}
        isFetchingPrice={isFetchingPrice}
      />
    );
  };

  const renderError = () => {
    if (!error) return <></>;
    return (
      <NotificationContainer>
        <Notification
          type="error"
          text="There was an error. Please try again."
        />
      </NotificationContainer>
    );
  };

  useEffect(() => {
    if (!tokens) return;
    const fromToken = tokens.find((t) => {
      if (process.env.NEXT_PUBLIC_ENV === "development") {
        return t.symbol === "BTCL";
      }
      return t.symbol === "PBTC";
    });
    if (fromToken) setActiveTokenFrom(fromToken);
    const toToken = tokens.find((t) => {
      if (process.env.NEXT_PUBLIC_ENV === "development") {
        return t.symbol === "USDL";
      }
      return t.symbol === "PUSDT";
    });
    if (toToken) setActiveTokenTo(toToken);
  }, [tokens]);

  useEffect(() => {
    if (marketPrice === toTokenValue) return;
    setToTokenValue(marketPrice);
  }, [marketPrice, toTokenValue]);

  useEffect(() => {
    if (!tokens || !tokens.length) return;
    if (activeTokenFrom.symbol === "LIBRE" || activeTokenTo.symbol === "LIBRE")
      setPool("BTCLIB");
    else setPool("BTCUSD");
  }, [activeTokenFrom, activeTokenTo]);

  return (
    <Container>
      <Content>
        <Title>Swap</Title>
        <Description>
          Trade tokens in an instant.&nbsp;View{" "}
          <a href="http://info.libre.org/" target="_blank">
            analytics
          </a>{" "}
          here.
        </Description>
        <Box>
          <Row>
            <RowLine>
              <RowHeader>From</RowHeader>
              <RowHeader>
                {`Balance:`}
                <HeadRight>
                  {isLoadingTokens ? (
                    <LoadingAnimation size="tiny" />
                  ) : (
                    activeTokenFrom.unstaked.toLocaleString("en-us", {
                      minimumFractionDigits: activeTokenFrom.precision,
                    })
                  )}
                </HeadRight>
              </RowHeader>
            </RowLine>
            <RowLine>
              <SwapAmountInput
                type="from"
                enabled
                showMax
                token={activeTokenFrom}
                handleSubmit={handleFormSubmit}
                initialValue={fromTokenValue}
              />
              <CurrencySelector
                tokens={tokens}
                onChange={handleSetActiveTokenFrom}
                activeToken={activeTokenFrom}
              />
            </RowLine>
          </Row>
          <MiddleContainer>
            <MiddleAction onClick={handleTokenSwap}>
              <Image src="/icons/swap-icon.svg" width={24} height={24} />
            </MiddleAction>
          </MiddleContainer>
          <Row>
            <RowLine>
              <RowHeader>To (estimated)</RowHeader>
              <RowHeader>
                {`Balance:`}
                <HeadRight>
                  {isLoadingTokens ? (
                    <LoadingAnimation size="tiny" />
                  ) : (
                    activeTokenTo.unstaked.toLocaleString("en-us", {
                      minimumFractionDigits: activeTokenTo.precision,
                    })
                  )}
                </HeadRight>
              </RowHeader>
            </RowLine>
            <RowLine>
              <SwapAmountInput
                type="to"
                token={activeTokenTo}
                handleSubmit={handleFormSubmit}
                initialValue={toTokenValue}
              />
              <CurrencySelector
                tokens={tokens}
                onChange={handleSetActiveTokenTo}
                activeToken={activeTokenTo}
                disabledTokens={[...disabledToTokens, activeTokenFrom.symbol]}
              />
            </RowLine>
          </Row>
        </Box>
        <SwapPrice
          fromTokenValue={fromTokenValue}
          activeTokenFrom={activeTokenFrom}
          activeTokenTo={activeTokenTo}
          isFetchingPrice={isFetchingPrice}
        />
        {renderError()}
        <ButtonContainer>
          <SwapButton
            disabledFromTokens={disabledFromTokens}
            disabledToTokens={disabledToTokens}
            activeTokenFrom={activeTokenFrom}
            activeTokenTo={activeTokenTo}
            fromTokenValue={fromTokenValue}
            handleSubmit={handleSwapSubmit}
          />
          {(!currentUser || !currentUser.actor) && (
            <PriceInfo>
              Connect wallet to get better price offers for larger order
            </PriceInfo>
          )}
        </ButtonContainer>
      </Content>
      {renderDetailsBox()}
    </Container>
  );
}
