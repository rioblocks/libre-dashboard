import React, { SyntheticEvent, useEffect, useState } from "react";
import styled from "styled-components";
import { IUserToken } from "../../models/Tokens";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
  width: 100%;
`;

const Input = styled.input`
  border: none;
  font-family: Poppins;
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
  width: 100%;
  background: none;

  &:focus {
    outline: none;
  }
`;

const MaxBtn = styled.span`
  display: inline-block;
  align-self: flex-end;
  margin-right: 20px;
  font-size: 12px;
  color: ${(p) => p.theme.detailBlockTitle};
  text-transform: uppercase;
  cursor: pointer;
`;

interface ISwapAmountInput {
  token: IUserToken;
  handleSubmit: ({ type, value }: { type: string; value: string }) => void;
  type: "to" | "from";
  enabled?: boolean;
  showMax?: boolean;
  initialValue?: string;
  maxValue?: number;
}

export const SwapAmountInput = ({
  token,
  handleSubmit,
  type,
  enabled,
  showMax,
  initialValue,
  maxValue,
}: ISwapAmountInput) => {
  const [value, setValue] = useState<string>("0.00");

  const handleClick = (e: SyntheticEvent<HTMLInputElement>) =>
    e.currentTarget.select();

  const handleBlur = () => {
    if (!value) setValue("0.00");
  };

  const handleMaxClick = () => {
    if (!token.unstaked || Number(token.unstaked) <= 0) return;
    if (maxValue) {
      setValue(String(maxValue));
      return handleSubmit({ type, value: String(maxValue) });
    }
    setValue(String(token.unstaked));
    handleSubmit({ type, value: String(token.unstaked) });
  };

  const handleOnChange = (e: React.FormEvent<HTMLInputElement>) => {
    const value = e.currentTarget.value;
    if (
      token.precision &&
      value.split(".")[1] &&
      value.split(".")[1].length > token.precision
    )
      return String(Number(value).toFixed(token.precision));
    setValue(value);
    handleSubmit({ type, value });
  };

  useEffect(() => {
    // IF TOKEN SWAPS POSITION, CLEAR VALUE
    setValue("0.00");
    handleSubmit({ type, value: "0.00" });
  }, [token]);

  useEffect(() => {
    // CHANGE INITIAL VALUE BASED ON CONVERSION OR FORCED UPDATE
    if (!initialValue) return; // REQUIRED TO KEEP COMPONENT CONTROLLED
    if (initialValue === value) return;
    setValue(initialValue);
  }, [initialValue]);

  return (
    <Wrapper>
      <Input
        type={"number"}
        onChange={handleOnChange}
        onClick={handleClick}
        onBlur={handleBlur}
        value={value}
        disabled={!enabled}
      />
      {showMax && <MaxBtn onClick={handleMaxClick}>Max</MaxBtn>}
    </Wrapper>
  );
};
