import styled, { css } from "styled-components";
import { useExchangeRates } from "../../hooks/useExchangeRates";
import { IUserToken } from "../../models/Tokens";
import { LoadingAnimation } from "../../uikit/LoadingAnimation";

const PriceContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 22px 0 0;
`;

const PriceRight = styled.div<{ isFetching?: boolean }>`
  display: flex;
  align-items: center;
  ${(p) =>
    p.isFetching &&
    css`
      opacity: 0.7;
    `}
`;

const PriceText = styled.span`
  display: flex;
  align-items: center;
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: #9695a0;
`;

const PriceAmount = styled.span`
  display: inline-block;
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: #9695a0;
`;

const Loader = styled.div`
  margin-right: 5px;
`;

interface ISwapPrice {
  fromTokenValue: string;
  activeTokenFrom: IUserToken;
  activeTokenTo: IUserToken;
  isFetchingPrice: boolean;
}

export const SwapPrice = ({
  fromTokenValue,
  activeTokenFrom,
  activeTokenTo,
  isFetchingPrice,
}: ISwapPrice) => {
  const { data: exchangeRates } = useExchangeRates();
  if (!fromTokenValue || Number(fromTokenValue) == 0) return <></>;
  if (activeTokenFrom.symbol === "LIBRE" || activeTokenTo.symbol === "LIBRE") {
    return (
      <PriceContainer>
        <PriceText>Best Price</PriceText>
        <PriceRight isFetching={isFetchingPrice}>
          {isFetchingPrice && (
            <Loader>
              <LoadingAnimation size="tiny" />
            </Loader>
          )}
          <PriceAmount>{`1 ${activeTokenFrom.symbol} = ${Number(
            exchangeRates?.BTCSAT ? exchangeRates?.BTCSAT * 100000000 : 0
          ).toFixed(0)} SATS`}</PriceAmount>
        </PriceRight>
      </PriceContainer>
    );
  }
  return (
    <PriceContainer>
      <PriceText>
        {isFetchingPrice ? "Fetching Best Price" : "Best Price"}
      </PriceText>
      <PriceRight isFetching={isFetchingPrice}>
        {isFetchingPrice && (
          <Loader>
            <LoadingAnimation size="tiny" />
          </Loader>
        )}
        <PriceAmount>{`1 BTC = $${
          exchangeRates
            ? Number(exchangeRates?.PBTC ?? exchangeRates?.BTCL).toLocaleString(
                "en-US"
              )
            : "--"
        }`}</PriceAmount>
      </PriceRight>
    </PriceContainer>
  );
};
