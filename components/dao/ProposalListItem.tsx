import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import styled, { css, useTheme } from "styled-components";
import { IProposal } from "../../models/Proposals";
import { Label } from "../../uikit/Label";
import { ProgressBar } from "../../uikit/ProgressBar";
import {
  formatMoneyString,
  getProposalStatus,
  getStatusLabelColor,
  renderVotesPercentage,
} from "../../utils";
import { MediaQueryWidths } from "../../utils/constants";

const ListItem = styled.div<{ lastItem?: boolean }>`
  display: flex;
  justify-content: space-between;
  padding: 20px 0;
  border-bottom: 1px solid ${(p) => p.theme.detailBlockBorder};

  @media (max-width: ${MediaQueryWidths.small}px) {
    flex-direction: column;
  }

  ${(p) =>
    p.lastItem &&
    css`
      border-bottom: none;
    `}
`;

const ItemCol = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;

  &:first-of-type {
    cursor: pointer;
    flex: 1;
  }
`;

const LastCol = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;

  &:first-of-type {
    cursor: pointer;
    flex: 1;
  }

  @media (max-width: ${MediaQueryWidths.small}px) {
    display: none;
  }
`;

const ItemRow = styled.div`
  display: flex;
  align-items: center;
  padding: 4px 0;
`;

const ItemTitle = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.24;
  color: ${(p) => p.theme.titleText};
`;

const RowText = styled.span`
  display: inline-block;
  margin-left: 10px;
  font-size: 13px;
  line-height: 1.71;
  color: ${(p) => p.theme.detailBlockTitle};
`;

const ProgressBarContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const Bars = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  width: 150px;
  justify-content: space-around;
`;

const BarCol = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const VotesCount = styled.span`
  display: flex;
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  justify-content: flex-end;
  color: ${(p) => p.theme.greyText};
  margin: 0 0 0 16px;
  min-width: 100px;
`;

const ConcludedRow = styled.div`
  display: flex;
`;

const ConcludedText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.greyText};
  display: inline-block;
  margin-left: 10px;
`;
interface IProposalListItem {
  proposal: IProposal;
  lastItem?: boolean;
}

export const ProposalListItem = ({ proposal, lastItem }: IProposalListItem) => {
  const theme = useTheme();

  const renderLabel = () => {
    const { backgroundColor, textColor } = getStatusLabelColor(
      proposal.status,
      theme
    );
    return (
      <Label color={textColor} backgroundColor={backgroundColor}>
        {getProposalStatus(proposal.status)}
      </Label>
    );
  };

  const renderStatus = () => {
    if (proposal.status === 3) {
      return (
        <ConcludedRow>
          <Image src="/icons/passed.svg" width={24} height={24} />
          <ConcludedText>Passed</ConcludedText>
        </ConcludedRow>
      );
    }

    if (proposal.status === 4) {
      return (
        <ConcludedRow>
          <Image src="/icons/failed.svg" width={24} height={24} />
          <ConcludedText>Failed</ConcludedText>
        </ConcludedRow>
      );
    }

    if (proposal.status === 2) {
      return (
        <ProgressBarContainer>
          <Bars>
            <ProgressBar
              percentage={renderVotesPercentage({ isFor: true, proposal })}
              color={theme.progressBarGreen}
            />
            <ProgressBar
              percentage={renderVotesPercentage({ isFor: false, proposal })}
              color={theme.progressBarRed}
            />
          </Bars>
          <BarCol>
            <VotesCount>{formatMoneyString(proposal.votes_for)}</VotesCount>
            <VotesCount>{formatMoneyString(proposal.votes_against)}</VotesCount>
          </BarCol>
        </ProgressBarContainer>
      );
    }

    // TODO: CONFIRM WHAT IS THE PENDING ACTION TO EXECUTE
    // return (
    //   <MediumButton
    //     text="Execute now"
    //     color={theme.buttonOrangeText}
    //     backgroundColor={theme.buttonOrange}
    //     onClick={() => {}}
    //   />
    // );
  };

  const renderTime = () => {
    if (moment(proposal.expires_on).isAfter(moment())) {
      return `Voting ends ${moment(proposal.expires_on).fromNow()}`;
    }
    return `Ended ${moment(proposal.expires_on).format("MMMM DD, YYYY")}`;
  };

  return (
    <ListItem lastItem={lastItem}>
      <Link href={`/dao/${proposal.name}`}>
        <ItemCol>
          <ItemRow>
            <ItemTitle>{proposal.title}</ItemTitle>
          </ItemRow>
          <ItemRow>
            {renderLabel()}
            <RowText>{renderTime()}</RowText>
          </ItemRow>
        </ItemCol>
      </Link>
      <LastCol>{renderStatus()}</LastCol>
    </ListItem>
  );
};
