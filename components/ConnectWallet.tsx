import Image from "next/image";
import { useContext } from "react";
import styled, { useTheme } from "styled-components";
import useComponentVisible from "../hooks/useComponentVisible";
import { useAuthContext } from "../providers/AuthProvider";
import { ConnectWalletContext } from "../providers/ConnectWalletProvider";
import { MediumButton } from "../uikit/Button";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  min-width: 100px;
  height: 40px;
  border-radius: 20px;
  background-color: #1a1b23;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
  position: relative;
  color: white;
  padding: 0 10px 0 15px;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none !important;
  }
`;

const Text = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  text-align: center;
  color: white;
  margin-right: 10px;
`;

const Dropdown = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  position: absolute;
  top: 40px;
  right: 0px;
  width: 160px;
  height: 50px;
  background: #1a1b23;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  z-index: 9999;

  li {
    margin: 0;
    padding: 0;
    font-size: 14px;
  }
`;

export const ConnectWallet = () => {
  const theme = useTheme();

  const { currentUser, logout } = useAuthContext();
  const { handleOnOpen } = useContext(ConnectWalletContext);
  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  const handleDropdown = () => setIsComponentVisible(!isComponentVisible);

  const renderDropdown = () => {
    if (!isComponentVisible) return <></>;
    return (
      <Dropdown>
        <li onClick={logout}>Logout</li>
      </Dropdown>
    );
  };

  if (currentUser && currentUser.actor && currentUser.permission === "active") {
    return (
      <Wrapper ref={ref} onClick={handleDropdown}>
        <Text>{currentUser.actor}</Text>
        <Image src="/icons/avatar-chevron-down.svg" width={16} height={16} />
        {renderDropdown()}
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      <MediumButton
        onClick={handleOnOpen}
        text="Connect Wallet"
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
      />
    </Wrapper>
  );
};
