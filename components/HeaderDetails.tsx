import Image from "next/image";
import { useMemo } from "react";
import styled from "styled-components";
import useTokens from "../hooks/queries/tokens/useTokens";
import { useExchangeRates } from "../hooks/useExchangeRates";
import { IconPairs } from "../uikit/IconPairs";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  background: #ffffff;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  padding: 5px 10px 5px 8px;
  display: flex;
  align-items: center;
  border-radius: 5px;
  align-self: flex-end;

  &:not(:last-of-type) {
    margin-bottom: 5px;

    @media (max-width: ${MediaQueryWidths.small}px) {
      margin-bottom: 2px;
    }
  }

  @media (max-width: ${MediaQueryWidths.small}px) {
    padding: 3px 2px;
  }
`;

const Label = styled.span`
  display: flex;
  align-items: center;
  font-size: 15px;
  text-transform: uppercase;
  margin-right: 5px;
  line-height: 1;

  @media (max-width: ${MediaQueryWidths.small}px) {
    font-size: 12px;
  }

  span {
    @media (max-width: ${MediaQueryWidths.small}px) {
      display: none !important;
    }
  }
  div {
    @media (max-width: ${MediaQueryWidths.small}px) {
      display: none !important;
    }
  }
`;

const Amount = styled.span`
  font-size: 15px;
  line-height: 1;

  @media (max-width: ${MediaQueryWidths.small}px) {
    font-size: 12px;
  }
`;

const Break = styled.div`
  width: 3px;
`;

export const HeaderDetails = () => {
  const { data: exchangeRates } = useExchangeRates();
  const { data: tokenData } = useTokens();

  const marketCap = useMemo(() => {
    if (!tokenData) return "--";
    const data = tokenData.reduce((a, b) => a + b.marketCap, 0);
    return Number(data.toFixed(2)).toLocaleString("en-US");
  }, [tokenData]);

  return (
    <Wrapper>
      <Item>
        <Label>
          <Image src="/icons/btc-asset-icon.svg" height={14} width={14} />
          <Break />
          BTC:
        </Label>
        <Amount>
          $
          {exchangeRates
            ? Number(exchangeRates?.BTCL ?? exchangeRates?.PBTC).toLocaleString(
                "en-US"
              )
            : "--"}
        </Amount>
      </Item>
      <Item>
        <Label>
          <Image src="/icons/libre-asset-icon.svg" height={14} width={14} />
          <Break />
          LIBRE:
        </Label>
        <Amount>
          $
          {exchangeRates
            ? `${exchangeRates?.LIBRE.toFixed(6)} (${Number(
                exchangeRates.BTCSAT * 100000000
              ).toFixed(2)} SATS)`
            : "--"}
        </Amount>
      </Item>
      <Item>
        <Label>
          <IconPairs
            imgSrc1="/icons/libre-asset-icon.svg"
            imgSrc2="/icons/btc-asset-icon.svg"
            imgSrc3="/icons/usdt-asset-icon.png"
            iconSize={14}
          />
          <Break />
          TVL:
        </Label>
        <Amount>${marketCap ?? "--"}</Amount>
      </Item>
    </Wrapper>
  );
};
