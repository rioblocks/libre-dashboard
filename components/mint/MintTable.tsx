import moment from "moment";
import {
  AutoSizer,
  Column,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled from "styled-components";
import useUserMints from "../../hooks/queries/mints/useUserMints";
import useMintStats from "../../hooks/queries/stats/useMintStats";
import { useAuthContext } from "../../providers/AuthProvider";
import {
  headerRenderer,
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../uikit/Table";
import { MediaQueryWidths } from "../../utils/constants";

const Wrapper = styled.div`
  padding: 24px;
  min-height: 500px;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  margin: 10px 0 25px;
  display: inline-block;
`;

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

const Date = styled.span`
  display: flex;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none;
  }
`;

const DateMobile = styled.span`
  display: none;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: inline-block;
  }
`;

const MintTable = ({}) => {
  const { currentUser } = useAuthContext();
  let { data, isLoading } = useUserMints(currentUser.actor);
  const { resolveRowStyle, tableHeight } = useGetTableInfo({
    data: data ?? [],
    isLoading,
    rows: 1,
  });
  const { data: mintStats } = useMintStats();

  const isRowLoaded = ({ index }: { index: number }) => !!data[index];

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "contribution_date") {
      return (
        <CellItem>
          <Date>{moment(cellData).utc().local().format("MMM Do, YYYY")}</Date>
          <DateMobile>
            {moment(cellData).utc().local().format("MM/DD/YY")}
          </DateMobile>
        </CellItem>
      );
    }
    if (dataKey === "btc_contributed") {
      return <CellItem>{cellData}</CellItem>;
    }
    if (dataKey === "staked_days") {
      return <CellItem>{cellData} Days</CellItem>;
    }
    if (dataKey === "mint_apy") {
      return <CellItem>{Number(cellData).toFixed(2)}%</CellItem>;
    }
    if (dataKey === "mint_bonus") {
      return <CellItem>{Number(cellData).toFixed(2)}x</CellItem>;
    }
    if (dataKey === "libre_minted") {
      return <CellItem>TBD</CellItem>;
    }
    if (dataKey === "%_of_pbtc") {
      if (!data || !mintStats) return <></>;
      return (
        <CellItem>
          {(
            (Number(
              rowData.btc_contributed.split(
                ` ${
                  process.env.NEXT_PUBLIC_ENV === "development"
                    ? "BTCL"
                    : "PBTC"
                }`
              )[0]
            ) /
              Number(mintStats?.btc_contributed)) *
            100
          ).toFixed(4)}
          %
        </CellItem>
      );
    }
    return <CellItem>{cellData}</CellItem>;
  };

  if (!data || !data.length) return <></>;

  return (
    <Wrapper>
      <Title>My Contributions</Title>
      <TableWrapper height={tableHeight}>
        <AutoSizer>
          {({ width, height }) => (
            <InfiniteLoader
              isRowLoaded={isRowLoaded}
              loadMoreRows={loadMoreRows}
              rowCount={1}
            >
              {({ onRowsRendered, registerChild }) => (
                <Table
                  ref={registerChild}
                  width={width < 900 ? 900 : width}
                  height={tableHeight}
                  headerHeight={HEADER_HEIGHT}
                  rowHeight={ROW_HEIGHT}
                  rowCount={data.length}
                  rowGetter={({ index }) => data[index]}
                  rowStyle={({ index }) =>
                    resolveRowStyle(index + 1 === data.length)
                  }
                  onRowsRendered={onRowsRendered}
                  overscanRowCount={5}
                >
                  <Column
                    label="Contribution Date"
                    dataKey={"contribution_date"}
                    flexGrow={2}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="PBTC Contributed"
                    dataKey={"btc_contributed"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="Lock Up Period"
                    dataKey={"staked_days"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="% of Total PBTC"
                    dataKey={"%_of_pbtc"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="Mint APY"
                    dataKey={"mint_apy"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="LIBRE Minted"
                    dataKey={"libre_minted"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                </Table>
              )}
            </InfiniteLoader>
          )}
        </AutoSizer>
      </TableWrapper>
    </Wrapper>
  );
};

export default MintTable;
