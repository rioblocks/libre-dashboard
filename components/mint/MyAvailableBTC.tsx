import Image from "next/image";
import { useMemo } from "react";
import useUserTokens from "../../hooks/queries/tokens/useUserTokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { DetailsBlock } from "../../uikit/DetailsBlock";

export const MyAvailableBTC = () => {
  const { currentUser } = useAuthContext();
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });

  const btcTokenAmount = useMemo(() => {
    if (!userTokens) return "0";
    const token = userTokens.find(
      (t) => t.symbol === "BTCL" || t.symbol === "PBTC"
    );
    if (!token) return "0";
    return String(token.unstaked);
  }, [userTokens]);

  return (
    <DetailsBlock
      title="My Available PBTC"
      details={`${btcTokenAmount} PBTC`}
      image={<Image src="/logos/bitcoin-circle.svg" width={32} height={32} />}
    />
  );
};
