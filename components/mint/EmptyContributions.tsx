import { useContext } from "react";
import Lottie from "react-lottie";
import styled, { useTheme } from "styled-components";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import connectAnimation from "../../uikit/animations/connect-animation.json";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  border-radius: 16px;
  background: #ffffff;
  height: 450px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ImageContainer = styled.div`
  margin-bottom: 24px;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  text-align: center;
  color: #04021d;
`;

const Description = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  text-align: center;
  color: ${(p) => p.theme.descriptionText};
  max-width: 294px;
  margin: 20px 0 30px;
`;

const BtnContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 144px;

  button {
    width: 100%;
  }
`;

export const EmptyContributions = () => {
  const theme = useTheme();
  const { handleOnOpen: handleLogin } = useContext(ConnectWalletContext);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: connectAnimation,
  };

  return (
    <Wrapper>
      <Content>
        <ImageContainer>
          <Lottie options={defaultOptions} width={180} height={180} />
        </ImageContainer>
        <Title>You have no contributions</Title>
        <Description>
          Looks like you have not contributed to the Mint Rush yet.
        </Description>
        <BtnContainer>
          {/* <MediumButton
            text="Contribute Now"
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            onClick={handleLogin}
          /> */}
        </BtnContainer>
      </Content>
    </Wrapper>
  );
};
