import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { useAuthContext } from "../providers/AuthProvider";
import { WindowSizeContext } from "../providers/WindowSizeProvider";
import { LoadingAppBar } from "../uikit/LoadingAppBar";
import { MediaQueryWidths } from "../utils/constants";
import { ConnectWallet } from "./ConnectWallet";

const Header = styled.header<{ mobile?: boolean }>`
  display: flex;
  align-items: center;
  flex: 1;
  height: 72px;
  padding: ${(p) => (p.mobile ? "0 24px" : "0 40px")};
  background-color: ${(p) => p.theme.headerBackground};
  position: relative;
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 72px;
  width: 100%;
`;

const Logo = styled.div`
  display: flex;
  align-items: center;
`;

const NavList = styled.div`
  display: flex;
  align-items: center;
  height: 72px;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none;
  }
`;

const NavListItem = styled.div<{ active?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${(p) => p.theme.headerTextColor};
  font-size: 14px;
  font-weight: 500;
  height: 72px;
  cursor: pointer;
  text-align: center;
  position: relative;
  margin: 0 15px;

  &:hover {
    > div {
      visibility: visible;
    }
  }

  &:hover ul {
    visibility: visible;
  }

  ul {
    position: absolute;
    display: table;
    top: 55px;
    bottom: 0;
    left: 14px;
    height: auto;
    background-color: #fff;
    visibility: hidden;
    border: 1px solid ${(p) => p.theme.detailBlockBorder};
    border-radius: 5px;
    list-style: none;
    margin: 0;
    padding: 10px 0;
    -webkit-box-shadow: 0 4px 8px 0 rgba(104, 103, 119, 0.16),
      0 0 2px 0 rgba(49, 48, 70, 0.04);
    box-shadow: 0 4px 8px 0 rgba(104, 103, 119, 0.16),
      0 0 2px 0 rgba(49, 48, 70, 0.04);
  }

  li {
    position: relative;
    color: ${(p) => p.theme.titleText};
    margin: 0;
    padding: 5px 0;
    text-align: left;
    padding: 5px 12px;
    vertical-align: top;

    &:hover {
      background-color: #e8e7f7;
    }

    a {
      display: flex;
      font-size: 14px;
      font-weight: 500;
      line-height: 1.71;
      color: ${(p) => p.theme.titleText};
      width: 100%;
    }
  }

  ${(p) =>
    p.active &&
    css`
      > div {
        visibility: visible;
      }
    `}
`;

const NavBorder = styled.div`
  height: 2px;
  background: ${(p) => p.theme.headerTextHover};
  width: 100%;
  position: absolute;
  bottom: 0px;
  visibility: hidden;
  z-index: 0;
`;

const MenuIcon = styled.div`
  display: none;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: flex !important;
  }
`;

interface INavBar {
  handleMobileMenuToggle: () => void;
}

export const NavBar = ({ handleMobileMenuToggle }: INavBar) => {
  const { pathname } = useRouter();
  const { isTablet, isMobile, isSmallMobile } = useContext(WindowSizeContext);
  const { isLoadingUser } = useAuthContext();

  const [showMobile, setShowMobile] = useState<boolean>(false);

  // NEEDED TO MAKE SSR NEXT.JS HAPPY
  useEffect(() => {
    if (isTablet || isMobile || isSmallMobile) {
      if (showMobile) return;
      return setShowMobile(true);
    }
    if (showMobile) setShowMobile(false);
  }, [isTablet, isMobile, isSmallMobile]);

  const renderLoadingBar = () => {
    if (isLoadingUser) return <LoadingAppBar />;
    return <></>;
  };

  return (
    <Header>
      <Container>
        <Link href={"/"}>
          <Logo style={{ cursor: "pointer" }}>
            <Image src="/logos/logo-full.svg" width={100} height={22} />
          </Logo>
        </Link>
        <NavList>
          <Link href="/">
            <NavListItem active={pathname == "/"}>
              <span>Dashboard</span>
              <NavBorder />
            </NavListItem>
          </Link>
          <Link href="/wallet">
            <NavListItem active={pathname == "/wallet"}>
              <span>Wallet</span>
              <NavBorder />
            </NavListItem>
          </Link>
          <Link href="/swap">
            <NavListItem active={pathname == "/swap"}>
              <span>Swap</span>
              <NavBorder />
            </NavListItem>
          </Link>
          <Link href="/farming">
            <NavListItem active={pathname == "/farming"}>
              <span>Farming</span>
              <NavBorder />
            </NavListItem>
          </Link>
          <Link href="/validators">
            <NavListItem active={pathname == "/validators"}>
              <span>Validators</span>
              <NavBorder />
            </NavListItem>
          </Link>
          <Link href="/dao">
            <NavListItem active={pathname == "/dao"}>
              <span>DAO</span>
              <NavBorder />
            </NavListItem>
          </Link>

          <Link href="/audit">
            <NavListItem active={pathname == "/audit"}>
              <span>Audit</span>
              <NavBorder />
            </NavListItem>
          </Link>
          <Link href="https://www.libreblocks.io/" passHref>
            <a target="_blank" rel="noreferrer">
              <NavListItem>
                <span>Explorer</span>
                <NavBorder />
              </NavListItem>
            </a>
          </Link>
        </NavList>
        <ConnectWallet />
        <MenuIcon>
          <Image
            onClick={handleMobileMenuToggle}
            src={"/icons/menu-hamburger.svg"}
            width={24}
            height={24}
          />
        </MenuIcon>
      </Container>
      {renderLoadingBar()}
    </Header>
  );
};
