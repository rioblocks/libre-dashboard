import { useMemo } from "react";
import {
  AutoSizer,
  Column,
  Table,
  TableCellProps,
  TableHeaderProps,
} from "react-virtualized";
import styled from "styled-components";
import useAuditStats from "../../hooks/queries/audit/useAuditStats";
import {
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../uikit/Table";

const HeadItem = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;
  font-size: 12.8px;
  font-weight: 600;
  color: ${(p) => p.theme.tableHeader};
  position: relative;
  white-space: nowrap;
  text-transform: none;
`;

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

const LoadingContainer = styled.div`
  margin: 20px;
`;

const ProofOfAssetsTable = () => {
  const { data: auditStats, isLoading } = useAuditStats();

  const data = useMemo(() => {
    if (!auditStats) return [];
    return Object.keys(auditStats).map((s) => auditStats[s]);
  }, [auditStats]);

  const { tableHeight, resolveRowStyle, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 1,
  });

  const headerRenderer = ({ label, dataKey }: TableHeaderProps) => {
    if (dataKey === "max_supply") {
      return (
        <HeadItem style={{ display: "flex", justifyContent: "end" }}>
          {label}
        </HeadItem>
      );
    }
    return <HeadItem>{label}</HeadItem>;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "supply") {
      return (
        <CellItem
          style={{ cursor: "pointer" }}
          onClick={() =>
            window.open(
              "https://lb.libre.org/v1/chain/get_currency_stats?code=btc.ptokens&symbol=pbtc",
              "_blank"
            )
          }
        >
          {rowData.supply}
        </CellItem>
      );
    }
    if (dataKey === "max_supply") {
      return (
        <CellItem
          style={{ cursor: "pointer" }}
          onClick={() =>
            window.open(
              "https://libre-chain.gitlab.io/pbtc-audit/index.json",
              "_blank"
            )
          }
        >
          {rowData.supply}
        </CellItem>
      );
    }
    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <TableWrapper height={tableHeight < 100 ? 100 : tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <Table
            width={width < 350 ? 350 : width}
            height={height < 100 ? 100 : height}
            headerHeight={HEADER_HEIGHT}
            rowHeight={ROW_HEIGHT}
            rowCount={data.length}
            rowGetter={({ index }) => data[index]}
            rowStyle={({ index }) => resolveRowStyle(index + 1 === data.length)}
            noRowsRenderer={noRowsRenderer}
          >
            <Column
              label="Total PBTC on Libre"
              dataKey={"supply"}
              flexGrow={1}
              flexShrink={1}
              width={100}
              minWidth={50}
              headerRenderer={headerRenderer}
              cellRenderer={cellRenderer}
            />
            <Column
              label="Total BTC held by PNetwork"
              dataKey={"max_supply"}
              flexGrow={1}
              flexShrink={1}
              width={150}
              minWidth={100}
              headerRenderer={headerRenderer}
              cellRenderer={cellRenderer}
              style={{ textAlign: "right" }}
            />
          </Table>
        )}
      </AutoSizer>
    </TableWrapper>
  );
};

export default ProofOfAssetsTable;
