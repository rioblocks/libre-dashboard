import { useState } from "react";
import styled from "styled-components";
import AppModal from "./AppModal";
import { BitcoinQRCodeContent } from "./wallet/mytokens/BitcoinQRCodeContent";
import { LightningQRCodeContent } from "./wallet/mytokens/LightningQRCodeContent";

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const TopNavigationContainer = styled.div`
  display: flex;
  align-items: center;
  height: 72px;
  margin: 0 -24px;
  justify-content: center;
`;

const TopNavigationItem = styled.div`
  color: black;
  width: 50%;
  text-align: center;
  position: relative;
  cursor: pointer;
  &:hover {
    > div {
      visibility: visible;
    }
  }
`;

const NavBorder = styled.div<{ active: boolean }>`
  height: 3px;
  background: ${(p) => p.theme.headerTextHover};
  width: 100%;
  position: absolute;
  bottom: -5px;
  visibility: ${(p) => (p.active ? "visible" : "hidden")};
  z-index: 0;
`;

const Notification = styled.div`
  background: #fcdfbf;
  border-radius: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem 1rem;
  margin-top: 1rem;
`;

const NotificationText = styled.span`
  color: #f9a339;
  text-align: center;
  font-size: 12px;
  font-weight: 500;
`;

interface IMintModal {
  open: boolean;
  handleOnClose: () => void;
}

export const AddBalanceModal = ({ open, handleOnClose }: IMintModal) => {
  const [btcAddressTabActive, setBtcAddressTabActive] = useState(true);

  if (!open) return <></>;

  const handleNavigationClick = (isBtc: boolean) => {
    setBtcAddressTabActive(isBtc);
  };

  return (
    <AppModal title="Add Bitcoin" show={true} onClose={handleOnClose}>
      <TopNavigationContainer>
        <TopNavigationItem onClick={() => handleNavigationClick(false)}>
          Lightning <NavBorder active={!btcAddressTabActive} />
        </TopNavigationItem>
        <TopNavigationItem onClick={() => handleNavigationClick(true)}>
          BTC Address <NavBorder active={btcAddressTabActive} />
        </TopNavigationItem>
      </TopNavigationContainer>
      <Content>
        {btcAddressTabActive ? (
          <>
            <BitcoinQRCodeContent />
            <Notification>
              <NotificationText>
              pNetwork cross-chain fees are subject to change, they are currently at a minimum of $10 or 0.1%. We recommend using Lightning Network for transactions of less than 200k SATS.
              </NotificationText>
            </Notification>
          </>
        ) : (
          <LightningQRCodeContent />
        )}
      </Content>
    </AppModal>
  );
};
