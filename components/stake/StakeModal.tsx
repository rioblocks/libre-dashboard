import { useMemo, useState } from "react";
import styled, { useTheme } from "styled-components";
import { useStakeLibre } from "../../hooks/queries/stakes/useStakeLibre";
import useUserTokens from "../../hooks/queries/tokens/useUserTokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";
import { InputSlider } from "../../uikit/InputSlider";
import { formatCurrencyWithPrecision } from "../../utils";
import AppModal from "../AppModal";

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const SliderContainer = styled.div`
  &:first-of-type {
    margin: 35px 0;
  }

  &:last-of-type {
    margin-bottom: 20px;
  }
`;

const BottomContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: -24px;
  margin-top: 24px;
  padding: 24px;
  background-color: #fafafa;
  border-top: 1px solid ${(p) => p.theme.detailBlockBorder};
  border-bottom-left-radius: 16px;
  border-bottom-right-radius: 16px;
`;

const BottomRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;

  &:last-of-type {
    margin-bottom: 40px;
  }
`;

const LeftText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.greyText};
`;

const RightText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.titleText};
`;

interface IStakeModal {
  open: boolean;
  handleOnClose: () => void;
}

const calculateAPYAndPayout = (stakeLength: number, stakeAmount: number) => {
  // stakeDate should be today
  var stakeDate = Math.floor(new Date().getTime() / 1000);

  // these are constants
  var alpha0 = 0.1; // minimum yield
  var beta0 = 2.0; // maximum yield
  var alphaT = 0.06; // terminal min yield
  var betaT = 0.3; // terminal max yield
  var T = 730.0; // days to terminal yield
  var L = Math.floor(new Date("2022-07-04").getTime() / 1000); //unix timestamp in seconds

  var daysDiff = stakeDate - L;

  var daysSince = Math.floor(daysDiff / (3600 * 24));

  var baseAPY =
    alpha0 +
    Math.sqrt(stakeLength / 365) * (beta0 - alpha0) +
    (alphaT +
      Math.sqrt(stakeLength / 365) * (betaT - alphaT) -
      (alpha0 + Math.sqrt(stakeLength / 365) * (beta0 - alpha0))) *
      Math.sqrt(Math.min(1, daysSince / T));
  var Apy = baseAPY;

  var payout = Apy * (stakeLength / 365) * stakeAmount + stakeAmount;
  return { apy: Apy, payout };
};

export const StakeModal = ({ open, handleOnClose }: IStakeModal) => {
  const theme = useTheme();
  const { currentUser } = useAuthContext();
  const { data: userTokens, isLoading: loadingUserTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data", "isLoading"],
  });
  const { mutateAsync } = useStakeLibre();

  const [amountValue, setAmountValue] = useState<string>("0");
  const [durationValue, setDurationValue] = useState<string>("1");
  const [valueError, setValueError] = useState<boolean>(false);
  const [submitLoading, setSubmitLoading] = useState(false);
  const [apy, setApy] = useState(0);
  const [payout, setPayout] = useState(0);

  const handleAmountChange = (value: number) => {
    if (valueError) setValueError(false);
    setAmountValue(String(value));
    const apyAndPayout = calculateAPYAndPayout(
      Number(durationValue),
      Number(value)
    );
    setApy(apyAndPayout.apy);
    setPayout(apyAndPayout.payout);
  };
  const handleDurationChange = (value: number) => {
    setDurationValue(String(value));
    const apyAndPayout = calculateAPYAndPayout(
      Number(value),
      Number(amountValue)
    );
    setApy(apyAndPayout.apy);
    setPayout(apyAndPayout.payout);
  };

  const handleSubmit = async () => {
    if (Number(amountValue) < 1) return setValueError(true);
    setSubmitLoading(true);
    await mutateAsync({
      quantity: String(Number(amountValue).toFixed(4)),
      duration: durationValue,
    });
    handleOnClose();
    setSubmitLoading(false);
  };

  const libreTokenBalance = useMemo(() => {
    if (!userTokens || !userTokens.length) return 0;
    const token = userTokens.find((t) => t.symbol === "LIBRE");
    if (token) return token.unstaked;
    return 0;
  }, [userTokens]);

  if (!open) return <></>;

  return (
    <AppModal title="New Stake" show={true} onClose={handleOnClose}>
      <Content>
        <SliderContainer>
          <InputSlider
            text="You stake (LIBRE):"
            valueSymbol="LIBRE"
            initialValue={0}
            minText="0"
            min={0}
            maxText="MAX"
            max={libreTokenBalance}
            handleUpdate={handleAmountChange}
            error={valueError}
          />
        </SliderContainer>
        <SliderContainer>
          <InputSlider
            text="Lock up for (Days):"
            valueSymbol="days"
            initialValue={1}
            minText="1 Day"
            min={1}
            maxText="MAX"
            max={365}
            handleUpdate={handleDurationChange}
          />
        </SliderContainer>
        <BottomContainer>
          <BottomRow>
            <LeftText>APY</LeftText>
            <RightText>{Number(apy * 100).toFixed(2)} %</RightText>
          </BottomRow>
          <BottomRow>
            <LeftText>Payout</LeftText>
            <RightText>
              {formatCurrencyWithPrecision(Number(payout), 4)} Libre
            </RightText>
          </BottomRow>
          <MediumButton
            onClick={handleSubmit}
            text="Stake Now"
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            isLoading={submitLoading}
            disabled={loadingUserTokens}
          />
        </BottomContainer>
      </Content>
    </AppModal>
  );
};
