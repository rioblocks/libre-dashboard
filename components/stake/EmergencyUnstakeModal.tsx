import Lottie from "react-lottie";
import styled, { useTheme } from "styled-components";
import { useUnstakedLibre } from "../../hooks/queries/stakes/useUnstakeLibre";
import warningAnimation from "../../uikit/animations/warning-animation.json";
import { MediumButton } from "../../uikit/Button";
import AppModal from "../AppModal";

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const Text = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  text-align: center;
  color: ${(p) => p.theme.greyText};
  display: block;
  max-width: 75%;
  margin-top: 16px;
`;

const BtnContainer = styled.div`
  margin-top: 40px;
  width: 100%;

  button {
    width: 100%;
  }
`;

const ImageContainer = styled.div`
  margin-bottom: 24px;
`;

interface IEmergencyUnstakeModal {
  open: boolean;
  handleOnClose: () => void;
  stakeIndex: number | null;
}

export const EmergencyUnstakeModal = ({
  open,
  handleOnClose,
  stakeIndex,
}: IEmergencyUnstakeModal) => {
  const theme = useTheme();
  const { mutateAsync } = useUnstakedLibre();

  const defaultOptions = {
    loop: false,
    autoplay: true,
    animationData: warningAnimation,
  };

  const handleSubmit = async () => {
    if (!stakeIndex) return;
    await mutateAsync(stakeIndex);
    handleOnClose();
  };

  return (
    <AppModal title="" show={open} onClose={handleOnClose}>
      <Content>
        <ImageContainer>
          <Lottie options={defaultOptions} width={120} height={120} />
        </ImageContainer>
        <Title>Emergency Unstake</Title>
        <Text>
          You can unstake anytime but you will lose all 20% of your principal
          and 100% of accrued interest.
        </Text>
        <BtnContainer>
          <MediumButton
            color={theme.buttonRedText}
            backgroundColor={theme.buttonRed}
            text="Confirm and Unstake Now"
            onClick={handleSubmit}
          />
        </BtnContainer>
      </Content>
    </AppModal>
  );
};
