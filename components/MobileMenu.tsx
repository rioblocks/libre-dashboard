import { useRouter } from "next/router";
import { useContext } from "react";
import styled, { useTheme } from "styled-components";
import { useAuthContext } from "../providers/AuthProvider";
import { ConnectWalletContext } from "../providers/ConnectWalletProvider";
import { MediumButton } from "../uikit/Button";

const Wrapper = styled.div`
  display: flex;
  background: ${(p) => p.theme.headerBackground};
  position: absolute;
  top: 72px;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 99999;
  overflow: auto;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 24px 24px;
  width: 100%;
  height: 100%;
`;

const List = styled.div`
  display: flex;
  flex-direction: column;
`;

const ListItem = styled.a<{ active?: boolean }>`
  font-size: 27.7px;
  font-weight: 600;
  line-height: 1.45;
  color: ${(p) =>
    p.active ? p.theme.headerTextColor : p.theme.headerInactiveColor};
  cursor: pointer;
  padding: 12px 0;

  &:hover {
    color: ${(p) => p.theme.headerTextColor};
  }
`;

const ListItemOrange = styled.a<{ active?: boolean }>`
  font-size: 27.7px;
  font-weight: 600;
  line-height: 1.45;
  background: ${(p) => p.theme.titleGradient};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  cursor: pointer;
  padding: 12px 0;
  margin-top: 30px;
`;

const BottomContainer = styled.div`
  width: 100%;
  margin-top: auto;

  button {
    width: 100%;
  }
`;

interface IMobileMenu {
  open: boolean;
  handleClose: () => void;
}

export const MobileMenu = ({ open, handleClose }: IMobileMenu) => {
  const router = useRouter();
  const theme = useTheme();
  const { currentUser, logout } = useAuthContext();
  const { handleOnOpen } = useContext(ConnectWalletContext);

  const handleClick = (route: string) => {
    router.push(route);
    handleClose();
  };

  const handleConnect = () => {
    handleOnOpen();
    handleClose();
  };

  const handleLogout = () => {
    logout();
    handleClose();
  };

  if (!open) return <></>;

  return (
    <Wrapper>
      <Container>
        <List>
          {(!currentUser ||
            !currentUser.actor ||
            currentUser.permission !== "active") && (
            <ListItemOrange onClick={handleConnect}>
              Connect Wallet
            </ListItemOrange>
          )}
          <ListItem
            onClick={() => handleClick("/")}
            active={router.pathname == "/"}
          >
            Dashboard
          </ListItem>
          <ListItem
            onClick={() => handleClick("/wallet")}
            active={router.pathname == "/wallet"}
          >
            Wallet
          </ListItem>
          <ListItem
            onClick={() => handleClick("/swap")}
            active={router.pathname == "/swap"}
          >
            Swap
          </ListItem>
          <ListItem
            onClick={() => handleClick("/farming")}
            active={router.pathname == "/farming"}
          >
            Farming
          </ListItem>
          <ListItem
            onClick={() => handleClick("/validators")}
            active={router.pathname == "/validators"}
          >
            Validators
          </ListItem>
          <ListItem
            onClick={() => handleClick("/dao")}
            active={router.pathname == "/dao"}
          >
            DAO
          </ListItem>

          <ListItem
            onClick={() => handleClick("/audit")}
            active={router.pathname == "/audit"}
          >
            Audit
          </ListItem>
          <ListItem
            onClick={() => window.open("https://www.libreblocks.io/", "_blank")}
          >
            Explorer
          </ListItem>
          {currentUser &&
            currentUser.actor &&
            currentUser.permission === "active" && (
              <BottomContainer>
                <MediumButton
                  onClick={handleLogout}
                  text="Logout"
                  backgroundColor={theme.buttonWhite}
                  color={theme.buttonWhiteText}
                />
              </BottomContainer>
            )}
        </List>
      </Container>
    </Wrapper>
  );
};
