import {
  AutoSizer,
  Column,
  Index,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import useApplicationStats from "../../../hooks/queries/stats/useApplicationStats";
import {
  headerRenderer,
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../../uikit/Table";

export const ChainActivityTable = () => {
  const { data, isLoading } = useApplicationStats();

  const tableData = data ? [data] : [];

  const { resolveRowStyle, tableHeight, noRowsRenderer } = useGetTableInfo({
    data: tableData,
    isLoading,
    rows: 1,
  });

  const isRowLoaded = ({ index }: Index) => !!tableData[index];
  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData }: TableCellProps) => {
    return Number(cellData).toLocaleString("en-US");
  };

  return (
    <TableWrapper height={tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={tableData.length}
          >
            {({ onRowsRendered, registerChild }) => (
              <Table
                autoHeight={true}
                ref={registerChild}
                width={width < 600 ? 600 : width}
                height={height}
                headerHeight={HEADER_HEIGHT}
                rowHeight={ROW_HEIGHT}
                rowCount={tableData.length}
                rowGetter={({ index }) => tableData[index]}
                rowStyle={({ index }) =>
                  resolveRowStyle(index + 1 === tableData.length)
                }
                overscanRowCount={5}
                onRowsRendered={onRowsRendered}
                noRowsRenderer={noRowsRenderer}
              >
                <Column
                  label="Total wallets"
                  dataKey={"accounts"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Daily Active"
                  dataKey={"active"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="New Accounts 24H"
                  dataKey={"newAccounts"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Total Referrals"
                  dataKey={"referrals"}
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
              </Table>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
    </TableWrapper>
  );
};
