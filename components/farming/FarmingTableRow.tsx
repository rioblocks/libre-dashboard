import Image from "next/image";
import React, { useContext, useState } from "react";
import styled, { css } from "styled-components";
import { IFarmingStats, IUserFarming } from "../../models/Farming";
import {
  displayTokenPrecisions,
  liquidityTokenPairs,
  tokenImgSources,
} from "../../models/Tokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import { IconPairs } from "../../uikit/IconPairs";
import { CellItem, ROW_HEIGHT } from "../../uikit/Table";
import { formatCurrencyWithPrecision } from "../../utils";
import { TokenWithUSDValue } from "../TokenWithUSDValue";
import { FarmingActionRow } from "./FarmingActionRow";
import { USDPriceText } from "./USDPriceText";

const Col = styled(CellItem)`
  display: flex;
  align-items: center;
  flex: 1;
  height: ${ROW_HEIGHT}px;
`;

const ActionCol = styled(Col)`
  justify-content: end;
`;

const Orange = styled(Col)`
  color: #fc8359;
`;

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const PairContainer = styled.div`
  display: flex;
  flex-shrink: 0;
  margin-right: 20px;
`;

const Caret = styled.div<{ active: boolean }>`
  cursor: pointer;
  ${(p) =>
    p.active &&
    css`
      transform: rotate(180deg);
      margin-top: -10px;
    `}
`;

interface IFarmingTableRow {
  stats: IFarmingStats;
  userData: IUserFarming;
  index: number;
  balance: number;
}

export const FarmingTableRow = ({
  stats,
  userData,
  balance,
  index,
}: IFarmingTableRow) => {
  const [open, setOpen] = useState<boolean>(false);

  const { currentUser } = useAuthContext();
  const { handleOnOpen } = useContext(ConnectWalletContext);

  const handleActionClick = () => {
    if (
      !currentUser ||
      !currentUser.actor ||
      currentUser.permission !== "active"
    )
      return handleOnOpen();
    setOpen(!open);
  };

  return (
    <React.Fragment key={stats.symbol}>
      <Col>
        <Container>
          <PairContainer>
            <IconPairs
              imgSrc1={
                tokenImgSources[process.env.NEXT_PUBLIC_ENV][
                  process.env.NEXT_PUBLIC_ENV !== "development"
                    ? "PBTC"
                    : "BTCL"
                ]
              }
              imgSrc2={
                tokenImgSources[process.env.NEXT_PUBLIC_ENV][
                  stats.symbol === "BTCUSD"
                    ? process.env.NEXT_PUBLIC_ENV === "development"
                      ? "USDL"
                      : "PUSDT"
                    : "LIBRE"
                ]
              }
            />
          </PairContainer>
          <CellItem>
            {liquidityTokenPairs[process.env.NEXT_PUBLIC_ENV][stats.symbol]}
          </CellItem>
        </Container>
      </Col>
      <Orange>{Number(stats.apy).toFixed(2)}%</Orange>
      <Col>
        <USDPriceText
          amount={Number(stats.farming_staked)}
          pool={stats.symbol}
        />
      </Col>
      <Col>
        <USDPriceText
          amount={Number(userData.total_staked)}
          pool={stats.symbol}
        />{" "}
      </Col>
      <Col>
        <TokenWithUSDValue
          tokenSymbol={"LIBRE"}
          value={formatCurrencyWithPrecision(
            Number(userData.claimed),
            displayTokenPrecisions["LIBRE"]
          )}
        />
      </Col>
      <ActionCol onClick={handleActionClick}>
        <Caret active={open}>
          <Image src={"/icons/chevron-down.svg"} width={24} height={24} />
        </Caret>
      </ActionCol>
      {open && (
        <FarmingActionRow index={index} userData={userData} balance={balance} />
      )}
    </React.Fragment>
  );
};
