import Link from "next/link";
import React, { useState } from "react";
import styled, { useTheme } from "styled-components";
import { useClaimMintRush } from "../../hooks/queries/tokens/useClaimMintRush";
import useUserMintRush from "../../hooks/queries/tokens/useUserMintRush";
import { IUserFarming } from "../../models/Farming";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";
import { IconPairs } from "../../uikit/IconPairs";
import { FarmingHarvestBlock } from "./FarmingHarvestBlock";
import { FarmingStakeModal } from "./FarmingStakeModal";

const ActionRow = styled.div<{ index: number }>`
  display: flex;
  flex-wrap: wrap;
  grid-row: ${(p) => p.index + 3};
  grid-column: 1 / 7;
  column-gap: 20px;
  margin-bottom: 24px;
`;

const ActionCol = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;

  padding: 20px;
  border-radius: 8px;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  min-width: 400px;
`;

const ActionColWrap = styled(ActionCol)`
  flex: 0 1 calc(50% - 10px);
  margin-top: 20px;
  margin-left: auto;
  column-gap: 20px;
`;

const ActionTitle = styled.span`
  text-transform: uppercase;
  font-size: 14px;
  font-weight: 600;
  line-height: 1.71;
  letter-spacing: 2px;
  color: ${(p) => p.theme.descriptionText};
  display: inline-block;
  margin-bottom: 20px;
`;

const ActionBtm = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: auto;
`;

const ActionBtmLeft = styled.div`
  display: flex;
  align-items: center;
`;

const ActionIcons = styled.div`
  display: flex;
  flex-shrink: 0;
  margin-right: 20px;
`;

const ActionText = styled.span`
  font-size: 33.2px;
  font-weight: 600;
  line-height: 1.21;
  color: ${(p) => p.theme.titleText};
`;

const ActionBtn = styled.div``;

export const FarmingActionRow = ({
  index,
  userData,
  balance,
}: {
  index: number;
  userData: IUserFarming;
  balance: number;
}) => {
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const theme = useTheme();
  const { currentUser } = useAuthContext();
  const { data: userMintRush } = useUserMintRush(currentUser?.actor);
  const { mutateAsync: claimMintRush } = useClaimMintRush();

  const renderStakeBtn = () => {
    if (balance === 0 && Number(userData.total_staked) == 0) {
      return (
        <Link
          href={{
            pathname: "/swap",
            query: { type: "liquidity", pool: userData.symbol },
          }}
          passHref
        >
          <a>
            <MediumButton
              text={"Add Liquidity"}
              onClick={() => {}}
              color={theme.buttonOrangeText}
              backgroundColor={theme.buttonOrange}
            />
          </a>
        </Link>
      );
    }
    return (
      <MediumButton
        text={"Update Stake"}
        onClick={() => setModalOpen(true)}
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
      />
    );
  };

  const renderClaimBtn = () => {
    if (
      userMintRush?.unclaimed &&
      userMintRush.unclaimed.split(" ")[0] &&
      Number(userMintRush.unclaimed.split(" ")[0]) > 0
    ) {
      return (
        <MediumButton
          text={"Claim"}
          onClick={claimMintRush}
          color={theme.buttonOrangeText}
          backgroundColor={theme.buttonOrange}
        />
      );
    }
    return (
      <MediumButton
        disabled
        text={`Claim`}
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  };

  const renderMintClaim = () => {
    if (userData.symbol !== "BTCLIB") return <></>;
    return (
      <ActionColWrap>
        <ActionTitle>LIBRE/PBTC Mint Rush</ActionTitle>
        <ActionBtm>
          <ActionBtmLeft>
            <ActionIcons>
              <IconPairs
                imgSrc1={"/icons/btc-asset-icon.svg"}
                imgSrc2={
                  userData.symbol === "BTCLIB"
                    ? "/icons/libre-asset-icon.svg"
                    : "/icons/usdt-asset-icon.png"
                }
              />
            </ActionIcons>
            <ActionText>
              {userMintRush?.unclaimed
                ? userMintRush?.unclaimed.split(" ")[0]
                : "--"}
            </ActionText>
          </ActionBtmLeft>
          <ActionBtn>{renderClaimBtn()}</ActionBtn>
        </ActionBtm>
      </ActionColWrap>
    );
  };

  return (
    <React.Fragment>
      <ActionRow index={index}>
        <FarmingHarvestBlock pool={userData.symbol} />
        <ActionCol>
          <ActionTitle>{`${userData.symbol} Farm`}</ActionTitle>
          <ActionBtm>
            <ActionBtmLeft>
              <ActionIcons>
                <IconPairs
                  imgSrc1={"/icons/btc-asset-icon.svg"}
                  imgSrc2={
                    userData.symbol === "BTCLIB"
                      ? "/icons/libre-asset-icon.svg"
                      : "/icons/usdt-asset-icon.png"
                  }
                />
              </ActionIcons>
              <ActionText>{userData.total_staked}</ActionText>
            </ActionBtmLeft>
            <ActionBtn>{renderStakeBtn()}</ActionBtn>
          </ActionBtm>
        </ActionCol>
        {renderMintClaim()}
      </ActionRow>
      <FarmingStakeModal
        open={modalOpen}
        handleOnClose={() => setModalOpen(false)}
        userFarming={userData}
      />
    </React.Fragment>
  );
};
