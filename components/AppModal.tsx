import Image from "next/image";
import { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { MediaQueryWidths } from "../utils/constants";

const StyledModal = styled.div`
  padding: 24px;
  border-radius: 16px;
  box-shadow: 0 0.5px 2px 0 rgba(104, 103, 119, 0.16),
    0 0 1px 0 rgba(49, 48, 70, 0.08);
  background-color: #fff;
  max-width: 552px;
  width: 90%;
  margin-top: -20%;
  overflow: auto;

  @media (max-height: 1000px) {
    margin: 0;
    width: 100%;
    max-width: auto;
    overflow: auto;
  }

  @media (max-width: ${MediaQueryWidths.small}px) {
    margin: 0;
    width: 100%;
    height: 100%;
    max-width: auto;
    overflow: auto;
  }
`;

const StyledModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(17, 15, 40, 0.9);
  z-index: 9999;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const HeaderTitle = styled.span`
  font-size: 22px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const AppModal = ({ show, onClose, children, title }) => {
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  const handleCloseClick = (e) => {
    e.preventDefault();
    onClose();
  };

  const modalContent = show ? (
    <StyledModalOverlay>
      <StyledModal>
        <Header>
          <HeaderTitle>{title}</HeaderTitle>
          <Image
            onClick={handleCloseClick}
            src="/icons/close-icon.svg"
            width={24}
            height={24}
            style={{ cursor: "pointer" }}
            alt="close-icon"
          />
        </Header>
        {children}
      </StyledModal>
    </StyledModalOverlay>
  ) : (
    <></>
  );

  if (isBrowser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById("modal-root")
    );
  } else {
    return <></>;
  }
};

export default AppModal;
