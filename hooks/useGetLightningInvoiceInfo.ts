import invoiceTools from "light-bolt11-decoder";
import { find, get } from "lodash";
import { useState } from "react";

export const useGetLightningInvoiceInfo = () => {
  const [loading, setLoading] = useState<boolean>(false);

  const getInfo = async (invoice: string) => {
    if (
      (invoice.startsWith("lightning:lnbc") || invoice.startsWith("lnbc")) &&
      invoice.length > 20
    ) {
      setLoading(true);
      let invoiceInfoRaw = invoiceTools.decode(
        invoice.replace("lightning:", "")
      );
      if (!invoiceInfoRaw) {
        setLoading(false);
        return null;
      }
      const amountSatoshis =
        get(find(invoiceInfoRaw.sections, { name: "amount" }), "value", 0) /
        1000;
      const timestamp = new Date(
        get(find(invoiceInfoRaw.sections, { name: "timestamp" }), "value", 0) *
          1000
      );
      const expirySeconds = get(
        find(invoiceInfoRaw.sections, { name: "expiry" }),
        "value",
        0
      );
      timestamp.setSeconds(timestamp.getSeconds() + expirySeconds);
      const isExpired = timestamp.getTime() < new Date().getTime();
      setLoading(false);
      return (
        {
          amountSatoshis,
          isExpired,
          invoice: invoiceInfoRaw.paymentRequest,
          expiry: expirySeconds,
        } || null
      );
    } else {
      return null;
    }
  };

  return {
    lightningInfoLoading: loading,
    getInfo,
  };
};
