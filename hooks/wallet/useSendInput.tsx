import { debounce } from "lodash";
import { useCallback, useContext, useEffect, useState } from "react";
import { WalletSendContext } from "../../providers/WalletSendProvider";
import { useGetLightningInvoiceInfo } from "../useGetLightningInvoiceInfo";
import { useValidateBitcoin } from "../useValidateBitcoin";
import { useValidateEthereum } from "../useValidateEthereum";

export const useSendInput = () => {
  const [value, setValue] = useState<string>("");
  const [debouncedValue, setDebouncedValue] = useState<string>("");
  const [error, setError] = useState<string>("");

  const {
    bitcoinAddress,
    isValid: isValidBitcoinAddress,
    isTapRoot,
  } = useValidateBitcoin(debouncedValue);
  const { address: ethAddress, isValid: isValidEthAddress } =
    useValidateEthereum(debouncedValue);
  const { setRecipient, setSearchQuery, selectedToken } =
    useContext(WalletSendContext);
  const { getInfo } = useGetLightningInvoiceInfo();

  const handleSearch = useCallback(
    debounce(async (query) => {
      setDebouncedValue(String(query).toLocaleLowerCase());
      if (query.length < 20) setSearchQuery(String(query).toLocaleLowerCase());
    }, 1000),
    [selectedToken.symbol]
  );

  const handleOnChange = useCallback(
    ({ target }) => {
      setValue(target.value);
      handleSearch(target.value.trim());
    },
    [handleSearch]
  );

  // INPUT FLOW ------------------------------ //

  const handleInputCheck = async () => {
    if (!debouncedValue) return;
    if (debouncedValue.length < 20) return;
    if (selectedToken.symbol === "BTCL" || selectedToken.symbol === "PBTC") {
      if (isTapRoot)
        return setError("Unsupported bitcoin address. (Taproot address)");
      if (isValidBitcoinAddress)
        return setRecipient({
          destination: bitcoinAddress,
          type: "bitcoin",
        });
      const lightningInvoice = await getInfo(debouncedValue);
      if (lightningInvoice) {
        if (lightningInvoice.isExpired) return setError("Invoice has expired");
        return setRecipient({
          destination: lightningInvoice,
          type: "lightning",
        });
      }
      return setError("Invalid bitcoin address");
    }
    if (selectedToken.symbol === "USDL" || selectedToken.symbol === "PUSDT") {
      if (isValidEthAddress)
        return setRecipient({
          destination: ethAddress,
          type: "ethereum",
        });
      return setError("Invalid ERC-20 tether address");
    }
  };

  useEffect(() => {
    handleInputCheck();
  }, [debouncedValue]);

  useEffect(() => {
    setError("");
    setValue("");
    setDebouncedValue("");
  }, [selectedToken.symbol]);

  return {
    value,
    error,
    onChange: handleOnChange,
  };
};
