import { useQuery, UseQueryResult } from "react-query";
import { ICurrencyBalanceRequest } from "../../../models/Contract";
import LibreClient from "../../../services/LibreClient";
import { RQ_CURRENCY_BALANCE } from "./keys";

const getCurrencyBalance = async (payload: ICurrencyBalanceRequest) => {
  const data: any[] = await LibreClient.getCurrencyBalance(payload);
  return data;
};

export default function useCurrencyBalance(
  payload: ICurrencyBalanceRequest
): UseQueryResult<any[], Error> {
  return useQuery<any[], Error>([RQ_CURRENCY_BALANCE, payload], () =>
    getCurrencyBalance(payload)
  );
}
