import { useQuery, UseQueryResult } from "react-query";
import { ITableRowRequest } from "../../../models/Contract";
import LibreClient from "../../../services/LibreClient";
import { RQ_TABLE_ROWS } from "./keys";

const getTableRows = async (payload: ITableRowRequest) => {
  const data: any[] = await LibreClient.getTableRows(payload);
  return data;
};

export default function useTableRows(
  payload: ITableRowRequest,
  options: any
): UseQueryResult<any[], Error> {
  return useQuery<any[], Error>(
    [RQ_TABLE_ROWS, { ...payload }],
    () => getTableRows(payload),
    options
  );
}
