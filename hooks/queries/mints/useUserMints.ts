import { useQuery, UseQueryResult } from "react-query";
import { IMint } from "../../../models/Mints";
import APIClient from "../../../services/APIClient";
import { RQ_USER_MINTS } from "./keys";

const getUserMints = async (accountName: string) => {
  const data: IMint[] = await APIClient.fetchUserMints(accountName);
  return data;
};

export default function useUserMints(
  accountName: string
): UseQueryResult<IMint[], Error> {
  return useQuery<IMint[], Error>([RQ_USER_MINTS, accountName], () =>
    getUserMints(accountName)
  );
}
