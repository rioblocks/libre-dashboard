import { useQuery, UseQueryResult } from "react-query";
import { IDaoStats } from "../../../models/Dao";
import LibreClient from "../../../services/LibreClient";
import { RQ_DAO_STATS } from "./keys";

const getDaoStats = async () => {
  const libreContract = "eosio.token";
  const btcContract =
    process.env.NEXT_PUBLIC_ENV !== "production"
      ? "eosio.token"
      : "btc.ptokens";
  const usdtContract =
    process.env.NEXT_PUBLIC_ENV !== "production"
      ? "eosio.token"
      : "usdt.ptokens";

  const libreSymbol = "LIBRE";
  const btcSymbol =
    process.env.NEXT_PUBLIC_ENV !== "production" ? "BTCL" : "PBTC";
  const usdtSymbol =
    process.env.NEXT_PUBLIC_ENV !== "production" ? "USDL" : "PUSDT";

  const daoPower = await LibreClient.getDaoPower();
  const daoLibreTotal = await LibreClient.getDaoCurrency({
    code: libreContract,
    symbol: libreSymbol,
  });
  const daoBtcTotal = await LibreClient.getDaoCurrency({
    code: btcContract,
    symbol: btcSymbol,
  });
  const daoUsdtTotal = await LibreClient.getDaoCurrency({
    code: usdtContract,
    symbol: usdtSymbol,
  });
  const [power, libre, btc, usdt] = await Promise.all([
    daoPower,
    daoLibreTotal,
    daoBtcTotal,
    daoUsdtTotal,
  ]);

  const votingPower = power.reduce((a, b) => a + Number(b.voting_power), 0);
  const totalLibre = libre ? Number(libre.split(" ")[0]) : 0;
  const totalBtc = btc ? Number(btc.split(" ")[0]) : 0;
  const totalUsdt = usdt ? Number(usdt.split(" ")[0]) : 0;

  return {
    voting_power: votingPower,
    total_libre: totalLibre,
    total_btc: totalBtc,
    total_usdt: totalUsdt,
  };
};

export default function useDaoStats(): UseQueryResult<IDaoStats, Error> {
  return useQuery<IDaoStats, Error>([RQ_DAO_STATS], getDaoStats);
}
