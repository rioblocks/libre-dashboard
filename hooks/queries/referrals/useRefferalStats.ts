import { useQuery, UseQueryResult } from "react-query";
import { IReferralStats } from "../../../models/Referrals";
import APIClient from "../../../services/APIClient";
import { RQ_REFERRAL_STATS } from "./keys";

const getReferralStats = async (name: string) => {
  const data: IReferralStats = await APIClient.fetchReferralStats(name);
  return data;
};

export default function useReferralStats(
  name: string
): UseQueryResult<IReferralStats, Error> {
  return useQuery<IReferralStats, Error>([RQ_REFERRAL_STATS, name], () =>
    getReferralStats(name)
  );
}
