import { useQuery } from "react-query";
import { IProposal } from "../../../models/Proposals";
import LibreClient from "../../../services/LibreClient";
import { RQ_PROPOSAL } from "./keys";

const getProposalById = async (proposalId: string) => {
  const data: IProposal[] = await LibreClient.getProposals(proposalId);
  let responseData = data[0];
  return responseData;
};

export default function useProposal(proposalId: string) {
  return useQuery([RQ_PROPOSAL, proposalId], () => getProposalById(proposalId));
}
