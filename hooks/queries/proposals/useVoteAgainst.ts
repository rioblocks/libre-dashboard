import { useMutation, useQueryClient } from "react-query";
import LibreClient from "../../../services/LibreClient";
import { RQ_PROPOSAL, RQ_VOTES } from "./keys";

export const useVoteAgainst = () => {
  const queryClient = useQueryClient();
  return useMutation(
    (proposalId: string) => LibreClient.voteAgainst(proposalId),
    {
      onSuccess: async (data) => {
        queryClient.invalidateQueries([RQ_VOTES, data.proposalId]);
        return queryClient.invalidateQueries([RQ_PROPOSAL, data.proposalId]);
      },
    }
  );
};
