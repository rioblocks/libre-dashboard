import { useQuery } from "react-query";
import { IVote } from "../../../models/Proposals";
import LibreClient from "../../../services/LibreClient";
import { RQ_VOTES } from "./keys";

const getVotesById = async (proposalId: string) => {
  const data = await LibreClient.getVotes(proposalId);
  return data;
};

export default function useVotes(proposalId: string) {
  return useQuery<IVote[], Error>([RQ_VOTES, proposalId], () =>
    getVotesById(proposalId)
  );
}
