import { useMutation } from "react-query";
import APIClient from "../../../services/APIClient";

export const useUserBTCAddress = () => {
  return useMutation((accountName: string) =>
    APIClient.fetchUserBTCAddress(accountName)
  );
};
