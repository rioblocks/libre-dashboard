import { useQuery, UseQueryResult } from "react-query";
import {
  ILightningWrappingAddress,
} from "../../../models/WrappingAdresses";
import APIClient from "../../../services/APIClient";
import { RQ_WRAPPING_ADRESSES } from "./keys";

export const getLightningWrappingAdresses = async (
  accountName: string,
  amount = 0
) => {
  if (!accountName) return { lightning: "" };
  const data: ILightningWrappingAddress =
    await APIClient.fetchLightningWrappingAdresses(accountName, amount);
  return data;
};

export default function useGetWrappingAdresses(
  accountName: string,
  amount = 0
): UseQueryResult<ILightningWrappingAddress, Error> {
  return useQuery<ILightningWrappingAddress, Error>(
    [RQ_WRAPPING_ADRESSES, accountName, amount],
    () => getLightningWrappingAdresses(accountName, amount)
  );
}
