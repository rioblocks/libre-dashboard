import { useMutation, useQueryClient } from "react-query";
import LibreClient, { IRemoveLiquidity } from "../../../services/LibreClient";
import { RQ_POOL_SUPPLY, RQ_USER_POOL_SUPPLY } from "./keys";

export const useRemoveLiquidity = () => {
  const queryClient = useQueryClient();
  return useMutation(
    (payload: IRemoveLiquidity) => LibreClient.removeLiquidity(payload),
    {
      onSuccess: async () => {
        // DELAY ADDED TO ALLOW API TO UPDATE
        setTimeout(() => {
          queryClient.invalidateQueries([RQ_POOL_SUPPLY]);
          queryClient.invalidateQueries([RQ_USER_POOL_SUPPLY]);
        }, 1000);
      },
    }
  );
};
