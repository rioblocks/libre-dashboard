import { useQuery, UseQueryResult } from "react-query";
import { IApplicationStats } from "../../../models/Stats";
import APIClient from "../../../services/APIClient";
import { RQ_APPLICATION_STATS } from "./keys";

const getApplicationStats = async () => {
  const data: IApplicationStats = await APIClient.fetchApplicationStats();
  return data;
};

export default function useApplicationStats(): UseQueryResult<
  IApplicationStats,
  Error
> {
  return useQuery<IApplicationStats, Error>(
    [RQ_APPLICATION_STATS],
    getApplicationStats
  );
}
