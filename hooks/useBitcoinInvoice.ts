import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../providers/AuthProvider";
import { useCacheBTCAddress } from "./queries/tokens/useCacheBTCAddress";
import useGetBitcoinWrappingAddress from "./queries/tokens/useGetBitcoinWrappingAddress";
import { useUserBTCAddress } from "./queries/tokens/useUserBTCAddress";

export const useBitcoinInvoice = () => {
  const { currentUser } = useContext(AuthContext);
  const { mutateAsync: generateAddress, isLoading: isLoadingGeneratedAddress } =
    useUserBTCAddress();
  const { data: wrappingAddress, isLoading: isLoadingWrappingAddress } =
    useGetBitcoinWrappingAddress(currentUser.actor);
  const { mutateAsync: cacheAddress } = useCacheBTCAddress();

  const [bitcoinAddress, setBitcoinAddress] = useState<string>("");

  const handleBitcoinAddress = async () => {
    if (wrappingAddress) return setBitcoinAddress(wrappingAddress);

    const address = await generateAddress(currentUser.actor);
    if (address) setBitcoinAddress(address);

    if (process.env.NEXT_PUBLIC_ENV === "production" && address) {
      await cacheAddress({
        accountName: currentUser.actor,
        address,
        network: "mainnet",
      });
    }
  };

  useEffect(() => {
    if (!currentUser || !currentUser.actor) return;
    if (isLoadingWrappingAddress) return;
    handleBitcoinAddress();
  }, [currentUser, isLoadingWrappingAddress]);

  return {
    bitcoinAddress,
    isLoading: isLoadingGeneratedAddress || isLoadingWrappingAddress,
  };
};
