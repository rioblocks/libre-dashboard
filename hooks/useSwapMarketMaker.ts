import { debounce, maxBy } from "lodash";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useQueryClient } from "react-query";
import { IMarkusOffer } from "../models/Markus";
import { IUserToken } from "../models/Tokens";
import { useAuthContext } from "../providers/AuthProvider";
import { formatRoundedDownTokenPrecision } from "../utils";
import { MARKUS_OFFERS_REFETCH_ATTEMPTS } from "../utils/constants";
import { RQ_MARKUS_OFFERS } from "./queries/markus/keys";
import { useCreateMarkusOrder } from "./queries/markus/useCreateMarkusOrder";
import useMarkusOffers from "./queries/markus/useMarkusOffers";

interface IUseSwapMarketMaker {
  slippage: string;
  minExpectedAmount: string;
  fromToken: IUserToken;
  toToken: IUserToken;
  fromTokenValue: string;
}

export const useSwapMarketMaker = ({
  slippage,
  minExpectedAmount,
  fromToken,
  toToken,
  fromTokenValue,
}: IUseSwapMarketMaker) => {
  const callCountRef = useRef(0);

  const queryClient = useQueryClient();
  const { currentUser } = useAuthContext();

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [orderId, setOrderId] = useState<string>("");

  const { mutateAsync: createMarkusOrder } = useCreateMarkusOrder();
  const { data: markusOffers } = useMarkusOffers(orderId, callCountRef);

  const orderPrice = useCallback(
    debounce(async (slippage) => {
      queryClient.setQueryData(RQ_MARKUS_OFFERS, null);
      if (fromToken.unstaked < Number(fromTokenValue)) return;
      if (Number(slippage) > 1 && currentUser.actor) {
        setOrderId("");
        setIsLoading(true);
        const contractName =
          process.env.NEXT_PUBLIC_ENV === "development"
            ? "eosio.token"
            : fromToken.name === "PBTC"
            ? "btc.ptokens"
            : fromToken.name === "PUSDT"
            ? "usdt.ptokens"
            : "eosio.token";
        try {
          const data = await createMarkusOrder({
            account: currentUser.actor,
            orderAsset: {
              quantity: `${formatRoundedDownTokenPrecision({
                value: Number(fromTokenValue),
                precision: fromToken.precision,
              })} ${fromToken.symbol}`,
              contract: contractName,
            },
            minReceivable: {
              quantity: `${formatRoundedDownTokenPrecision({
                value: Number(0.0),
                precision: toToken.precision,
              })} ${toToken.symbol}`,
              contract: contractName,
            },
            expiry: 30,
          });
          if (data && data.identifier) setOrderId(data.identifier);
        } catch (e) {
          setIsLoading(false);
        }
      } else {
        setOrderId("");
        setIsLoading(false);
      }
    }, 500),
    [fromTokenValue, fromToken, toToken, slippage]
  );

  const {
    bestMarkusOffer,
    bestOfferPrice,
  }: { bestMarkusOffer: IMarkusOffer | null; bestOfferPrice: number | null } =
    useMemo(() => {
      const nullResponse = {
        bestMarkusOffer: null,
        bestOfferPrice: Number(minExpectedAmount),
      };
      if (!markusOffers || !markusOffers.length) return nullResponse;
      const bestOffer = maxBy(markusOffers, (mo: IMarkusOffer) => {
        return Number(mo.offerredAmount.quantity.split(" ")[0]);
      });
      if (!bestOffer) return nullResponse;
      return {
        bestMarkusOffer: bestOffer,
        bestOfferPrice: Number(bestOffer.offerredAmount.quantity.split(" ")[0]),
      };
    }, [markusOffers, minExpectedAmount]);

  // GRAB LATEST SLIPPAGE WHICH REPRESENTS
  // USER UNPUT CHANGES
  useEffect(() => {
    orderPrice(slippage);
    callCountRef.current = 0;
  }, [slippage]);

  useEffect(() => {
    if (!bestMarkusOffer) return;
    setIsLoading(false);
  }, [bestMarkusOffer]);

  useEffect(() => {
    if (callCountRef.current > MARKUS_OFFERS_REFETCH_ATTEMPTS)
      setIsLoading(false);
  }, [callCountRef.current]);

  return {
    marketPrice: `${formatRoundedDownTokenPrecision({
      value: bestOfferPrice,
      precision: toToken.precision,
    })}`,
    marketSlippage: bestMarkusOffer ? "1.0" : slippage,
    markusOffer: bestMarkusOffer,
    isLoading,
  };
};
