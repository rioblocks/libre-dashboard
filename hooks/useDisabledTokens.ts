import { IUserToken } from "../models/Tokens";

interface IUseDisabledTokens {
  tokenFrom: IUserToken;
  tokenTo: IUserToken;
  availableTokens?: IUserToken[];
}

export const useDisabledTokens = ({
  tokenFrom,
  tokenTo,
  availableTokens,
}: IUseDisabledTokens) => {
  let disabledFromTokens: string[] = [];
  let disabledToTokens: string[] = [];

  if (availableTokens) {
    if (tokenFrom.name === "LIBRE")
      disabledToTokens = availableTokens
        .filter((t) => t.name === "USDL" || t.name === "PUSDT")
        .map((t) => t.symbol);
    if (tokenTo.name === "LIBRE")
      disabledFromTokens = availableTokens
        .filter((t) => t.name === "USDL" || t.name === "PUSDT")
        .map((t) => t.symbol);
    if (tokenFrom.symbol === "USDL" || tokenFrom.symbol === "PUSDT")
      disabledToTokens = availableTokens
        .filter((t) => t.name === "LIBRE")
        .map((t) => t.symbol);
    if (tokenTo.symbol === "LIBRE")
      disabledFromTokens = availableTokens
        .filter((t) => t.name === "USDL" || t.name === "PUSDT")
        .map((t) => t.symbol);
  }

  return {
    disabledFromTokens,
    disabledToTokens,
  };
};
