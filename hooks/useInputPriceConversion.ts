import { IPoolSupply, IUserToken } from "../models/Tokens";
interface IUsePoolPriceConversion {
  poolSupply: IPoolSupply[] | undefined;
  pool: string;
  userTokens: IUserToken[] | undefined;
  input1Token: IUserToken;
  input2Token: IUserToken;
  amount1: number;
  amount2: number;
}

const emptyResponse = {
  convertedToken2Price: "0.00",
  convertedToken1Price: "0.00",
};

export const useInputPriceConversion = ({
  poolSupply,
  pool,
  userTokens,
  input1Token,
  input2Token,
  amount1,
  amount2,
}: IUsePoolPriceConversion) => {
  if (!poolSupply) return emptyResponse;
  let activePool;

  if (pool === "BTCUSD") {
    activePool = poolSupply[0];
  } else {
    activePool = poolSupply[1];
  }

  const pool1Asset = {
    price:
      Number(activePool.pool1.quantity.split(" ")[0]) /
      Number(activePool.supply.split(" ")[0]),
    symbol: activePool.pool1.quantity.split(" ")[1],
  };

  const pool2Asset = {
    price:
      Number(activePool.pool2.quantity.split(" ")[0]) /
      Number(activePool.supply.split(" ")[0]),
    symbol: activePool.pool2.quantity.split(" ")[1],
  };

  const poolAssets = [pool1Asset, pool2Asset];

  const input1TokenPrice = poolAssets.find(
    (a) => a.symbol === input1Token.symbol
  )?.price;
  const input2TokenPrice = poolAssets.find(
    (a) => a.symbol === input2Token.symbol
  )?.price;

  const convertedToken2Price =
    (amount1 / Number(input1TokenPrice)) * Number(input2TokenPrice);

  const convertedToken1Price =
    (amount2 / Number(input2TokenPrice)) * Number(input1TokenPrice);

  return {
    convertedToken1Price:
      convertedToken1Price == 0 ? "0.00" : convertedToken1Price.toFixed(8),
    convertedToken2Price:
      convertedToken2Price == 0 ? "0.00" : convertedToken2Price.toFixed(8),
  };
};
