import useDaoStats from "../queries/dao/useDaoStats";
import { useExchangeRates } from "../useExchangeRates";

export const useGovernanceOverviewData = () => {
  const { data, isLoading } = useDaoStats();
  const { data: exchangeRates, isLoading: isLoadingRates } = useExchangeRates();

  const btcValue =
    data?.total_btc && exchangeRates
      ? data?.total_btc *
        exchangeRates[
          process.env.NEXT_PUBLIC_ENV === "development" ? "BTCL" : "PBTC"
        ]
      : 0;
  const libreValue =
    data?.total_libre && exchangeRates && exchangeRates["LIBRE"]
      ? data?.total_libre * exchangeRates["LIBRE"]
      : 0;
  const totalValue = btcValue + libreValue + (data?.total_usdt ?? 0);

  return {
    data: {
      btc: data?.total_btc,
      libre: Number(data?.total_libre).toLocaleString("en-US", {
        minimumFractionDigits: 4,
      }),
      usdt: Number(data?.total_usdt).toLocaleString("en-US", {
        minimumFractionDigits: 2,
      }),
      voting_power: Number(data?.voting_power).toLocaleString("en-US", {
        maximumFractionDigits: 0,
      }),
      total_value: Number(totalValue).toLocaleString("en-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      }),
    },
    isLoading: isLoading || isLoadingRates,
  };
};
