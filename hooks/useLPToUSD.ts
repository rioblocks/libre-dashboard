import { useEffect, useMemo, useState } from "react";
import { usePoolSupply } from "./tokens/usePoolSupply";
import { useExchangeRates } from "./useExchangeRates";

export const useLPToUSD = (tokenAmount: number, pool: string) => {
  const { data: exchangeRates } = useExchangeRates();
  const poolSupply = usePoolSupply();
  const [convertedPrice, setConvertedPrice] = useState<string>("");

  const poolDetails = useMemo(() => {
    const emptyPool = {
      pool1Quantity: 0,
      pool2Quantity: 0,
      supply: 0,
    };
    if (!poolSupply) return emptyPool;
    const activePool = poolSupply.find(
      (ps) => ps.supply.split(" ")[1] === pool
    );
    if (!activePool) return emptyPool;
    return {
      pool1Quantity: Number(activePool.pool1.quantity.split(" ")[0]),
      pool2Quantity: Number(activePool.pool2.quantity.split(" ")[0]),
      supply: Number(activePool.supply.split(" ")[0]),
    };
  }, [poolSupply]);

  useEffect(() => {
    if (
      !poolDetails ||
      !poolDetails.pool1Quantity ||
      !poolDetails.pool2Quantity ||
      !poolDetails.supply ||
      !tokenAmount
    )
      return;
    const rateSymbol =
      process.env.NEXT_PUBLIC_ENV === "development" ? "BTCL" : "PBTC";
    const btcRate =
      exchangeRates && exchangeRates[rateSymbol]
        ? exchangeRates[rateSymbol]
        : 0;
    const totalPoolValue = poolDetails.pool1Quantity * 2 * btcRate; // SINCE BOTH POOLS SHOW BE EQUAL USD VALUE
    const LPValue = totalPoolValue / poolDetails.supply;
    const convertedAmtValue = LPValue * tokenAmount;

    setConvertedPrice(String(convertedAmtValue));
  }, [poolDetails, tokenAmount, exchangeRates]);

  return convertedPrice
    ? `$${Number(Number(convertedPrice).toFixed(2)).toLocaleString("en-us")}`
    : "--";
};
