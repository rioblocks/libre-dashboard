export const EMPTY_BALANCE = "0 LIBRE";
export const TOKEN_PRECISION = 0;
export const SHORTENED_TOKEN_PRECISION = 0;
export const PROPOSALS_CONTRACT = "dao.libre"; // all proposal actions live on this contract
export const PROPOSALS_TOKEN = "LIBRE";
export const PROPOSALS_TRANSFER_TOKEN = "eosio.token"; // token contract that the token lives on (transfer and pulling balance data must use this)
export const PROPOSALS_FEE = "2000.0000 LIBRE";
export const MAX_ASSET_FEE_MULTIPLIER: number = 1.00012;
export const MIN_FEE_MULTIPLIER: number = 2 / 10;
export const EXPECTED_FEE_DIVIDER: number = 10000;
export const MARKUS_OFFERS_REFETCH_ATTEMPTS = 10;

export const MediaQueryWidths = {
  tiny: 400,
  small: 600,
  medium: 970,
  large: 1224,
};

export enum SendWalletValueTypes {
  USD = "USD",
  TOKEN = "TOKEN",
}

interface IChainConfig {
  [key: string]: any;
}

export const CHAIN_CONFIG = (): IChainConfig => {
  const environment = process.env.NEXT_PUBLIC_ENV;
  const nodeUrl = process.env.NEXT_PUBLIC_NODE_URL;
  const secondaryNode = process.env.NEXT_PUBLIC_BACKUP_NODE_URL;
  const chainId = process.env.NEXT_PUBLIC_CHAIN_ID;
  if (environment && environment.toLowerCase() === "development") {
    return {
      hyperion: {
        url: nodeUrl || "https://testnet.libre.org",
        swapContract: "evotest",
        backup_url:
          secondaryNode ?? "https://hyperion.libretest.quantumblok.com",
        chainId:
          chainId ??
          "b64646740308df2ee06c6b72f34c0f7fa066d940e831f752db2006fcc2b78dee",
      },
      btc: {
        symbol: "BTCL",
        contract: "eosio.token",
        precision: 8,
        icon: "https://cdn.libre.org/icon-btc.svg",
      },
      usdt: {
        symbol: "USDL",
        contract: "eosio.token",
        precision: 8,
        icon: "https://cdn.libre.org/icon-usdt-v2.svg",
      },
      libre: {
        symbol: "LIBRE",
        contract: "eosio.token",
        precision: 4,
        icon: "https://cdn.libre.org/icon-libre-v2.svg",
      },
    };
  }
  return {
    hyperion: {
      url: nodeUrl || "https://lb.libre.org",
      swapContract: "swap.libre",
      backup_url: secondaryNode || "https://lb.libre.org",
      chainId:
        chainId ??
        "38b1d7815474d0c60683ecbea321d723e83f5da6ae5f1c1f9fecc69d9ba96465",
    },
    btc: {
      symbol: "PBTC",
      contract: "btc.ptokens",
      precision: 9,
      icon: "https://cdn.libre.org/icon-btc.svg",
    },
    usdt: {
      symbol: "PUSDT",
      contract: "usdt.ptokens",
      precision: 9,
      icon: "https://cdn.libre.org/icon-usdt-v2.svg",
    },
    libre: {
      symbol: "LIBRE",
      contract: "eosio.token",
      precision: 4,
      icon: "https://cdn.libre.org/icon-libre-v2.svg",
    },
  };
};
